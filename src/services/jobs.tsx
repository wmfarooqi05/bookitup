import {
    // makePostRequest,
    makeGetRequest,
  } from '../Utils/requestUtils';
  
  const requestFetchJobs = ({
    url, requestPayload, needsAuthentication = false, authToken = null,
  }) => 
    makeGetRequest(url, requestPayload, needsAuthentication, authToken);
  
  export {
    requestFetchJobs,
  };
  