import {
    makePostRequest,
    makeGetRequest,
  } from '../Utils/requestUtils';
import { AUTH_TOKEN } from '../redux/constants';
import { AsyncStorage } from 'react-native';
  
  const convertFacebookTokenToAuthToken = ({
    url, requestPayload, needsAuthentication = false, customURL = '',
  }) =>
    makePostRequest(url, requestPayload, needsAuthentication, customURL);
  
  const requestSignup = ({ url, requestPayload, needsAuthentication = false }) =>
    makePostRequest(url, requestPayload, needsAuthentication);
  
  const requestEditPassword = ({ url, requestPayload, needsAuthentication = true }) =>
    makePostRequest(url, requestPayload, needsAuthentication);
  
  const getGuestAuthToken = ({ url, requestPayload, needsAuthentication = false }) =>
    makePostRequest(url, requestPayload, needsAuthentication);
  
  const requestCreateGuestUser = ({ url, requestPayload, needsAuthentication = false }) =>
    makePostRequest(url, requestPayload, needsAuthentication);
  
  const requestFetchUserInfo = ({ url, queryString = '', needsAuthentication = true }) =>
    makeGetRequest(url, queryString, needsAuthentication);
  
  const requestCreateMovieProfile = ({ url, queryString = '', needsAuthentication = true }) =>
    makeGetRequest(url, queryString, needsAuthentication);
  
  const sendPasswordResetEmail = ({ url, requestPayload = {}, needsAuthentication = true }) =>
    makePostRequest(url, requestPayload, needsAuthentication);
  
  
  const requestlocalSignIn = ({
    url,
    requestPayload = {},
    needsAuthentication = false,
    customURL,
  }) => makePostRequest(url, requestPayload, needsAuthentication, customURL);
  
  const requestAuthTokenSignIn = ({
    url,
    queryString = '',
    needsAuthentication = true,
    authToken,
  }) => makePostRequest(url, queryString, needsAuthentication, authToken);
  

async function getAuthToken(){
  let token = AsyncStorage.getItem(AUTH_TOKEN);
  return token;
}
  
export {
  getGuestAuthToken,
  requestCreateGuestUser,
  requestCreateMovieProfile,
  requestFetchUserInfo,
  requestlocalSignIn,
  sendPasswordResetEmail,
  convertFacebookTokenToAuthToken,
  requestSignup,
  requestEditPassword,
  requestAuthTokenSignIn,
  getAuthToken,
};
  