// import fetch from 'fetch-everywhere';
import {
  GET,
  POST,
  DELETE,
} from './httpVerbs';
// import { buildURL, embedTokenInHeaders, needMoreHeaders, needsUrlApiPrefix, stringifyBody, stringifyHeaders } from '../Utils/authUtils';
import { needsUrlApiPrefix, buildURL, embedTokenInHeaders } from '../Utils/authUtils';
import isNeededToken from '../Utils/authUtils';
import URLS from '../constants/URLS';
import { makeFormDataObject } from '../Utils/requestUtils';

require('es6-promise').polyfill();

class Api {
  constructor() {
    // will be used for both production and test servers
    this.baseImageUrl = 'https://static.taste.io/images/user/avatar/';

    this.baseURL = URLS.baseURL; // for sake of convenience
    this.apiURL = URLS.apiURL;

    this.tempAuth = URLS.tempAuthToken;

    // this.baseUrl = 'https://ymal-devcrew.herokuapp.com';
    // this.apiURL = 'https://ymal-devcrew.herokuapp.com/api/';

    this.headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };

    this.delete = this.delete.bind(this);
    this.get = this.get.bind(this);
    this.post = this.post.bind(this);
  }

  delete({
    needsAuthentication = false,
    queryString = '',
    url,
  }) {
    let { headers } = this;
    const apiURL = URLS.apiURL;
    const completeUrl = buildURL(DELETE, apiURL, url, queryString);

    if (isNeededToken(needsAuthentication)) {
      headers = embedTokenInHeaders(headers);
    }

    return fetch(completeUrl, {
      headers,
      method: DELETE,
    });
  }

  get({
    url,
    queryString = '',
    needsAuthentication = false,
    authToken = null,
  }) {
    const apiURL = URLS.apiURL;
    let { headers } = this;
 
    const completeUrl = buildURL(GET, apiURL, url, queryString);
    if (isNeededToken(needsAuthentication)) {
      headers = embedTokenInHeaders(headers, authToken);
    }
    console.log('Token is ', JSON.stringify(headers));

    return fetch(completeUrl, {
      headers,
    }).then((responce) => {
      // successful fetch
      return responce;
    }).catch(function(error) {

      _errorBody = JSON.stringify({
        status: 504,
        error: error.message,
      });

      errorJson = {
        ok: false,
        status: 504,
        url: completeUrl,
        _bodyText: _errorBody,
      }
      return errorJson;
      // network request failed / timeout
    });
    }

  post({
    needsAuthentication = false,
    requestPayload = {},
    customURL = '',
    url,
    authToken
    // authToken = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmM4N2MyNTI2ZTlkNTFhOGNiZjM3YTEiLCJlbWFpbCI6ImFsaWFsaUBnbWFpbC5jb20iLCJpYXQiOjE1NDAzODQyMDF9.nO2WKGrpYSgjjAQoNf7U9EUJbffnYz2ALKuJzEtRHI0",
  }) {
    let completeUrl = '';
    const {
      apiURL,
    } = this;

    let { headers } = this;

    // customURL = 'https://www.taste.io/api/abc';

    if (needsUrlApiPrefix(customURL)) {
      completeUrl = buildURL(POST, apiURL, url);
    } else {
      completeUrl = customURL;
    }

    //TODO: write this method
    // if (needMoreHeaders(customHeaders)) {
    //   const stringifiedHeaders = stringifyHeaders(...customHeaders);
    //   headers = {
    //     ...headers,
    //     stringifiedHeaders,
    //   };
    // }

    const body = makeFormDataObject(requestPayload);
    // Not needed in bookitup scope
    if (isNeededToken(needsAuthentication)) {
      headers = embedTokenInHeaders(headers, authToken);
    }


    return fetch(completeUrl, {
      body,
      headers,
      method: POST,
    }).then((responce) => {
      // successful fetch
      return responce;
    }).catch(function(error) {

      _errorBody = JSON.stringify({
        status: 504,
        error: error.message,
      });

      errorJson = {
        ok: false,
        status: 504,
        url: completeUrl,
        _bodyText: _errorBody,
      }
      return errorJson;
      // network request failed / timeout
    });
  }
}

export default (new Api());
