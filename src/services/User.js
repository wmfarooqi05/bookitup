class User {
// replace this with userprofile class
    userToken: string;

    constructor() {
      this.userToken = '';
      this.setUserToken = this.setUserToken.bind(this);
      this.getUserToken = this.getUserToken.bind(this);
    }
  
    setUserToken(token) {
      this.userToken = token;
    }
  
    getUserToken() {
      return this.userToken;
    }
  }
  
  export default (new User());
  