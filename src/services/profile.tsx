import {
    makePostRequest,
    makeGetRequest,
  } from '../Utils/requestUtils';
  
  const requestAddEducation = ({
    url, requestPayload, needsAuthentication = false, customURL = '', authToken = ''
  }) =>
  makePostRequest(url, requestPayload, needsAuthentication, customURL, authToken);
  
  const requestAddWorkExperienceApi = ({
    url, requestPayload, needsAuthentication = false, customURL = '', authToken = ''
  }) =>
  makePostRequest(url, requestPayload, needsAuthentication, customURL, authToken);
  // const requestCreateMovieProfile = ({ url, queryString = '', needsAuthentication = true }) =>
  //   makeGetRequest(url, queryString, needsAuthentication);
  
  export {
    requestAddEducation,
    requestAddWorkExperienceApi,
  };