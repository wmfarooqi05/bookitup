const URLS = {
  baseURL: 'http://arkotechsoftware.com/dev/bookitup',
  apiURL: 'http://arkotechsoftware.com/dev/bookitup',
  tempAuthToken : "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YmM4N2MyNTI2ZTlkNTFhOGNiZjM3YTEiLCJlbWFpbCI6ImFsaWFsaUBnbWFpbC5jb20iLCJpYXQiOjE1NDAzODQyMDF9.nO2WKGrpYSgjjAQoNf7U9EUJbffnYz2ALKuJzEtRHI0",

  SIGNIN: 'signin.php',
  GET_JOBS: '/job/',
  GET_MY_JOBS: '/job/',
  EDUCATION: '/profile/education',
  WORK: '/profile/work',
  COVER_LETTER: '/coverletter',
  PERSONAL_STATEMENTL: 'personalstatement',
};

export default URLS;