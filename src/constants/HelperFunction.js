import emailValidator from '../helperFunctions/emailValidator';

const HelperFunction = {
    emailValidator,
};

export default HelperFunction;