const iconForward = require('../assets/icons/iconForward.png');
const lock = require('../assets/icons/icon_password.png');
const email = require('../assets/icons/icon_email.png');
const biuLogo = require('../assets/icons/img_biu.png');
const iconDownward = require('../assets/icons/icon_down_arrow.png');
const iconUpward = require('../assets/icons/icon_up_arrow.png');
const estonia = require('../assets/icons/img_estonia.png');
const iconBack = require('../assets/icons/iconBack.png');

// const cross = require('../assets/icons/Cross.png');
// const crossFilled = require('../assets/icons/CrossFilled.png');
// const create = require('../assets/icons/Create.png');
// const avatarIcon = require('../assets/icons/avatarIcon.png');
// const plusIcon = require('../assets/icons/plusIcon.png');
// const MPLOLogoHolo = require('../assets/icons/MPLOLogoHolo.png');
// const Star = require('../assets/icons/Star.png');
// const dollarButton = require('../assets/icons/Accept.png');
// const message = require('../assets/icons/Message.png');
// const meter = require('../assets/icons/MeterIcon.png');
// const fourOptions = require('../assets/icons/FourOptionIcon.png');
// const files = require('../assets/icons/FilesIcon.png');
// const board = require('../assets/icons/BoardIcon.png');
// const sun = require('../assets/icons/SunIcon.png');
// const mapPointer = require('../assets/icons/Home-MPLO-slice_11.png');
// const graph = require('../assets/icons/GraphIcon.png');
// const search = require('../assets/icons/Search.png');
// const Forma = require('../assets/icons/Forma.png');
// const OptIcon = require('../assets/icons/OptIcon.png');
// const location = require('../assets/icons/Location.png');
// const MenuIcon = require('../assets/icons/MenuIcon.png');
// const HeartIcon = require('../assets/icons/HeartIcon.png');

// const HomeButton = require('../assets/icons/HomeButton.png');
// const NavHomeIcon = require('../assets/icons/NavHomeIcon.png');
// const NavNotificationIcon = require('../assets/icons/NavNotificationIcon.png');
// const NavSearchIcon = require('../assets/icons/NavSearchIcon.png');
// const NavUserIcon = require('../assets/icons/NavUserIcon.png');
// const Pointer = require('../assets/icons/Pointer.png');
// const PointerFaded = require('../assets/icons/PointerFaded.png');
// const NavPointer = require('../assets/icons/NavPointer.png');
// const Checkmark = require('../assets/icons/Checkmark.png');
// const CrossButton = require('../assets/icons/CrossButton.png');
// const BellNotificaiton = require('../assets/icons/BellNotificaiton.png');
// const AddButtonBlueTransparent = require('../assets/icons/AddButtonBlueTransparent.png');
// const IconLoc = require('../assets/icons/iconloc.png');
// const SettingsIcon = require('../assets/icons/settingsicon.png');

const Icons = {
  iconForward,
  lock,
  email,
  biuLogo,
  iconDownward,
  iconUpward,
  estonia,
  iconBack,
  // cross,
  // crossFilled,
  // create,
  // avatarIcon,
  // plusIcon,
  // MPLOLogoHolo,
  // Star,
  // dollarButton,
  // message,
  // meter,
  // fourOptions,
  // files,
  // board,
  // sun,
  // mapPointer,
  // graph,
  // search,
  // Forma,
  // OptIcon,
  // location,
  // MenuIcon,
  // HeartIcon,
  // HomeButton,
  // NavHomeIcon,
  // NavNotificationIcon,
  // NavSearchIcon,
  // NavUserIcon,
  // Pointer,
  // PointerFaded,
  // NavPointer,
  // Checkmark,
  // CrossButton,
  // BellNotificaiton,
  // AddButtonBlueTransparent,
  // IconLoc,
  // SettingsIcon,
};

export default Icons;
