const Routes = {
  Home:'Home',

  Temp:'Temp',
  Splash:'Splash',
  SignIn:'SignIn',
  SignUp: 'SignUp',
  GettingStarted:'GettingStarted',
  Hello: 'Hello',
  
  ForgotPassword:'ForgotPassword',
  ProfileMatched:'ProfileMatched',
  Maps:'Maps',
  ViewsOptionsScreen:'ViewsOptionsScreen',
  JobBoard:'JobBoard',
  CreateJob:'CreateJob',
  EmpCreateProfile:'EmpCreateProfile',
  EmpRegisterScreen:'EmpRegisterScreen',
  
  JobDetailScreen: 'JobDetailScreen',


  UserCreateProfile:'UserCreateProfile',
  AddEducation: 'AddEducation',
  AddWorkExperience: 'AddWorkExperience',
    
  UserRegisterScreen:'UserRegisterScreen',

  SearchJobs: 'SearchJobs',

  Settings: 'Settings',
  AppNotifications: 'AppNotifications',
  ChangePassword: 'ChangePassword',
  EmailNotifications: 'EmailNotifications',
  UserProfileView: 'UserProfileView',
};

export const initialRouteName = Routes.Hello;

export default Routes;