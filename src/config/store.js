import { createStore, applyMiddleware, compose } from 'redux';
import logger from 'redux-logger';
import thunk from 'redux-thunk';
import createSagaMiddleware from 'redux-saga';
import { middleware } from '../navigator/AppWithNavState';
import reducers from '../redux/reducers';
import rootSaga from '../redux/sagas';
const sagaMiddleware = createSagaMiddleware();
import { composeWithDevTools } from 'redux-devtools-extension';

export default function configureStore() {

  const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
  
  // const enhancer = compose(
  const enhancer = composeEnhancers(
    applyMiddleware(middleware),
    applyMiddleware(thunk),
    applyMiddleware(sagaMiddleware),
    applyMiddleware(logger),
  );

  const store = createStore(reducers, enhancer);

  sagaMiddleware.run(rootSaga);
  
  return store;
}