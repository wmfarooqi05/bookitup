import React, { Component } from 'react';
import { Text, View, Image, Dimensions, ActivityIndicator } from 'react-native';
import { Button } from 'native-base';

import { connect } from 'react-redux';
import { navigate } from '../redux/actions/nav';
// import { loginWithEmail } from '../redux/actions/user';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

function withErrors(Component) {
  function WrappedComponent({ hasError, children, ...props }) {
    if (hasError) {
      return <View><Text>Oops!</Text></View>;
    }
    return <Component>{children}</Component>;
  }

  return WrappedComponent;
}

const DivWithErrors = withErrors(function(props) {
  return <View><Text>Waleed</Text>{props.children}</View>;
});

class Temp extends Component {

  constructor(props) {
    super(props);
    this.state = {    
      loading: false,
      hasError:false,
    }
  }

  componentDidMount() {
    // let user = this.props.user;
  }

  render() {
    return(
        <View style={{justifyContent: 'center',}}>
          <DivWithErrors hasError={this.state.hasError}>
            <Button onPess={() => this.setState({hasError:true})}><Text>Waleed</Text></Button>
            <Text>No Errors</Text>
          </DivWithErrors>  
        </View>
    )
  }
}

const mapStateToProps = state => ({
  nav: state.nav,
  user: state.user,
});

const mapDispatchToProps = dispatch => ({
  navigate: (route) => dispatch(navigate(route)),
  resetRoute: (route) => dispatch(resetRoute(route)),
  loginWithEmail: (email, sso_key) => dispatch(loginWithEmail(email, sso_key))
});

export default connect(mapStateToProps, mapDispatchToProps)(Temp);
