/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform, ImageBackground, ActivityIndicator,
  StyleSheet,Image, Text, Button,
  View, Dimensions,StatusBar,
} from 'react-native';
import { connect } from 'react-redux';
// import GlobalKeys from '../../constants';
import DropdownAlert from 'react-native-dropdownalert';
import Routes from '../../constants/Routes';
import Types from 'Types';
import { Dispatch } from 'redux';

import { navigate, resetRoute, ROUTE_TYPE } from '../../redux/actions/nav';
import { requestLocalSignIn } from '../../redux/actions/auth';
import { isEmpty } from 'lodash';
import { CURR_USER_TYPE, INITIAL_USER } from '../../redux/reducers/auth';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const defaultProps = Object.freeze({
});

type OwnProps = typeof defaultProps;
type StateProps = typeof defaultStateProps;
type Props = StateProps & DispProps & OwnProps;

type State = {
  profileLoaded: boolean,
  profileExist: boolean,
  isLoading: boolean,
};

class Splash extends Component<Props, State> {

  constructor(props: Props){
    super(props);
    this.state = { 
      profileLoaded: false,
      profileExist: false,
      isLoading: false,
    };
  }

  componentDidMount() {
    // this.checkForSavedProfile();
  }

  shouldComponentUpdate(nextProps, nextState): boolean {
    const { currentUser, hasLoginAfterEffectsCompleted, errorMessages } = this.props;

    if (errorMessages !== nextProps.errorMessages) {
      if (!isEmpty(nextProps.errorMessages)) {
        const { email, password } = nextProps.errorMessages;
        this.setState({ isLoading: false });
        this.dropdown.alertWithType('error', 'Error!', email || password || JSON.stringify(nextProps.errorMessages.error));
        return true;
      }
    }

    if (hasLoginAfterEffectsCompleted !== nextProps.hasLoginAfterEffectsCompleted) {
      if (!isEmpty(nextProps.currentUser.userID)) {
        // if (currentUser && currentUser.movieProfile.onboarding) {
        //   return this.props.resetRoute({ routeName: 'TasteCalculationModal' });
        // }
        this.props.resetRoute(Routes.GettingStarted);
        return true;
      }
    }
    return true;
  }

  checkForSavedProfile(){
    try{
        // this.props.requestLocalSignIn({ email: 'wmfarooqi05@gmail.com', password: 'waleed05' });

      if (isEmpty(this.props.currentUser.userID)){
        this.props.resetRoute(Routes.GettingStarted);
      } else {
        this.props.resetRoute(null);
      }
    } catch(e){
      // alert(e);
     this.props.resetRoute(null);
    }
  }

  onPr(){
    alert('a')
    this.checkForSavedProfile();
  }

  isEmptyObject(obj) {
    for(var key in obj) {
        if(obj.hasOwnProperty(key))
            return false;
    }
    return true;
  } 

  ////////////// Ref Functions ////////////////////

  dropdownCallback(ref) {
    this.dropdown = ref;
  }

  render() {
      // if (this.state.isLoading === true){
      return(
          <View style={{flex:1, marginTop:50}}>
          <Text>Waleed</Text>
          <Button title="Waleed" onPress={()=>this.onPr()}>
          </Button>
            {/* <ImageBackground style={{width: deviceWidth, height: deviceHeight, flex:1, justifyContent: 'center', alignItems:'center', backgroundColor: GlobalKeys.Colors.white}}
              source={GlobalKeys.Images.logoSplash} resizeMode="center" 
            >

           <ActivityIndicator style={{zIndex:1, position:'absolute',alignSelf:'center',}} size="large" color={GlobalKeys.Colors.redFont} />
             </ImageBackground>             */}
            <DropdownAlert
              ref={this.dropdownCallback.bind(this)}
            />
        </View>
      )
      // } 
  }
}

// const mapStateToProps = state => {
//   const {
//     auth: {
//       errorMessages,
//       currentUser,
//       hasLoginAfterEffectsCompleted,
//     }, 
//     nav,
//     appState,
//   } = state;

//   return {
//     errorMessages,
//     currentUser,
//     hasLoginAfterEffectsCompleted,
//     nav,appState,
//   };
// }

// const mapDispatchToProps = dispatch => ({
//   navigate: (route) => dispatch(navigate(route)),
//   resetRoute: (route) => dispatch(resetRoute(route)),
//   requestLocalSignIn: (payload) => dispatch(requestLocalSignIn(payload)),
// });

// export default connect(mapStateToProps, mapDispatchToProps)(Splash);


const defaultStateProps = {
  errorMessages: {} as any,
  currentUser: INITIAL_USER as CURR_USER_TYPE,
  hasLoginAfterEffectsCompleted: true as boolean,
}

type DispProps = {
  navigate: (any) => void,
  resetRoute: ( ROUTE_TYPE ) => void,
}

const mapStateToProps = (state: Types.RootState): StateProps => {
  const {
    auth: {
      errorMessages,
      currentUser,
      hasLoginAfterEffectsCompleted,
    }
  } = state;

  return {
    errorMessages,
    currentUser,
    hasLoginAfterEffectsCompleted,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>) : DispProps => ({
  navigate: (route: any) => dispatch(navigate(route)),
  resetRoute: ( ROUTE_TYPE ) => dispatch(resetRoute( ROUTE_TYPE )),
});

export default connect(mapStateToProps, mapDispatchToProps)(Splash);

