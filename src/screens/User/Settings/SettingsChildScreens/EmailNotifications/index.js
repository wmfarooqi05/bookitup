/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform, FlatList, ImageBackground, TextInput,
  StyleSheet, Alert, Image, TouchableOpacity,
  View, Dimensions, Text, StatusBar, Modal as RNModal,
  KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';

import {
  Container, Header, Left, Body, Right, Button, Icon,
  Title, Content, Input, Item, Form, Text as NBText
} from 'native-base';
import { navigate, resetRoute, goBack } from '../../../../../redux/actions/nav';

import GlobalKeys from '../../../../../constants';
import styles from './styles';

import Icons from '../../../../../constants/Icons';
import Images from '../../../../../constants/Images';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

class EmailNotifications extends Component<Props> {

  /*----------------------------
      state cycle functions
    -----------------------------*/
  constructor(props) {
    super(props);
    this.state = {
      educationInfo: [
        {
          id: 1,
          title: 'Universty Degree',
          duration: '2008 - 2012',
          companyName: 'Universty of California, Los Angeles',
        },
      ],
      workInfo: [
        {
          id: 1,
          title: 'React Native Developer',
          duration: '2016 - Today',
          companyName: 'Lorizzle',
        },
        {
          id: 1,
          title: 'Android Developer',
          duration: '2012 - 2016',
          companyName: 'PICKSUM IPSUM',
        },
      ],
      selectedOption:1,
      personalStatement: 'A highly motivated and hardworking individual, who has recently completed their A-Levels, achieving excellent grades in both Maths and Science. Seeking an apprenticeship in the engineering industry to build upon a keen scientific interest and start a career as a maintenance engineer. Eventual career goal is to become a fully-qualified and experienced maintenance or electrical engineer, with the longer-term aspiration of moving into project management.',
    };
  }

  componentDidMount() {
  }




  /*----------------------------
      helper functions
    -----------------------------*/

  /*----------------------------
      network calls
    -----------------------------*/

  /*----------------------------
      render functions
    -----------------------------*/

  renderListItemInfo(item) {
    console.log(item);
    return (
      <View style={styles.infoContainer}>
        <Text style={styles.groupSecondaryTitle}>{item.title}</Text>
        <Text style={styles.groupSubHeader}>{item.duration}</Text>
        <Text style={styles.groupHeader}>{item.companyName}</Text>
      </View>
    )
  }
  renderTextInfo(item) {
    return (
      <View style={styles.textInfoContainer}>
        <Text>
          <Text style={styles.groupHeader}>{item}</Text>
          {/* {item.length > 250 ? 
          <Text>Show More</Text>
          : null
        } */}
        </Text>
      </View>
    )
  }

  /*----------------------------
      state render helper functions
    -----------------------------*/
  truncateText(text) {
    if (text.length > 250) {
      return text.substring(0, 250);
    } else return text;
  }

  getSelectedOption(index){
    if (this.state.selectedOption === index){
    return(
      <Icon style={styles.forwardIcon} name="checkmark"></Icon>
    )
    } else return null;
  }


  getJobsAlertCheckBox(){
    if (this.state.jobsAlert) {
      return(
        <Icon style={styles.forwardIcon} name="ios-checkbox-outline"></Icon>
      );
    } else {
      return(
        <Icon style={styles.forwardIcon} name="ios-square-outline"></Icon>
      );
    }
  }

  getJobsNewsCheckBox(){
    if (this.state.jobsNews) {
      return(
        <Icon style={styles.forwardIcon} name="ios-checkbox-outline"></Icon>
      );
    } else {
      return(
        <Icon style={styles.forwardIcon} name="ios-square-outline"></Icon>
      );
    }
  }


  /*----------------------------
      state changing functions
    -----------------------------*/


  switchJobsAlertOption(){
    if (this.state.jobsAlert)
      this.setState({jobsAlert: false});
    else
      this.setState({jobsAlert: true});
  }

  switchJobsNewsOption(){
    if (this.state.jobsNews)
      this.setState({jobsNews: false});
    else
      this.setState({jobsNews: true});
  }

  /*----------------------------
      navigation functions
    -----------------------------*/




  ///////////////////////////////
  ///////////////////////////////

  render() {
    return (
      <Container style={styles.container}>
        <GlobalKeys.Components.HeaderWSettings renderLeftButton={true}
          onLeftButtonPress={() => this.props.goBack()} title="Email Notification"
          headerBackgroundColor={GlobalKeys.Colors.jobDetailHeaderBGColor} 
          />

        <Form style={styles.form}>
        <View style={styles.groupHeaderContainer}>
              <Text style={styles.groupTitle}>Jobs</Text>
            </View>

          <View style={styles.groupOuterContainer}>
            <TouchableOpacity style={styles.settingItemContainer} onPress={() => this.switchJobsAlertOption()}>
              <Text style={styles.settingItemText}>Get Jobs Alert</Text>
              {this.getJobsAlertCheckBox()}
            </TouchableOpacity>
            <TouchableOpacity style={styles.settingItemContainer} onPress={() => this.switchJobsNewsOption()}>
              <Text style={styles.settingItemText}>Get News regarding companies, jobs and more</Text>
              {this.getJobsNewsCheckBox()}
            </TouchableOpacity>
          </View>

        </Form>

      </Container>
    );
  }
}

const mapStateToProps = state => ({
  jobList: state.jobList,
});

const mapDispatchToProps = dispatch => ({
  loadJobs: () => dispatch(loadJobs()),
  resetRoute: (route) => dispatch(resetRoute(route)),
  goBack: () => dispatch(goBack())
});

export default connect(mapStateToProps, mapDispatchToProps)(EmailNotifications);

