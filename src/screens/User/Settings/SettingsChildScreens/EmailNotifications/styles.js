import { MediaQueryStyleSheet } from 'react-native-responsive';
import Colors from '../../../../../constants/Colors';
import { Dimensions, Platform } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import Fonts from '../../../../../constants/Fonts';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = MediaQueryStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.emptyLine,
    alignItems: 'center',
    width:deviceWidth,

  },
  form:{
    alignItems: 'center',
    backgroundColor:Colors.emptyLine,
    // backgroundColor: 'blue',
  },
  profileContainer:{
    backgroundColor:Colors.white,
    width:deviceWidth,
    alignItems:'center',
    paddingBottom: deviceHeight * .02,
  },
  profilePicContainer:{
    width: deviceWidth * .13,
    height:deviceWidth * .13,
    // backgroundColor: 'red',
    marginTop: deviceHeight * .028,
  },
  profilePic:{
    height:'100%',
    width:'100%',
    // backgroundColor:'blue',
  },
  username:{
    fontSize: responsiveFontSize(1.7),
    fontFamily: Fonts.SEMIBOLD,
    color: Colors.appHeaderDarkColor,
    marginTop: deviceHeight * .023,
  },
  email:{
    fontSize: responsiveFontSize(.98),
    fontFamily: Fonts.SEMIBOLD,
    color: Colors.profileEmail,
    marginTop: deviceHeight * .0098,
  },
  cvButton:{
    width:deviceWidth * .35,
    height: deviceHeight * .04,
    borderColor: Colors.appPrimaryBlueColor,
    borderWidth: 1,
    backgroundColor:Colors.white,
    alignSelf: 'center',
    marginTop: deviceHeight * .019,
  },
  cvText:{
    fontSize: responsiveFontSize(1),
    color: Colors.appPrimaryBlueColor,
    textAlign:'center',
    flex:1,
    fontFamily: Fonts.BOLD,
  },
  groupOuterContainer:{
    width: deviceWidth * .86,
    alignItems: 'center',
    backgroundColor:Colors.emptyLine,
    marginTop: deviceHeight * .02,
    // marginBottom: deviceHeight * .02,
    // paddingTop: deviceHeight * .014,
    // paddingBottom: deviceHeight * .014,
  },
  groupContainer:{
    width: deviceWidth * .82,
    borderWidth:0,
    borderColor: Colors.appLightBorderColor,
    borderRadius: 5,
    // paddingTop: deviceHeight * .014,
    // paddingBottom: deviceHeight * .014,
    paddingLeft: deviceWidth * .026,
    paddingRight: deviceWidth * .017,
    // alignItems: 'center',
  },
  groupHeaderContainer:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    width:'100%',


    alignItems: 'center',
    // marginTop: deviceHeight * .0,
    paddingTop: deviceHeight * .01,
    // paddingBottom: deviceHeight * .005,
    marginBottom:deviceHeight * .01,
    // borderBottomWidth: .5,
    // borderBottomColor: Colors.blueColor,
    // backgroundColor:Colors.white,
    // paddingLeft: deviceWidth * .026,
    paddingRight: deviceWidth * .017,
  },
  infoContainer:{
    marginBottom: deviceHeight * .006
  },
  flatListStyle:{
    marginTop: deviceHeight * .012,
  },
  textInfoContainer:{
    marginBottom: deviceHeight * .006,
    marginTop: deviceHeight * .012,
    // maxHeight: deviceHeight * .05,    
  },
  groupTitle:{
    fontFamily: Fonts.SEMIBOLD,
    fontSize: responsiveFontSize(1.8),
    color: Colors.appHeaderDarkColor,
  },
  settingItemContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    alignItems: 'center',
    // marginTop: deviceHeight * .0,
    paddingTop: deviceHeight * .01,
    paddingBottom: deviceHeight * .01,
    marginBottom:deviceHeight * .005,
    // borderBottomWidth: .5,
    // borderBottomColor: Colors.blueColor,
    width:'100%',
    backgroundColor:Colors.white,
    paddingLeft: deviceWidth * .026,
    paddingRight: deviceWidth * .017,
  },
  settingItemText:{
    fontFamily: Fonts.LIGHT,
    fontSize: responsiveFontSize(1.6),
    color: Colors.appPrimaryBlueColor,
  },
  forwardIcon:{
    fontSize: responsiveFontSize(2.2),
    color: Colors.appPrimaryBlueColor,
  },
});

export default styles;