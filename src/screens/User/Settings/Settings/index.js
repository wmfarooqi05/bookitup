/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  Platform, FlatList, ImageBackground, TextInput,
  StyleSheet, Alert, Image, TouchableOpacity,
  View, Dimensions, Text, StatusBar, Modal as RNModal,
  KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';

import {
  Container, Header, Left, Body, Right, Button, Icon,
  Title, Content, Input, Item, Form, Text as NBText
} from 'native-base';
import { navigate, resetRoute } from '../../../../redux/actions/nav';

import GlobalKeys from '../../../../constants';
import styles from './styles';

import Icons from '../../../../constants/Icons';
import Images from '../../../../constants/Images';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

class UserProfileView extends Component<Props> {

  /*----------------------------
      state cycle functions
    -----------------------------*/
  constructor(props) {
    super(props);
    this.state = {
      educationInfo: [
        {
          id: 1,
          title: 'Universty Degree',
          duration: '2008 - 2012',
          companyName: 'Universty of California, Los Angeles',
        },
      ],
      workInfo: [
        {
          id: 1,
          title: 'React Native Developer',
          duration: '2016 - Today',
          companyName: 'Lorizzle',
        },
        {
          id: 1,
          title: 'Android Developer',
          duration: '2012 - 2016',
          companyName: 'PICKSUM IPSUM',
        },
      ],
      cvAllowOption: false,
      personalStatement: 'A highly motivated and hardworking individual, who has recently completed their A-Levels, achieving excellent grades in both Maths and Science. Seeking an apprenticeship in the engineering industry to build upon a keen scientific interest and start a career as a maintenance engineer. Eventual career goal is to become a fully-qualified and experienced maintenance or electrical engineer, with the longer-term aspiration of moving into project management.',
    };
  }

  componentDidMount() {
  }




  /*----------------------------
      helper functions
    -----------------------------*/

  /*----------------------------
      network calls
    -----------------------------*/

  /*----------------------------
      render functions
    -----------------------------*/

  renderListItemInfo(item) {
    console.log(item);
    return (
      <View style={styles.infoContainer}>
        <Text style={styles.groupSecondaryTitle}>{item.title}</Text>
        <Text style={styles.groupSubHeader}>{item.duration}</Text>
        <Text style={styles.groupHeader}>{item.companyName}</Text>
      </View>
    )
  }
  renderTextInfo(item) {
    return (
      <View style={styles.textInfoContainer}>
        <Text>
          <Text style={styles.groupHeader}>{item}</Text>
          {/* {item.length > 250 ? 
          <Text>Show More</Text>
          : null
        } */}
        </Text>
      </View>
    )
  }

  /*----------------------------
      state render helper functions
    -----------------------------*/
  truncateText(text) {
    if (text.length > 250) {
      return text.substring(0, 250);
    } else return text;
  }

  getCVIcon(){
    if (this.state.cvAllowOption) {
      return(
        <Icon style={styles.forwardIcon} name="ios-checkbox-outline"></Icon>
      );
    } else {
      return(
        <Icon style={styles.forwardIcon} name="ios-square-outline"></Icon>
      );
    }
  }


  /*----------------------------
      state changing functions
    -----------------------------*/

    switchCVOption(){
      if (this.state.cvAllowOption)
        this.setState({cvAllowOption: false});
      else
        this.setState({cvAllowOption: true});
    }

  /*----------------------------
      navigation functions
    -----------------------------*/




  ///////////////////////////////
  ///////////////////////////////

  render() {
    return (
      <Container style={styles.container}>
        <GlobalKeys.Components.HeaderWSettings renderLeftButton={false}
          onRightButtonPress={() => Alert.alert('a')} title="Settings"
          headerBackgroundColor={GlobalKeys.Colors.jobDetailHeaderBGColor} />

        <Form style={styles.form}>

          <View style={styles.groupOuterContainer}>
            <View style={styles.groupHeaderContainer}>
              <Text style={styles.groupTitle}>User Details</Text>
            </View>
            <TouchableOpacity style={styles.settingItemContainer} onPress={ () => this.props.navigate({routeName: GlobalKeys.screens.AppNotifications})}>
              <Text style={styles.settingItemText}>App Notification Setting</Text>
              <Icon style={styles.forwardIcon} name="arrow-forward"></Icon>
            </TouchableOpacity>
            <TouchableOpacity style={styles.settingItemContainer} onPress={ () => this.props.navigate({routeName: GlobalKeys.screens.EmailNotifications})}>
              <Text style={styles.settingItemText}>Email Notification Setting</Text>
              <Icon style={styles.forwardIcon} name="arrow-forward"></Icon>
            </TouchableOpacity>
            <TouchableOpacity style={styles.settingItemContainer} onPress={ () => this.props.navigate({routeName: GlobalKeys.screens.ChangePassword})}>
              <Text style={styles.settingItemText}>Change Password</Text>
              <Icon style={styles.forwardIcon} name="arrow-forward"></Icon>
            </TouchableOpacity>

          </View>

          <View style={styles.groupOuterContainer}>
            <View style={styles.groupHeaderContainer}>
              <Text style={styles.groupTitle}>Feedback</Text>
            </View>
            <TouchableOpacity style={styles.settingItemContainer} onPress={ () => Alert.alert("No yet uploaded") }>
              <Text style={styles.settingItemText}>Rate the app</Text>
              <Icon style={styles.forwardIcon} name="arrow-forward"></Icon>
            </TouchableOpacity>
            <TouchableOpacity style={styles.settingItemContainer}>
              <Text style={styles.settingItemText}>Read Privacy Policy</Text>
              <Icon style={styles.forwardIcon} name="arrow-forward"></Icon>
            </TouchableOpacity>
          </View>

          <View style={styles.groupOuterContainer}>
            <View style={styles.groupHeaderContainer}>
              <Text style={styles.groupTitle}>CV Search</Text>
            </View>
            <TouchableOpacity style={styles.settingItemContainer} onPress={() => this.switchCVOption()}>
              <Text style={styles.settingItemText}>Allow Recruits to search your CV</Text>
              {this.getCVIcon()}
            </TouchableOpacity>
          </View>

          <View style={styles.groupOuterContainer}>
            <View style={styles.groupHeaderContainer}>
              <Text style={styles.groupTitle}>App Settings</Text>
            </View>
            {/* <View style={styles.settingItemContainer}>
              <Text style={styles.settingItemText}>Send us Feedback</Text>
              <Icon style={styles.forwardIcon} name="arrow-forward"></Icon>
            </View> */}
            {/* <View style={styles.settingItemContainer}>
              <Text style={styles.settingItemText}>Share the app</Text>
              <Icon style={styles.forwardIcon} name="arrow-forward"></Icon>
            </View> */}
            <TouchableOpacity style={styles.settingItemContainer} onPress={ () => this.props.resetRoute(GlobalKeys.screens.GettingStarted)}>
              <Text style={styles.settingItemText}>Signout</Text>
              {/* <Icon style={styles.forwardIcon} name="arrow-forward"></Icon> */}
            </TouchableOpacity>
          </View>
        </Form>

      </Container>
    );
  }
}

const mapStateToProps = state => ({
  jobList: state.jobList,
});

const mapDispatchToProps = dispatch => ({
  loadJobs: () => dispatch(loadJobs()),
  navigate: (route) => dispatch(navigate(route)),
  resetRoute: (route) => dispatch(resetRoute(route)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileView);

