import React, { Component, ReactNode } from 'react';
import SafeAreaView from 'react-native-safe-area-view';
import { View, Text, Image, ImageBackground, TextInput } from 'react-native';
import { Container, Content, Form } from 'native-base';
import Types from 'Types';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { navigate, resetRoute, ROUTE_TYPE } from '../../../../redux/actions/nav';

import Images from '../../../../constants/Images';
import Icons from '../../../../constants/Icons';
import styles from './styles';
import ButtonFowardIcon from '../../../../components/ButtonFowardIcon';
import Header from '../../../../components/Header';
import TextInputBIU from '../../../../components/TextInputBIU';

const defaultProps = Object.freeze({
});

type OwnProps = typeof defaultProps;
type StateProps = typeof defaultStateProps;
type Props = StateProps & DispProps & OwnProps;

type State = {
  phone:string,
  email:string,
};

class ForgotPassword extends Component<Props, State> {

  subtitle = "Recover your password using your email or phone number. We will send you a recovery code. Please insert one of them below.";

  constructor(props){
    super(props);
    this.state = { 
      email: '',
      phone: '',
    };
  };

  handleBackPress():void{

  }

  render(): React.ReactNode{
    return(
      <Container>
        <ImageBackground source={Images.gradientBackground} style={styles.container}>
          <SafeAreaView style={{flex:1, }}>
            <Header onPress={() => this.handleBackPress()} renderBackIcon={true}/>
            <Form style={styles.form}>
              <Text style={styles.title}>Forgot your password</Text>

              <Text style={styles.subtitle}>{this.subtitle}</Text>
              <View style={styles.buttonContainer}>
                <TextInputBIU style={styles.input} placeholder="Email"
                  onChangeText={(text) => this.setState({email:text})}
                  // onSubmitEditing={()=>this.onLogin()} 
                  // ref={child => {this.password = child}} 
                  // returnKeyType="go"
                />
              <Text style={styles.or}>or</Text>
              <TextInputBIU style={styles.input} placeholder="Phone"
                onChangeText={(text) => this.setState({phone:text})}
                secureTextEntry={true}
                // onSubmitEditing={()=>this.onLogin()} 
                // ref={child => {this.password = child}} 
                // returnKeyType="go"
              />
              </View>
            </Form>
            <ButtonFowardIcon 
              style={styles.sendButton} title="Send"/>
          </SafeAreaView>
        </ImageBackground>
      </Container>
    )
  }
}



const defaultStateProps = {
}

type DispProps = {

}

const mapStateToProps = (state: Types.RootState): StateProps => {
  return defaultStateProps;
};

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>): DispProps => ({
  navigate: (route) => dispatch(navigate(route)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPassword);

