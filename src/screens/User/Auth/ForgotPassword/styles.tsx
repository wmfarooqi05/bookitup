import { MediaQueryStyleSheet } from 'react-native-responsive';
import Colors from '../../../../constants/Colors';
import { Dimensions, Platform, ViewStyle, ImageStyle, TextStyle } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import Fonts from '../../../../constants/Fonts';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

type Style = {
  container?:ViewStyle,
  form?: ViewStyle,
  title?: TextStyle,
  subtitle?: TextStyle,
  buttonContainer?: ViewStyle,
  button?: ViewStyle,
  or?: TextStyle,
  inputContainer?:ViewStyle,
  input: TextStyle,
}

const styles = MediaQueryStyleSheet.create<Style>({
 container:{ 
   width: deviceWidth, 
   height: deviceHeight 
  },
  form:{
    marginTop: deviceHeight * .02,
    width: deviceWidth * .85,
    alignSelf: 'center',
    justifyContent:'center',
  },
  title:{
    fontFamily: Fonts.LIGHT,
    textAlign:'center',
    color: Colors.white,
    ...ifIphoneX({
      fontSize: responsiveFontSize(2.8),
    }, {
      fontSize: responsiveFontSize(3.3),
    }),
  },
  subtitle:{
    fontFamily: Fonts.LIGHT,
    textAlign:'center',
    color: Colors.white,
    marginTop: deviceHeight * .07,
    ...ifIphoneX({
      fontSize: responsiveFontSize(1.9),
    }, {
      fontSize: responsiveFontSize(2.4),
    }),
  },
  buttonContainer:{
    marginTop: deviceHeight *.1,
    alignSelf: 'center',
  },
  button:{
    width: deviceWidth * .72,
  },

  inputContainer:{
    // marginTop: deviceHeight * .027,
    flexDirection:'row',
    width: deviceWidth * .73,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent:'center',
    borderWidth: .3,
    height: deviceHeight * .067,
    borderRadius: 4,
    borderColor: Colors.white,
    // backgroundColor:'blue',
  },
  input:{
    // backgroundColor: 'red',
    flex:1,
    marginHorizontal: deviceWidth*.05,
    fontSize: responsiveFontSize(2.2),
    fontFamily: Fonts.LIGHT,
    color: Colors.white,
    alignSelf: 'center',
    textAlign:'center',
    // backgroundColor:'red',
    // marginLeft: 10,
    // marginRight: 10,
  },

  or :{
    fontSize: responsiveFontSize(2.2),
    fontFamily: Fonts.LIGHT,
    paddingVertical: deviceHeight * .02,
    color: Colors.white,
    alignSelf: 'center',
  },
  sendButton:{
    marginTop: deviceHeight * .18,
    alignSelf: 'flex-end',
    width: deviceWidth * .37,
    marginRight: deviceWidth * .13,
  },
});

export default styles;