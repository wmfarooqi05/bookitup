import { MediaQueryStyleSheet } from 'react-native-responsive';
import Colors from '../../../../constants/Colors';
import { Dimensions, Platform, ViewStyle, ImageStyle, TextStyle } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import Fonts from '../../../../constants/Fonts';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

type Style = {
  container?:ViewStyle,
  inputContainer?: ViewStyle,
  logoHolo?: ImageStyle,
  inputIcon?: ImageStyle,
  input?: TextStyle,
  lockIcon?: ImageStyle,
  overlay?: ViewStyle,
  activityIndicator?: ViewStyle, 
}

const styles = MediaQueryStyleSheet.create<Style>({
 container:{ 
   width: deviceWidth, 
   height: deviceHeight 
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 100,
    backgroundColor: Colors.white,
    opacity: 0.6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  activityIndicator: {
    alignSelf: 'center'
  },
  logoHolo:{
    width: deviceHeight * .254 * .56,
    height: deviceHeight * .254,
    marginTop: deviceHeight * .07,
    alignSelf: 'center',
  },
  form:{
    marginTop: deviceHeight * .068,
    width: deviceWidth * .73,
    alignSelf: 'center',
  },
  inputContainer:{
    marginTop: deviceHeight * .027,
    flexDirection:'row',
    width: deviceWidth * .73,
    alignSelf: 'center',
    alignItems: 'center',
    borderWidth: .3,
    height: deviceHeight * .067,
    borderRadius: 4,
    borderColor: Colors.white,
  },
  inputIcon:{
    width: deviceWidth * .076,
    height: deviceWidth * .054,
    marginLeft: deviceWidth * .04,
    marginRight: deviceWidth * .097,
  },
  input:{
    // backgroundColor: 'red',
    ...ifIphoneX({
      width: deviceWidth * .52,
      marginLeft: deviceWidth * -.05,
    }, {
      width: deviceWidth * .47,
    }),
    fontSize: responsiveFontSize(2.2),
    fontFamily: Fonts.LIGHT,
    color: Colors.white,
  },
  lockIcon:{
    width: deviceWidth * .076,
    height: deviceWidth * .07,
    marginLeft: deviceWidth * .04,
    marginRight: deviceWidth * .097,
    alignSelf: 'center',
    // backgroundColor:'red',
  },

  loginContainer:{
    marginTop: deviceHeight * .024,
    flexDirection:'row',
    width: deviceWidth * .37,
    alignSelf: 'flex-end',
    alignItems: 'center',
    justifyContent:'space-between',
    borderWidth: .3,
    borderColor: Colors.white,
    height: deviceHeight * .067,
    borderRadius: 4,
    paddingHorizontal: deviceWidth *.056,
  },
  loginText:{
    // backgroundColor: 'red',
    fontSize: responsiveFontSize(2.2),
    fontFamily: Fonts.LIGHT,
    color: Colors.white,
    // marginLeft: deviceWidth *.056,
  },
  forwardIcon: {
    // position:'absolute',
    // right: deviceWidth * .03,
    height: deviceHeight * .038,
    resizeMode:"contain",
    // backgroundColor:'blue',
    width: deviceWidth*.04,
  },
});

export default styles;