/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component, ReactNode, ReactElement } from 'react';
import {
  ScrollView, TextInput, Image, TouchableOpacity, Keyboard, ImageBackground,
  View, Dimensions, Text, Modal as RNModal, ActivityIndicator,
  KeyboardAvoidingView, TouchableWithoutFeedback, TextInputProperties, TextInputProps, Alert,
} from 'react-native';

import SafeAreaView from 'react-native-safe-area-view';

import { connect } from 'react-redux';
import { Container, Header, Left, Body, Right, Button, Icon,
  Title, Content, Input,Item, Form, Text as NBText
 } from 'native-base';
import DropdownAlert from 'react-native-dropdownalert';
import { isEmpty } from 'lodash';

import Types from 'Types';
import { Dispatch } from 'redux';
import { CURR_USER_TYPE, INITIAL_USER } from '../../../../redux/reducers/auth';
import { navigate, resetRoute, ROUTE_TYPE } from '../../../../redux/actions/nav';

import styles from './styles';
import Validator from '../../../../Utils/Validator';
import Icons from '../../../../constants/Icons';
import Images from '../../../../constants/Images';
import Colors from '../../../../constants/Colors';

import {
  requestLocalSignIn,
  requestFacebookSignIn,
} from '../../../../redux/actions/auth';
import TextInputBIU from '../../../../components/TextInputBIU';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const defaultProps = Object.freeze({
});

type OwnProps = typeof defaultProps;
type StateProps = typeof defaultStateProps;
type Props = StateProps & DispProps & OwnProps;

type State = {
  email: string,
  password: string,
  disableInput: boolean,
  requestInProgress: boolean,
  enableButton: boolean,
  scrollEnabled: boolean,
};

class SignIn extends Component<Props, State> {

subHeadline = "We will send you the latest jobs with 2 daily notifications. You can change his within your settings";


//////////// REFS /////////////

private dropdownRef = React.createRef();
// private password: React.LegacyRef<TextInput>;// = React.createRef<React.LegacyRef<TextInput>>();
private email = React.createRef();
private password: React.LegacyRef<TextInput> = React.createRef<TextInput>();


/////////// Life cycle Methods ////////////

  constructor(props){
    super(props);
    this.state = { 
      email: '',
      password: '',
      disableInput: false,
      requestInProgress: false,
      enableButton: false,
      scrollEnabled: false,
    };
    this.onLogin = this.onLogin.bind(this);
    this.handleEmail = this.handleEmail.bind(this);
    this.keyboardDidShow = this.keyboardDidShow.bind(this);
    this.keyboardDidHide = this.keyboardDidHide.bind(this);
    // this.dropdownRef = React.createRef<React.ReactNode>();
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  focusPassword(){
    const node = this.password;
    try {
      if (node) {
        if (typeof node.focus == "function")
          node.focus();
        else {
          if (typeof child.wrappedInstance.focus == 'function') {
            this.email = child.wrappedInstance;
          }
        }
      }
    } catch (e) { }
    // this.password.focus();
  }

  componentDidMount() {
    
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }


  shouldComponentUpdate (nextProps, nextState): boolean {
    const { currentUser, hasLoginAfterEffectsCompleted, errorMessages } = this.props;

    if (errorMessages !== nextProps.errorMessages) {
      if (!isEmpty(nextProps.errorMessages)) {
        const { email, password } = nextProps.errorMessages;
        this.setState({ requestInProgress: false });
        this.dropdownRef.alertWithType('error', 'Error!', email || password || JSON.stringify(nextProps.errorMessages.message));
        return true;
      }
    }

    if (hasLoginAfterEffectsCompleted !== nextProps.hasLoginAfterEffectsCompleted) {
      if (!isEmpty(nextProps.currentUser.token)) {
        // if (currentUser && currentUser.movieProfile.onboarding) {
        //   return this.props.resetRoute({ routeName: 'TasteCalculationModal' });
        // }
        this.props.resetRoute(null);
        return true;
      }
    }
    return true;
  }

  ////////////// Ref Functions ////////////////////

  // dropdownCallback(ref) {
  //   this.dropdownRef = ref;
  // }

  ////////////// General Global Functions //////////////

  keyboardDidShow() {
    const { scrollEnabled } = this.state;
    if (!scrollEnabled) {
      this.setState({ scrollEnabled: true });
    }
  }

  keyboardDidHide() {
    const { scrollEnabled } = this.state;
    if (scrollEnabled) {
      this.setState({ scrollEnabled: false });
    }
  }

  ///////////// User-Action Handling Function ////////////


  goToNextScreen(route){
    this.props.navigate(route);
  }

  /////////  State helper functions ///////////

  onLogin(): void {
    // return true;
    const { email, password } = this.state;

    if (Validator.email(email))
    {
      if (!isEmpty(password)){
        if (password.trim().length >= 5){
          this.setState({ requestInProgress: true });
          Keyboard.dismiss();
          return this.props.requestLocalSignIn({ email, password });
        } else {
          Alert.alert('Password must be atleast 5 characters long');
        }
      } else {
        Alert.alert('Enter Passowrd')
      }
    } else {
      Alert.alert('Enter Valid Email');
    }
  }

  handleEmail(emailText) {
    return this.setState({
      email: emailText,
      // enableButton: this.shouldEnableButton(),
    });
  }

  handlePassword(passwordText) {
    return this.setState({
      password: passwordText,
      // enableButton: this.shouldEnableButton(),
    });
  }

  //////////////   Render Function /////////////////

  render(): React.ReactNode {
    const { requestInProgress, scrollEnabled } = this.state;
    return (
      <Container>
      <ImageBackground source={Images.gradientBackground} style={styles.container}>
        {requestInProgress && (
        <View style={styles.overlay}>
          <ActivityIndicator style={styles.activityIndicator} size="large" color={Colors.blueColor}/>
        </View>
        )}
        <SafeAreaView style={{flex:1, justifyContent:'space-between'}}>
          <View>
          <Image source={Images.logoHolo} style={styles.logoHolo}/>
          <Form style={styles.form}>
            <TextInputBIU placeholder="Email"
                containerStyle={{marginTop: deviceHeight * .027}}
                onChangeText={(text) => this.handleEmail(text)}
                onSubmitEditing={()=>this.focusPassword()} 
                returnKeyType={"next"} showIcon={true} iconName={Icons.email}/>
            {/* <View style={styles.inputContainer}>
              <Image source={Icons.email} style={styles.inputIcon} resizeMode="contain"/>
              <TextInput style={styles.input} 
              placeholder="Email"
                onChangeText={(text) => this.handleEmail(text)}
                onSubmitEditing={()=>this.focusPassword()} 
                returnKeyType={"next"}
              />
            </View> */}
            <TextInputBIU 
              containerStyle={{marginTop: deviceHeight * .027}}
              placeholder="Password"
              onChangeText={(text) => this.handlePassword(text)}
              secureTextEntry={true}
              onSubmitEditing={()=>this.onLogin()} 
              ref={(child)=> {this.password = child}} 
              returnKeyType="go"
              showIcon={ } iconName={Icons.lock}
            />
            <TouchableOpacity style={styles.loginContainer} onPress={this.onLogin.bind(this)}>
              <Text style={styles.loginText}>Login</Text>
              <Image source={Icons.iconForward} style={styles.forwardIcon} resizeMode="contain"/>
            </TouchableOpacity>
          </Form>
          </View>
          <TouchableOpacity style={{alignSelf:'center', marginBottom:deviceHeight *.01}}
          >
            <Text style={styles.loginText}>Forgot Your passowrd?</Text>
          </TouchableOpacity>

        </SafeAreaView>
      </ImageBackground>
      <DropdownAlert ref={child => {this.dropdownRef = child}} />
      </Container>
      // <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
      //   <View style={{ flex: 1 }}>
      //     {requestInProgress && (
      //       <View style={styles.overlay}>
      //         <ActivityIndicator style={styles.activityIndicator} size="large" color={Colors.appPrimaryBlueColor}/>
      //       </View>
      //     )}
      // <ScrollView scrollEnabled={scrollEnabled} style={styles.container}>
      // <KeyboardAvoidingView style={styles.innerContainer} behavior="position">
      //   <View style={styles.contentContainer}>
      //   <View style={styles.logoContainer}>
      //     <Image style={styles.logo} source={Images.NewLogo} resizeMode="contain"/>
      //   </View>
      //   <Text style={styles.headline}>Login</Text>
      //   <Item style={styles.inputContainer} regular>
      //     <Image style={styles.inputIcon} resizeMode="contain" source={Icons.avatarIcon}/>
      //     <TextInput 
      //       style={styles.input} 
      //       onSubmitEditing={()=>this.focusPassword()} 
      //       placeholderTextColor = {Colors.signUpLightBorder} 
      //       placeholder="Email Address"
      //       ref={(input: TextInput) => {
      //         console.log(input);
      //         this.email = input;
      //       }}
      //       editable={!this.state.disableInput}
      //       autoCapitalize="none"
      //       keyboardType="email-address"
      //       multiline={false}
      //       onChangeText={text => this.handleEmail(text)}
      //     />
      //   </Item>
      //   <Item style={styles.inputContainer} regular>
      //     <Image style={styles.inputIcon} resizeMode="contain" source={Icons.avatarIcon}/>
      //     <TextInput 
      //       ref={child => {this.password = child}} 
      //       style={styles.input} 
      //       placeholderTextColor={Colors.signUpLightBorder} 
      //       placeholder="Password"
      //       editable={!this.state.disableInput}
      //       autoCapitalize="none"
      //       secureTextEntry
      //       multiline={false}
      //       onChangeText={text => this.handlePassword(text)}
      //     />
      //   </Item>
      //   <TouchableOpacity>
      //     <NBText style={styles.forgotText}>Forgot Password?</NBText>
      //   </TouchableOpacity>
      // {/* </KeyboardAvoidingView> */}
      //   <Button style={styles.nextButton} 
      //     disabled={!this.state.enableButton} 
      //     onPress={this.onLogin}
      //   >
      //     <NBText style={styles.nextText}>Login</NBText>
      //   </Button>

      //   <Text style={styles.newMember}>New Member?</Text>
      //   <TouchableOpacity onPress={() => this.goToNextScreen({routeName: screens.SignUp})}>
      //     <NBText style={styles.createAccount}>CREATE ACCOUNT</NBText>
      //   </TouchableOpacity>
      // {/* </View> */}
      //         {/* <DropdownAlert
      //           ref={this.dropdownCallback.bind(this)}
      //         /> */}
      //         </View>
      //       </KeyboardAvoidingView>
      //     </ScrollView>
      //     <DropdownAlert ref={this.dropdownRef} />
      //   </View>
      // </TouchableWithoutFeedback>
    );
  }
}

const defaultStateProps = {
  errorMessages: {} as any,
  currentUser: INITIAL_USER as CURR_USER_TYPE,
  hasLoginAfterEffectsCompleted: true as boolean
  // mapStateToProps objects
}

type DispProps = {
  navigate: (any) => void,
  resetRoute: (ROUTE_TYPE) => void,
  requestLocalSignIn: (any) => void
}

const mapStateToProps = (state: Types.RootState): StateProps => {
  const {
    auth: {
      errorMessages,
      currentUser,
      hasLoginAfterEffectsCompleted,
    },
  } = state;

  return {
    errorMessages,
    currentUser,
    hasLoginAfterEffectsCompleted,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>) : DispProps => ({
  navigate: (route) => dispatch(navigate(route)),
  resetRoute: (route) => dispatch(resetRoute(route)),
  requestLocalSignIn: (payload) => dispatch(requestLocalSignIn(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);

