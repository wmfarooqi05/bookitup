/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component, LegacyRef } from 'react';
import {
  Platform, FlatList, ImageBackground, TextInput, Keyboard,
  StyleSheet, Alert, Image, TouchableOpacity, ActivityIndicator,
  View, Dimensions, Text, StatusBar, Modal as RNModal, ViewStyle,
  KeyboardAvoidingView, ScrollView, TextStyle, TextInputComponent,
} from 'react-native';
import { connect } from 'react-redux';
import ModalDropdown from 'react-native-modal-dropdown';

import {
  Container, Left, Body, Right, Button, Icon,
  Title, Content, Input, Item, Form, Text as NBText
} from 'native-base';
import ImagePicker from 'react-native-image-picker';
import DropdownAlert from 'react-native-dropdownalert';
import { isEmpty } from 'lodash';
import PropTypes from 'prop-types';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { Dropdown } from 'react-native-material-dropdown';

import { CURR_USER_TYPE, INITIAL_USER } from '../../../../redux/reducers/auth';

import Types from 'Types';
import { Dispatch } from 'redux';
import { navigate, resetRoute, goBack, ROUTE_TYPE } from '../../../../redux/actions/nav';
import Validator from '../../../../Utils/Validator';
import { signup } from '../../../../redux/actions/auth';

import Colors from '../../../../constants/Colors';
import Components from '../../../../constants/Components';
import styles from './styles';

import Icons from '../../../../constants/Icons';
import Images from '../../../../constants/Images';
import SafeAreaView from 'react-native-safe-area-view';
import ButtonFowardIcon from '../../../../components/ButtonFowardIcon';
import HOCWithProgressIndicator from '../../../../components/ProgressIndicator';
import Header from '../../../../components/Header';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const defaultProps = Object.freeze({
});

type OwnProps = typeof defaultProps;
type StateProps = typeof defaultStateProps;
type Props = StateProps & DispProps & OwnProps;

type State = {
  avatarSource: string | null,
  firstName: string,
  lastName: string,
  email: string,
  password: string,
  enableButton: boolean,
  disableInput: boolean,
  showLoader: boolean,
  scrollEnabled: boolean,
  selectedGender: string,
};

type Genders = 'Male' | 'Female';

class SignUp extends Component<Props, State> {

  //////////// REFS /////////////

  private email = React.createRef();
  private password = React.createRef();
  private passwordAgain = React.createRef();
  private birthday = React.createRef();
  private phone = React.createRef();


  constructor(props) {
    super(props);
    this.state = {
      // avatarSource: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQfRGquFy720UJfDdDHTPRbR5KUW2fhSybpcCLt6EIfb0nUPJrYpA',
      avatarSource: null,
      firstName: '',
      lastName: '',
      email: '',
      password: '',
      enableButton: false,
      disableInput: false,
      showLoader: false,
      scrollEnabled: false,
      selectedGender: 'Male',
    };

    this.onCancel = this.onCancel.bind(this);
    this.onSaveProfile = this.onSaveProfile.bind(this);
    // this.renderSaveProfileButton = this.renderSaveProfileButton.bind(this);
    this.keyboardDidShow = this.keyboardDidShow.bind(this);
    this.keyboardDidHide = this.keyboardDidHide.bind(this);
    this.onPressSignUp = this.onPressSignUp.bind(this);
    this.shouldEnableButton = this.shouldEnableButton.bind(this);
  }

  /////////// Life cycle Methods ////////////

  componentDidMount() {
  }

  componentWillMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this.keyboardDidShow);
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this.keyboardDidHide);
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  shouldComponentUpdate(nextProps, nextState): boolean {
    const { hasLoginAfterEffectsCompleted, errorMessages } = this.props;

    if (errorMessages !== nextProps.errorMessages) {
      if (!isEmpty(nextProps.errorMessages)) {
        const { firstName, lastName, email, password } = nextProps.errorMessages;
        this.setState({ showLoader: false });
        this.dropdown.alertWithType('error', 'Error!', firstName || lastName || email || password ||
          JSON.stringify(nextProps.errorMessages.error.toString()));
        return true;
      }
    }

    if (hasLoginAfterEffectsCompleted !== nextProps.hasLoginAfterEffectsCompleted) {
      if (!isEmpty(nextProps.currentUser.token)) {
        this.props.resetRoute(null); // Home        
        return true;

      }
    }
    return true;

  }

  goToNextScreen(route) {
    this.props.navigate(route);
  }

  goToHome() {
    this.props.resetRoute();
  }
  ////////////// Ref Functions ////////////////////

  dropdownCallback(ref) {
    this.dropdown = ref;
  }

  ////////////// General Global Functions //////////////


  shouldEnableButton() {
    const { email, password } = this.state;
    return Validator.email(email) && (password && password.length >= 5);
  }

  ///////////// User-Action Handling Function ////////////


  onCancel() {
    this.props.goBack();
  }

  keyboardDidShow() {
    const { scrollEnabled } = this.state;
    if (!scrollEnabled) {
      this.setState({ scrollEnabled: true });
    }
  }

  keyboardDidHide() {
    const { scrollEnabled } = this.state;
    if (scrollEnabled) {
      this.setState({ scrollEnabled: false });
    }
  }

  onSaveProfile() {
    const { firstName, lastName, email, password } = this.state;
    this.setState({ showLoader: true });
    Keyboard.dismiss();
    return this.props.signup({ firstName, lastName, email, password });
  }

  onPressSignUp() {
    const { email, password, firstName, lastName } = this.state;
    this.setState({ requestInProgress: true });
    Keyboard.dismiss();
    return this.props.signup({ firstName, lastName, email, password, firstName, userTypeName: 'JobUser' });
  }

  onPressGender() { }


  /////////  Refs helper functions ///////////

  focusEmail(): void {
    try {
      this.email.focus();
    } catch (e) { }
  }
  focusPassword(): void {
    try {
      this.password.focus();
    } catch (e) { }
  }
  focusPasswordAgain(): void {
    try {
      this.passwordAgain.focus();
    } catch (e) { }
  }
  focusPasswordBirthday(): void {
    try {
      this.birthday.focus();
    } catch (e) { }
  }
  focusPasswordPhone(): void {
    try {
      this.phone.focus();
    } catch (e) { }
  }

  emailRef(child) {
    try {
      if (child) {
        if (typeof child.focus == "function")
          this.email = child;
        else {
          if (typeof child.wrappedInstance.focus == 'function') {
            this.email = child.wrappedInstance;
          }
        }
      }
    } catch (e) { }
  }

  passwordRef(child){
    try {
      if (child) {
        if (typeof child.focus == "function")
          this.password = child;
        else {
          if (typeof child.wrappedInstance.focus == 'function') {
            this.password = child.wrappedInstance;
          }
        }
      }
    } catch (e) { }
  }

  passwordAgainRef(child){
    try {
      if (child) {
        if (typeof child.focus == "function")
          this.passwordAgain = child;
        else {
          if (typeof child.wrappedInstance.focus == 'function') {
            this.passwordAgain = child.wrappedInstance;
          }
        }
      }
    } catch (e) { }
  }

  birthdayRef(child){
    try {
      if (child) {
        if (typeof child.focus == "function")
          this.birthday = child;
        else {
          if (typeof child.wrappedInstance.focus == 'function') {
            this.birthday = child.wrappedInstance;
          }
        }
      }
    } catch (e) { }
  }

  phoneRef(child){
    try {
      if (child) {
        if (typeof child.focus == "function")
          this.phone = child;
        else {
          if (typeof child.wrappedInstance.focus == 'function') {
            this.phone = child.wrappedInstance;
          }
        }
      }
    } catch (e) { }
  }
  /////////  State helper functions ///////////
  //////////////   Render Function /////////////////


  categoryDrpFrame(style) {
    style.height = deviceHeight * .052 * 2;
    return style;
  }

  render() {
    const { showLoader, scrollEnabled } = this.state;
    let genderTypes = ["Male","Female"];

  
    return (
      <Container style={styles.container as ViewStyle}>
          <HOCWithProgressIndicator showLoading ={showLoader}>
        <ImageBackground source={Images.gradientBackground} style={styles.background}>
          <Content scrollEnabled={scrollEnabled} style={styles.scrollView as ViewStyle} >
            <SafeAreaView>
              <Header/>
              <Form style={styles.form as ViewStyle}>
                <Input placeholder="Full name" placeholderTextColor={Colors.white} style={styles.input as TextStyle}
                  onSubmitEditing={() => this.focusEmail()}
                />
                <Input placeholder="Email" 
                  ref={(child)=> {this.emailRef(child)}}
                  onSubmitEditing={() => this.focusPassword()}
                  placeholderTextColor={Colors.white} style={styles.input as TextStyle} />

                <Input placeholder="Password" 
                  ref={(child: React.RefObject<TextInput> )=> {this.passwordRef(child)}} 
                  onSubmitEditing={() => this.focusPasswordAgain()}
                  placeholderTextColor={Colors.white}
                  style={styles.input as TextStyle} secureTextEntry={true} />
                <Input placeholder="Password Again" ref={(child)=> {this.passwordAgainRef(child)}}

                  onSubmitEditing={() => this.focusPasswordBirthday()}
                  placeholderTextColor={Colors.white}
                  style={styles.input as TextStyle} secureTextEntry={true} />

                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }}>
                <ModalDropdown options={genderTypes} textStyle={styles.buttonText}
                  dropdownStyle={styles.dropdownContainer} dropdownTextStyle={styles.dropdownText}
                  adjustFrame={style => this.categoryDrpFrame(style)} 
                  defaultIndex={0}
                  onSelect={(idx, value) => this.setState({selectedGender:value})}
                >
                  <View style={styles.buttonContainer}>
                    <Text style={styles.buttonText} numberOfLines={1}>{this.state.selectedGender}</Text>
                    <Image source={Icons.iconDownward} style={styles.downIcon} />
                  </View>
                </ModalDropdown>
                  {/* <TouchableOpacity style={styles.buttonContainer}
                    onPress={this.onPressGender.bind(this)}
                  >
                    <Text style={styles.buttonText}>Gender</Text>
                    <Image source={Icons.iconDownward} style={styles.downIcon} />
                  </TouchableOpacity> */}
                  <TouchableOpacity style={styles.buttonContainer}
                    onPress={this.onPressGender.bind(this)}
                  >
                    <Text style={styles.buttonText}>Estonia</Text>
                    <Image source={Icons.estonia} style={styles.downIcon} />
                  </TouchableOpacity>
                </View>
                <Input placeholder="Birthday (dd/mm/yyyy)" ref={(child)=> {this.birthdayRef(child)}}
                  onSubmitEditing={() => this.focusPasswordPhone()}
                  placeholderTextColor={Colors.white} style={[styles.input as TextStyle, { marginTop: deviceHeight * -.005 }]} />
                <Input placeholder="Phone number" ref={(child)=> {this.phoneRef(child)}}
                  onSubmitEditing={this.onPressSignUp}
                  placeholderTextColor={Colors.white} style={styles.input as TextStyle} />

                <ButtonFowardIcon
                  style={styles.signup as ViewStyle}
                  onPress={this.onPressSignUp}
                  fontSize={responsiveFontSize(2.2)} title="Sign Up" />

              </Form>
            </SafeAreaView>
          </Content>

          <DropdownAlert
            ref={this.dropdownCallback.bind(this)}
          />
        </ImageBackground>
          </HOCWithProgressIndicator>
      </Container>
    );
  }
}

// Initialize
const defaultStateProps = {
  errorMessages: {} as any,
  error: false as boolean,
  hasLoginAfterEffectsCompleted: true as boolean
  // mapStateToProps objects
}

type DispProps = {
  navigate: (any) => void,
  resetRoute: (ROUTE_TYPE) => void,
  goBack: () => void,
  signup: (payload) => void
}

const mapStateToProps = (state: Types.RootState): StateProps => {
  const {
    auth: {
      hasLoginAfterEffectsCompleted,
      error,
      errorMessages
    },
  } = state;

  return {
    errorMessages,
    error,
    hasLoginAfterEffectsCompleted,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>): DispProps => ({
  navigate: (route) => dispatch(navigate(route)),
  resetRoute: (ROUTE_TYPE) => dispatch(resetRoute(ROUTE_TYPE)),
  goBack: () => dispatch(goBack()),
  signup: payload => dispatch(signup(payload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(SignUp);
