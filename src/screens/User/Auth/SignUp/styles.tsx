import Colors from '../../../../constants/Colors';
import { Dimensions, Platform, StyleSheet, ViewStyle, ImageStyle, TextStyle } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import Fonts from '../../../../constants/Fonts';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

type StylesT = {
  container?: ViewStyle,
  background?: ImageStyle,
  overlay?: ViewStyle,
  activityIndicator?: ViewStyle,
  scrollView?: ViewStyle,

  logo?: ImageStyle,
  form?: ViewStyle,
  inputItem?: ViewStyle,
  input?: TextStyle,
  buttonContainer?: ViewStyle,
  dropdownContainer?:ViewStyle,
  buttonText?: TextStyle,
  downIcon?: ImageStyle,
  signup?: ViewStyle,
  dropdownText?: TextStyle,
}

const styles = StyleSheet.create<StylesT>({
  container:{

  },
  background:{
    height: deviceHeight,
    width: deviceWidth,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 100,
    backgroundColor: Colors.white,
    opacity: 0.6,
    justifyContent: 'center',
    alignItems: 'center'
  },
  activityIndicator: {
    alignSelf: 'center'
  },
  scrollView: {
  },

  form:{
    width: deviceWidth * .72,
    alignSelf:'center',
    marginTop: deviceHeight * .07,
    // backgroundColor:'red'
  },
  input:{
    fontSize: responsiveFontSize(2),
    fontFamily: Fonts.LIGHT,
    color: Colors.white,
    paddingTop: deviceHeight*.028,
    width:'100%',
    // backgroundColor:'blue',
    flex:1,
    borderBottomColor:Colors.white,
    borderBottomWidth:1,
    marginTop: deviceHeight * .01,
  },
  buttonContainer: {
    width: deviceWidth * .355,
    height: deviceHeight * .067,
    borderWidth:.3,
    borderColor: Colors.buttonForwardBorder,
    // backgroundColor:'red',
    justifyContent: 'center',
    // alignItems:'center',
    borderRadius:2,
    marginTop: deviceHeight *.03,
  },
  dropdownContainer:{
    width: deviceWidth * .355,
    height: deviceHeight * .067,
    borderWidth:0,
    // borderColor: Colors.buttonForwardBorder,
    backgroundColor:Colors.dropdown,
    justifyContent: 'center',
    // alignItems:'center',
    // alignItems:'center',
    // borderRadius:2,
    // marginTop: deviceHeight *.03,
  },
  dropdownText:{
    textAlign:'center',
    fontSize: responsiveFontSize(1.7),
    color: Colors.buttonForwardText,
    fontFamily: Fonts.LIGHT,
    backgroundColor:Colors.dropdown,
  },
  buttonText: {
    textAlign:'center',
    fontSize: responsiveFontSize(1.7),
    color: Colors.buttonForwardText,
    fontFamily: Fonts.LIGHT,
    // backgroundColor:Colors.dropdown,
    // marginLeft: deviceWidth * .1,
    // backgroundColor:'blue',
    // alignSelf:'center',
    // height: deviceHeight * .067,
  },
  downIcon: {
    position:'absolute',
    right: deviceWidth * .025,
    height: '60%',
    resizeMode:"contain",
    // backgroundColor:'blue',
    width: deviceWidth*.08,
  },
  signup:{
    width: deviceWidth * .37, 
    alignSelf:'flex-end', 
    marginTop: deviceHeight * .048,
  }
});

export default styles;