/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import * as React from 'react';
import {
  Platform, FlatList, ImageBackground, TextInput,Modal, ActivityIndicator,
  StyleSheet, Alert, Image, TouchableOpacity, TouchableHighlight,
  View, Dimensions, Text, StatusBar, Modal as RNModal,
  KeyboardAvoidingView,
} from 'react-native';
import { connect } from 'react-redux';

import {
  Container, Header, Left, Body, Right, Button, Icon,
  Title, Content, Input, Item, Form, Text as NBText
} from 'native-base';
import lodash from 'lodash';
import moment from 'moment';
import Types from 'Types';
import { isEmpty } from 'lodash';
import { requestAddCoverLetter, requestAddPersonalStatement } from '../../../../redux/actions/profile';
import { PROFILE_STATE_TYPE } from '../../../../redux/reducers/profile';
import DropdownAlert from 'react-native-dropdownalert';
import HOCWithProgressIndicator from '../../../../components/ProgressIndicator';
import GlobalKeys from '../../../../constants';
import styles from './styles';

import Icons from '../../../../constants/Icons';
import Images from '../../../../constants/Images';
import { navigate, resetRoute } from '../../../../redux/actions/nav';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

/////////////////////

type State = {
  coverLetter: string,
  personalStatement: string,
  modalVisible: boolean,
  modalType: MODAL_TYPES,
  requestInProgress: boolean,
};

type Props = StateProps & DispProps & typeof defaultProps;

const defaultProps = Object.freeze({
});

// type IProfile = {
//   _id: number,
//   firstName: string,
//   lastName: string,
//   userTypeName: string,
//   email: string,
//   contactNo: string,
//   userImage: string,
//   registrationDate: string,
//   experiences: Array<any>,
//   education: Array<any>,
//   currentSalary: string,
//   currency: string,
//   cv: string,
//   personalStatement: string,
//   coverLetter: string,
//   skills: Array<any>,
// };

// const initialState = Object.freeze({
//   profile: {
//     _id: 0,
//     firstName: "",
//     lastName: "",
//     userTypeName: "",
//     email: "",
//     contactNo: "",
//     userImage: "",
//     registrationDate: "",
//     experiences: [],
//     education: [],
//     currentSalary: "",
//     currency: "",
//     cv: "",
//     personalStatement: "",
//     coverLetter: "",
//     skills: [],
//   } as PROFILE_STATE_TYPE,
//   educationInfo: [] as Array<any>,
//   workInfo: [] as Array<any>,
//   personalStatement: "" as string,
//   modalVisible: false as boolean,
//   modalType: "EDUCATION" as MODAL_TYPES,
// });

type MODAL_TYPES = "COVER_LETTER" | "PERSONAL_STATEMENT";

class UserProfileView extends React.Component<Props, State> {

  /*----------------------------
      state cycle functions
    -----------------------------*/
  static readonly defaultProps = defaultProps;
  
  
  // static defaultProps = {
  //   errorMessages: null,
  //   modalVisible: false,
  //   profile: null,
  // } as StateProps;

  constructor(props: Props) {
    super(props);
    this.state = {
      // profile: this.props.profile.profile,
      // profile: this.getCurrentUser(),
      modalVisible: false,
      modalType: 'COVER_LETTER',
      coverLetter: "",
      personalStatement: "",
      requestInProgress: true,
      // educationInfo: [
      //   {
      //     id: 1,
      //     title: 'Universty Degree',
      //     duration: '2008 - 2012',
      //     companyName: 'Universty of California, Los Angeles',
      //   },
      // ],
      // workInfo: [
      //   {
      //     id: 1,
      //     title: 'React Native Developer',
      //     duration: '2016 - Today',
      //     companyName: 'Lorizzle',
      //   },
      //   {
      //     id: 1,
      //     title: 'Android Developer',
      //     duration: '2012 - 2016',
      //     companyName: 'PICKSUM IPSUM',
      //   },
      // ],
      // personalStatement: 'A highly motivated and hardworking individual, who has recently completed their A-Levels, achieving excellent grades in both Maths and Science. Seeking an apprenticeship in the engineering industry to build upon a keen scientific interest and start a career as a maintenance engineer. Eventual career goal is to become a fully-qualified and experienced maintenance or electrical engineer, with the longer-term aspiration of moving into project management.',
    };
  }

  // shouldComponentUpdate(nextProps, nextState): boolean{


  //   return true;
  // }

  componentDidMount() {

  }

  shouldComponentUpdate (nextProps, nextState) {
    const { profile, errorMessages, loadingRequest } = this.props;

    if (errorMessages !== nextProps.errorMessages) {
      if (!isEmpty(nextProps.errorMessages)) {
        const { email, password } = nextProps.errorMessages;
        this.setState({ requestInProgress: false });
        this.dropdown.alertWithType('error', 'Error!', JSON.stringify(nextProps.errorMessages.error));
        return true;
      }
    }

    if (loadingRequest !== nextProps.loadingRequest && nextProps.loadingRequest === false) {
      
      let flag = isEmpty(nextProps.errorMessages);
      if (flag) {
        console.log('close modal');
        // Alert.alert('work added added');
        this.closeModal();
        // this.props.goBack();
      }
    }

    return true;
  }

  /*----------------------------
      helper functions
    -----------------------------*/

  getCurrentUser(){
    let p: IProfile = {
      _id: 0,
      firstName: "",
      lastName: "",
      userTypeName: "",
      email: "",
      contactNo: "",
      userImage: "",
      registrationDate: "",
      experiences: [],
      education: [],
      currentSalary: "",
      currency: "",
      cv: "",
      personalStatement: "",
      coverLetter: "",
      skills: [],
    };
    return p;
  }
  /*----------------------------
      network calls
    -----------------------------*/

  /*----------------------------
      state render helper functions
    -----------------------------*/
  truncateText(text): string {
    if (text.length > 250) {
      return text.substring(0, 250);
    } else return text;
  }

  getDuration(item): string {
    let dur = "";
    try{
      let startDate = moment(item.fromYear).format('YYYY');
      let endDate = moment(item.toYear).format('YYYY');
      dur = startDate + " - " + endDate;
    } catch(e) { return "" }
    return dur;
  }

  getValidString(str1: string, str2: string):string{
    let str = "";
    if (!isEmpty(str1) && str1.trim().length > 0){
      str = str1;
    }

    else if (!isEmpty(str2)) return str2;
    return str;
  }

/*----------------------------
    user action functions
  -----------------------------*/
  setModalVisible(modalVisible): void{
    this.setState({modalVisible});
  }

  /*----------------------------
      navigation functions
    -----------------------------*/

  ///////////////////////////////
  ///////////////////////////////

  /*----------------------------
      render functions
    -----------------------------*/

  renderWorkListItem(item) : React.ReactNode{
    console.log(item);
    return (
      <View style={styles.infoContainer}>
        <Text style={styles.groupSecondaryTitle}>{item.title}</Text>
        <Text style={styles.groupSubHeader}>{this.getDuration(item)}</Text>
        <Text style={styles.groupHeader}>{item.company}</Text>
      </View>
    )
  }
  renderTextInfo(item): React.ReactNode {
    return (
      <View style={styles.textInfoContainer}>
        <Text>
          <Text style={styles.groupHeader}>{item}</Text>
          {/* {item.length > 250 ? 
          <Text>Show More</Text>
          : null
        } */}
        </Text>
      </View>
    )
  }

  renderEducationInfo(item): React.ReactNode {
    return (
      <View style={styles.infoContainer}>
        <Text style={styles.groupSecondaryTitle}>{item.certificateDegreeName}</Text>
        <Text style={styles.groupSubHeader}>{this.getDuration(item)}</Text>
        <Text style={styles.groupHeader}>{item.instituteUniversityName}</Text>
      </View>
    )
  }

  openModal(modalType: MODAL_TYPES): void{
    this.setState({ modalVisible: true, modalType });
  }

  closeModal(): void{
    this.setState({ modalVisible: false});
  }

  addCoverLetter():void{
    const { coverLetter } = this.state;
    if (!isEmpty(coverLetter) && coverLetter.trim().length > 0) {
      this.props.requestAddCoverLetter({ coverLetter });
    }
  }

  addPersonalStatement(): void{
    const { personalStatement } = this.state;
    if (!isEmpty(personalStatement) && personalStatement.trim().length > 0) {
      this.props.requestAddPersonalStatement({ personalStatement });
    }
  }

  renderCoverLetterModal(): React.ReactNode{
    return (
      <View style={{flex:1}}>
        <GlobalKeys.Components.HeaderWSettings renderLeftButton={true}
          onLeftButtonPress={()=> this.closeModal()} title="Add Cover Letter"
          headerBackgroundColor={GlobalKeys.Colors.jobDetailHeaderBGColor} />

        <View style={styles.modalContainer}>
          <Text style={styles.modalTitle}>Enter Your Cover Letter</Text>
          <TextInput style={styles.modalInput} multiline={true} 
            onChangeText = {(coverLetter) => this.setState({coverLetter})}/>
          <Button style={styles.submitButton} onPress={() => this.addCoverLetter()}>
            <Text style={styles.buttonText}>Add Cover Letter</Text>
          </Button>
        </View>

      </View>
    )
  }

  renderPersonalStatmentModal(): React.ReactNode{
    return (
      <View style={{flex:1}}>
        <GlobalKeys.Components.HeaderWSettings renderLeftButton={true}
          onLeftButtonPress={()=> this.closeModal()} title="Add Personal Statement"
          headerBackgroundColor={GlobalKeys.Colors.jobDetailHeaderBGColor} />

        <View style={styles.modalContainer}>
          <Text style={styles.modalTitle}>Enter Your Personal Statement</Text>
          <TextInput style={styles.modalInput} multiline={true} 
            onChangeText = {(personalStatement) => this.setState({personalStatement})}/>
          <Button style={styles.submitButton} onPress={() => this.addPersonalStatement()}>
            <Text style={styles.buttonText}>Add Personal Statement</Text>
          </Button>
        </View>

      </View>
    )
  }

  renderModalMenu(): React.ReactNode{
    switch(this.state.modalType){
      case 'COVER_LETTER':
        return this.renderCoverLetterModal();
      return;
      case 'PERSONAL_STATEMENT':
        return this.renderPersonalStatmentModal();
      return;
    }
  }
  
  onAddEducation(edu){
    alert(edu);
  }

  getRender(){
    console.log("state",this.state);
    console.log("props", this.props);
  }

  openCV(){
    console.log('a');
  }

  dropdownCallback(ref) {
    this.dropdown = ref;
  }

  render(): React.ReactNode {
    return (
      <Container style={styles.container}>
        <HOCWithProgressIndicator showLoading = {this.props.loadingRequest}>
        <GlobalKeys.Components.HeaderWSettings renderLeftButton={false}
          onRightButtonPress={() => Alert.alert('a')} title="Profile"
          headerBackgroundColor={GlobalKeys.Colors.jobDetailHeaderBGColor} />

        <Content>
        <Form style={styles.form}>
          <View style={styles.profileContainer}>
            <View style={styles.profilePicContainer}>
              <Image
                style={styles.profilePic}
                source={{ uri: this.props.profile.userImage }}
              />
            </View>
            <Text style={styles.username}>
              {this.getValidString(this.props.profile.firstName + " " + this.props.profile.lastName, "Name")}
            </Text>
            <Text style={styles.email}>{this.getValidString(this.props.profile.email,"Email")}</Text>
            <Button rounded style={styles.cvButton} onPress={() => this.openCV()}>
              <Text style={styles.cvText}>Upload Your CV</Text>
            </Button>
          </View>
          <View style={styles.groupOuterContainer}>
            <View style={styles.groupContainer}>
              <View style={styles.groupHeaderContainer}>
                <Text style={styles.groupTitle}>Cover Letter</Text>
                <TouchableOpacity onPress={() => this.openModal("COVER_LETTER")}>
                  <Image style={styles.addButton} source={Icons.AddButtonBlueTransparent} />
                </TouchableOpacity>
              </View>
            </View>
          </View>
          <View style={styles.groupOuterContainer}>
            <View style={styles.groupContainer}>
              <View style={styles.groupHeaderContainer}>
                <Text style={styles.groupTitle}>Education</Text>
                <TouchableOpacity onPress={() => this.props.navigate({ routeName: 'AddEducation' })}>
                  <Image style={styles.addButton} source={Icons.AddButtonBlueTransparent} />
                </TouchableOpacity>
              </View>
              <FlatList
                data={this.props.profile.education}
                keyExtractor={item => item._id}
                renderItem={({ item }) => this.renderEducationInfo(item)}
                style={styles.flatListStyle} scrollEnabled={false}
              />
            </View>
          </View>

          <View style={styles.groupOuterContainer}>
            <View style={styles.groupContainer}>
              <View style={styles.groupHeaderContainer}>
                <Text style={styles.groupTitle}>Work Info</Text>
                <TouchableOpacity onPress={() => this.props.navigate({ routeName: 'AddWorkExperience' })}>
                  <Image style={styles.addButton} source={Icons.AddButtonBlueTransparent} />
                </TouchableOpacity>
              </View>
              <FlatList
                data={this.props.profile.experiences}
                keyExtractor={item => item._id} scrollEnabled={false}
                renderItem={({ item }) => this.renderWorkListItem(item)}
                style={styles.flatListStyle}
              />
            </View>
          </View>
          <View style={styles.groupOuterContainer}>
            <View style={styles.groupContainer}>
              <View style={styles.groupHeaderContainer}>
                <Text style={styles.groupTitle}>Personal Statement</Text>
                <TouchableOpacity onPress={() => this.openModal("PERSONAL_STATEMENT")}>
                  <Image style={styles.addButton} source={Icons.AddButtonBlueTransparent} />
                </TouchableOpacity>
              </View>
              {this.renderTextInfo(this.props.profile.personalStatement)}
            </View>
          </View>
        </Form>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          {this.renderModalMenu()}
        </Modal>
        </Content>
        <DropdownAlert
          ref={this.dropdownCallback.bind(this)}
        />
        </HOCWithProgressIndicator>
      </Container>
    );
  }
}

type StateProps = {
  profile: PROFILE_STATE_TYPE,
  errorMessages: any,
  loadingRequest: any,
}

type DispProps = {
  navigate: (any) => void,
  resetRoute: (any) => void,
  requestAddCoverLetter: (any) => void,
  requestAddPersonalStatement: (any) => void,
}

const mapStateToProps = (state: Types.RootState): StateProps => {
  const {
    profile: {
      errorMessages,
      profile,
      loadingRequest,
    }
  } = state;
  
  return { profile, errorMessages, loadingRequest };
};

const mapDispatchToProps = dispatch => ({
  navigate: (route) => dispatch(navigate(route)),
  resetRoute: (route) => dispatch(resetRoute(route)),
  requestAddCoverLetter: (requestPayload) => dispatch(requestAddCoverLetter(requestPayload)),
  requestAddPersonalStatement: (requestPayload) => dispatch(requestAddPersonalStatement(requestPayload)),
});

export default connect(mapStateToProps, mapDispatchToProps)(UserProfileView);

