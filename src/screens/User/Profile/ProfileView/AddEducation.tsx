/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component, PureComponent, Children } from 'react';
import {
  Platform, Dimensions, AsyncStorage, TouchableWithoutFeedback,
  StyleSheet, ActivityIndicator, Alert,
  View, Image, KeyboardAvoidingView, TouchableOpacity
} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon,
    Title, Content, Input,Item, Form, Label, Textarea,
    Text } from 'native-base';
import moment from 'moment';
// CHECK: Proper TS Stateless component
import { MediaQueryStyleSheet } from 'react-native-responsive';
import { isEmpty } from 'lodash';
import Types from 'Types';
import DropdownAlert from 'react-native-dropdownalert';
import { bindActionCreators, Dispatch } from 'redux';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import YearMonthPicker from '../../../../components/YearMonthPicker';

import Colors from '../../../../constants/Colors';
import Fonts from '../../../../constants/Fonts';
import GlobalKeys from '../../../../constants';
import { navigate, resetRoute, goBack } from '../../../../redux/actions/nav';
import { yearValidator } from '../../../../helperFunctions/yearValidator';
import { requestAddEducation } from '../../../../redux/actions/profile';
import { connect } from 'react-redux';


type State = {
  school: string,
  degree: string,
  major: string,
  grade: string,
  fromYear: string,
  toYear: string,
  requestInProgress: boolean,
  enableSubmitButton: boolean,

  startYear: string,
  endYear: string,
  selectedYear: string,
  selectedMonth: string,
  fromSelectedYear:string,
  fromSelectedMonth: string,
  toSelectedMonth: string,
  toSelectedYear: string,
  isFromSelected: boolean,
};

type IEducationObj = {
  school: string,
  degree: string,
  major: string,
  grade: string,
  fromYear: string,
  toYear: string,
}

type OwnProps = typeof defaultProps;
type StateProps = typeof defaultStateProps;
type Props = StateProps & DispProps & OwnProps;

// num:3 as number
const defaultProps = Object.freeze({
});

class AddEducation extends Component<Props, State> {

  /*----------------------------
      state cycle functions
    -----------------------------*/
  static readonly defaultProps = defaultProps;
  
  constructor(props: Props){
    super(props);
    const currDate = moment();
    const currYear = (currDate.format('YYYY'));
    const currMonth = ((new Date()).getMonth())
    this.state = { 
      school: "",
      degree: "",
      major: "",
      grade: "",
      fromYear: "",
      toYear: "",
      enableSubmitButton: false,

      startYear: "1960",
      endYear: currYear,
      requestInProgress: false,
      selectedYear: currYear.toString(),
      selectedMonth: currMonth.toString(),
      fromSelectedYear:"",
      fromSelectedMonth: "",
      toSelectedMonth: "",
      toSelectedYear: "",
      isFromSelected: true,
    };
  }

  shouldComponentUpdate (nextProps, nextState) {
    const { profile, errorMessages, loadingRequest } = this.props;

    if (errorMessages !== nextProps.errorMessages) {
      if (!isEmpty(nextProps.errorMessages)) {
        const { email, password } = nextProps.errorMessages;
        this.setState({ requestInProgress: false });
        this.dropdown.alertWithType('error', 'Error!', JSON.stringify(nextProps.errorMessages.error));
        return true;
      }
    }

    if (loadingRequest !== nextProps.loadingRequest && nextProps.loadingRequest === false) {
      if (isEmpty(nextProps.errorMessages)) {
        Alert.alert('education added');
        this.props.goBack();
      }
    }
    return true;
  }

  validateFromDate(t): void{
    let v = yearValidator(t);
  }

  validateStringInput(str: String): boolean{
    if (!isEmpty(str) && str.length >= 2)
      return true;
    else return false;
  }

  validateEducation(): void{
    const { school, degree, major, grade, fromSelectedYear, fromSelectedMonth,
      toSelectedMonth, toSelectedYear, enableSubmitButton
    } = this.state;
    let flag: boolean = false;

    if (isEmpty(school) || isEmpty(school) || isEmpty(degree) || isEmpty(major)
      // || isEmpty(grade)
      || isEmpty(fromSelectedYear) || isEmpty(fromSelectedMonth) 
      || isEmpty(toSelectedMonth) || isEmpty(toSelectedYear)) {
        flag = false;
    } else flag = true;

    console.log(flag);
    if (flag !== enableSubmitButton){
      this.setState({enableSubmitButton: flag});
    }
  }

  addEducation(requestPayload: any): void{
    const { school, degree, major, grade, fromSelectedYear, fromSelectedMonth,
      toSelectedMonth, toSelectedYear
    } = this.state;

    // we are making sure it will reach here only when valid
    let fromYear = `01-${fromSelectedMonth}-${fromSelectedYear}`;
    let toYear = `01-${toSelectedMonth}-${toSelectedYear}`;

    const eduObj: IEducationObj = {
      school, 
      degree, 
      major, 
      grade,
      fromYear, 
      toYear
    }
    this.props.requestAddEducation(eduObj);
  }

  dropdownCallback(ref): void{
    this.dropdown = ref;
  }

  onPressFromDate(): void{
    this.setState({isFromSelected: true});
    this.showPicker();
  }

  onPressToDate(): void{
    this.setState({isFromSelected: false});
    this.showPicker();
  }

  showPicker = () => {
    const { startYear, endYear, selectedYear, selectedMonth } = this.state;
    this.picker.show({startYear, endYear, selectedYear, selectedMonth})
      .then(({year, month}) => {
        if (this.state.isFromSelected){
          this.setState({
            selectedYear: year,
            selectedMonth: month,
            fromSelectedYear: year,
            fromSelectedMonth: month,
        })} else {
          this.setState({
            selectedYear: year,
            selectedMonth: month,
            toSelectedYear: year,
            toSelectedMonth: month,
        })}
        this.validateEducation();
      });
  }

  setSchool(school): void{
    this.setState({school});
    this.validateEducation();
  }

  setDegree(degree): void{
    this.setState({degree});
    this.validateEducation();
  }

  setMajor(major): void{
    this.setState({major});
    this.validateEducation();
  }

  setGrade(grade): void{
    this.setState({grade});
    this.validateEducation();
  }

  render(){
    const {fromSelectedMonth, fromSelectedYear, enableSubmitButton,
      toSelectedMonth, toSelectedYear, requestInProgress} = this.state;
    return ( 
      <View style={styles.container}>
        {requestInProgress && (
        <View style={styles.overlay}>
          <ActivityIndicator style={styles.activityIndicator} size="large" color={GlobalKeys.Colors.appPrimaryBlueColor}/>
        </View>
        )}
        <GlobalKeys.Components.HeaderWSettings renderLeftButton={true}
          onLeftButtonPress={()=> this.props.goBack()} title="Add Education"
          headerBackgroundColor={GlobalKeys.Colors.jobDetailHeaderBGColor} />

        <Content>
        <Form style={styles.form}>
          <Item style={styles.fullInputContainer}>
            <Input placeholder="School" style={styles.inputStyle} onChangeText={(school) => this.setSchool(school)} />
          </Item>
          <Item style={styles.fullInputContainer}>
            <Input placeholder="Degree" style={styles.inputStyle} onChangeText={(degree) => this.setDegree(degree)}/>
          </Item>
          <Item style={styles.fullInputContainer}>
            <Input placeholder="Major" style={styles.inputStyle} onChangeText={(major) => this.setMajor(major)}/>
          </Item>
          <Item style={styles.fullInputContainer}>
            <Input placeholder="Grade (optional)" style={styles.inputStyle} onChangeText={(grade) => this.setGrade(grade)}/>
          </Item>
            <View style={styles.halfContainer}>
              <TouchableWithoutFeedback onPress={() => this.onPressFromDate()}>
                {fromSelectedYear === "" ? 
                <View style={styles.datePickerContainer}>
                  <Text style={styles.placeHolderText}>From</Text>
                </View>
                :
                <View style={styles.datePickerContainer}>
                  <Text style={styles.locationText}>{fromSelectedYear}-{fromSelectedMonth}</Text>
                </View>
              }
              </TouchableWithoutFeedback>
              <TouchableWithoutFeedback onPress={() => this.onPressToDate()}>
                {toSelectedYear === "" ? 
                <View style={styles.datePickerContainer}>
                  <Text style={styles.placeHolderText}>To</Text>
                </View>
                :
                <View style={styles.datePickerContainer}>
                  <Text style={styles.locationText}>{toSelectedYear}-{toSelectedMonth}</Text>
                </View> 
                }
              </TouchableWithoutFeedback>   
            </View> 
          {/* <Item> */}
            <Button style={styles.addEduButton} disabled={!enableSubmitButton} onPress={() => this.addEducation()}>
              <Text>Add Education</Text>
            </Button>
          {/* </Item> */}
          {/* <Item style={{ marginTop: deviceHeight * .03,}}>
            <Textarea rowSpan={3} bordered placeholder="Description" 
              style={styles.descriptionContainer}
            />
          </Item> */}
      </Form>
        </Content>
        <DropdownAlert
          ref={this.dropdownCallback.bind(this)}
        />
        <YearMonthPicker ref={(picker) => this.picker=picker}/>
      </View>
    );
  }
};

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = MediaQueryStyleSheet.create({
  container: {
    flex: 1,
    // width: deviceWidth * .9,
    backgroundColor: Colors.appPrimaryLightBackground,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 100,
    backgroundColor: Colors.appPrimaryLightBackground,
    opacity: 0.6,
    justifyContent:'center',
    alignItems:'center'
  },
  activityIndicator:{
    alignSelf:'center'
  },
  form:{
    width: deviceWidth * .9,
  },
  halfInputContainer:{
    justifyContent:'space-between', 
    borderColor: Colors.appPrimaryLightBackground,
    // backgroundColor:'red',
  },
  fullInputContainer:{
    borderColor: Colors.singinLightBorder,
  },
  halfInput:{
    width:'45%',
    maxWidth:'45%',
    borderBottomColor:Colors.singinLightBorder,
    borderBottomWidth: 1,
    fontFamily: Fonts.LIGHT,
    color: Colors.appHeaderDarkColor,
  },
  descriptionContainer:{
    borderColor:Colors.singinLightBorder,
    width: '100%',
    fontFamily: Fonts.LIGHT,
    color: Colors.appHeaderDarkColor,
    // width: deviceWidth * .9
  },
  inputStyle:{
    fontFamily: Fonts.LIGHT,
    color: Colors.appHeaderDarkColor,
  },
  addEduButton:{
    marginTop: deviceHeight * .05,
    alignSelf: 'center',
  },
  locationContainer:{
    height: deviceHeight * .078,
    width: deviceWidth * .86,
    // backgroundColor:'red',
    alignSelf: 'flex-end',
    justifyContent:'center',
    paddingLeft:deviceWidth * .015,
    borderBottomColor: Colors.singinLightBorder,
    borderBottomWidth:1,
  },
  placeHolderText:{
    fontFamily: Fonts.LIGHT,
    color: Colors.placeHolderTextColor,
  },
  locationText:{
    fontFamily: Fonts.LIGHT,
    color: Colors.appHeaderDarkColor,
  },
  halfContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
    // backgroundColor:'red',
  },
  datePickerContainer:{
    height: deviceHeight * .078,
    width: deviceWidth * .4,
    // backgroundColor:'red',
    alignSelf: 'flex-end',
    justifyContent:'center',
    marginLeft:deviceWidth * .04,
    borderBottomColor: Colors.singinLightBorder,
    borderBottomWidth:1,
    paddingLeft:deviceWidth * .015,
  },
});

// num: 3 as number
const defaultStateProps = {
  // errorMessages: any,
  errorMessages:{} as any,
  profile: {} as any,
  loadingRequest: true as boolean,
}

type DispProps = {
  navigate: (any) => void,
  requestAddEducation: (IEducationObj) => void,
  goBack: () => void,
}

const mapStateToProps = (state: Types.RootState): StateProps => {
  const {
    profile:{
      profile,
      errorMessages,
      loadingRequest
    }
  } = state;

  return {
    errorMessages,
    profile,
    loadingRequest,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>) : DispProps => ({
  navigate: (route) => dispatch(navigate(route)),
  requestAddEducation: (IEducationObj) => dispatch(requestAddEducation(IEducationObj)),
  goBack: () => dispatch(goBack()),
});

const enhance = connect<StateProps, DispProps>(
  mapStateToProps, mapDispatchToProps
)

export default enhance(AddEducation);
// export default connect(mapStateToProps)(mapDispatchToProps)(AddEducation);

