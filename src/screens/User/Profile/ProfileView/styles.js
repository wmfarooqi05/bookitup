import { MediaQueryStyleSheet } from 'react-native-responsive';
import Colors from '../../../../constants/Colors';
import { Dimensions, Platform } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import Fonts from '../../../../constants/Fonts';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = MediaQueryStyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.emptyLine,
    alignItems: 'center',
    width:deviceWidth,

  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 100,
    backgroundColor: Colors.appPrimaryLightBackground,
    opacity: 0.6,
    justifyContent:'center',
    alignItems:'center'
  },
  activityIndicator:{
    alignSelf:'center'
  },
  form:{
    alignItems: 'center',
    backgroundColor:Colors.emptyLine,
  },
  profileContainer:{
    backgroundColor:Colors.white,
    width:deviceWidth,
    alignItems:'center',
    paddingBottom: deviceHeight * .02,
  },
  profilePicContainer:{
    width: deviceWidth * .13,
    height:deviceWidth * .13,
    backgroundColor: 'red',
    marginTop: deviceHeight * .028,
  },
  profilePic:{
    height:'100%',
    width:'100%',
    // backgroundColor:'blue',
  },
  username:{
    fontSize: responsiveFontSize(1.7),
    fontFamily: Fonts.SEMIBOLD,
    color: Colors.appHeaderDarkColor,
    marginTop: deviceHeight * .023,
  },
  email:{
    fontSize: responsiveFontSize(.98),
    fontFamily: Fonts.SEMIBOLD,
    color: Colors.profileEmail,
    marginTop: deviceHeight * .0098,
  },
  cvButton:{
    width:deviceWidth * .35,
    height: deviceHeight * .04,
    borderColor: Colors.appPrimaryBlueColor,
    borderWidth: 1,
    backgroundColor:Colors.white,
    alignSelf: 'center',
    marginTop: deviceHeight * .019,
  },
  cvText:{
    fontSize: responsiveFontSize(1),
    color: Colors.appPrimaryBlueColor,
    textAlign:'center',
    flex:1,
    fontFamily: Fonts.BOLD,
  },
  groupOuterContainer:{
    width: deviceWidth * .86,
    alignItems: 'center',
    backgroundColor:Colors.white,
    marginTop: deviceHeight * .015,
    // marginBottom: deviceHeight * .02,
    paddingTop: deviceHeight * .014,
    paddingBottom: deviceHeight * .014,
  },
  groupContainer:{
    width: deviceWidth * .82,
    backgroundColor:Colors.white,
    borderWidth:0,
    borderColor: Colors.appLightBorderColor,
    borderRadius: 5,
    // paddingTop: deviceHeight * .014,
    // paddingBottom: deviceHeight * .014,
    paddingLeft: deviceWidth * .026,
    paddingRight: deviceWidth * .017,
    // alignItems: 'center',
  },
  groupHeaderContainer:{
    flexDirection: 'row',
    // backgroundColor:'blue',
    justifyContent: 'space-between',
    alignItems:'center',
    // marginBottom: deviceHeight * .012,
  },
  infoContainer:{
    marginTop: deviceHeight * .006
  },
  flatListStyle:{
    // marginTop: deviceHeight * .012,
  },
  textInfoContainer:{
    marginBottom: deviceHeight * .006,
    marginTop: deviceHeight * .012,
    // maxHeight: deviceHeight * .05,    
  },
  groupTitle:{
    fontFamily: Fonts.SEMIBOLD,
    fontSize: responsiveFontSize(1.6),
    color: Colors.appHeaderDarkColor,

  },
  groupSecondaryTitle:{
    fontFamily: Fonts.SEMIBOLD,
    fontSize: responsiveFontSize(1.2),
    color: Colors.appPrimaryBlueColor,
    marginBottom: deviceHeight * .003,
  },
  groupHeader:{
    fontSize: responsiveFontSize(.9),
    color: Colors.signUpEntryTitle,
    fontFamily: Fonts.SEMIBOLD,
    marginBottom: deviceHeight * .003,
  },
  groupSubHeader:{
    fontSize: responsiveFontSize(.8),
    color: Colors.jobDetailHeaderText,
    fontFamily: Fonts.SEMIBOLD,
    marginBottom: deviceHeight * .003,
  },
  addButton:{
    height: deviceHeight * .022,
    width: deviceHeight * .022,
  },

  modalContainer:{
    flex:1,
    // backgroundColor:'pink',
    marginLeft: deviceWidth * .05,    
  },
  modalTitle:{
    fontSize: responsiveFontSize(2),
    fontFamily: Fonts.BOLD,
    color: Colors.signUpEntryTitle,
    marginTop: deviceHeight * .023,
    // marginTop:deviceHeight * .005,
  },
  modalInput:{
    fontSize: responsiveFontSize(1.7),
    color: Colors.jobDetailHeaderText,
    fontFamily: Fonts.SEMIBOLD,
    borderWidth:.5,
    borderColor: Colors.appLightBorder,
    width: deviceWidth * .9,
    // alignSelf:'center',
    height: deviceHeight * .7,
    marginTop:deviceHeight * .015,
    // marginBottom: deviceHeight * .003,
  },
  submitButton:{
    marginTop: deviceHeight * .03,
    alignSelf: 'center',
  },
  buttonText:{
    paddingHorizontal: deviceWidth * .03,
    color: Colors.white,
  },
});

export default styles;