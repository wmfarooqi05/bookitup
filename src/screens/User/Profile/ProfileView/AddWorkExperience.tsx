/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component, PureComponent, Children } from 'react';
import {
  Platform, Dimensions, AsyncStorage, TouchableWithoutFeedback,Modal, 
  StyleSheet, ActivityIndicator, Alert, TouchableHighlight,
  View, Image, KeyboardAvoidingView, TouchableOpacity
} from 'react-native';
import { Container, Header, Left, Body, Right, Button, Icon,
    Title, Content, Input,Item, Form, Label, Textarea,
    Text } from 'native-base';
// CHECK: Proper TS Stateless component
import { MediaQueryStyleSheet } from 'react-native-responsive';
import { isEmpty, some } from 'lodash';
import Types from 'Types';
import DropdownAlert from 'react-native-dropdownalert';
import { Dispatch } from 'redux';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';
import YearMonthPicker from '../../../../components/YearMonthPicker';
import moment from 'moment';

import Colors from '../../../../constants/Colors';
import Fonts from '../../../../constants/Fonts';
import GlobalKeys from '../../../../constants';
import { navigate, resetRoute, goBack } from '../../../../redux/actions/nav';
import { yearValidator } from '../../../../helperFunctions/yearValidator';
import { requestAddWorkExperience } from '../../../../redux/actions/profile';
import { connect } from 'react-redux';


type State = {

  // System states
  requestInProgress: boolean,
  modalVisible: boolean,
  enableSubmitButton: boolean,

  // Input fields states
  company: string,
  title: string,
  locationPointer: any,
  locationName: string,
  startYear: string,
  endYear: string,
  selectedYear: string,
  selectedMonth: string,
  fromSelectedYear:string,
  fromSelectedMonth: string,
  toSelectedMonth: string,
  toSelectedYear: string,
  isFromSelected: boolean,
};
type OwnProps = typeof defaultProps;
type StateProps = typeof defaultStateProps;
type Props = StateProps & DispProps & OwnProps;

// num:3 as number
const defaultProps = Object.freeze({
});

class AddWorkExperience extends Component<Props, State> {

  /*----------------------------
      state cycle functions
    -----------------------------*/
  static readonly defaultProps = defaultProps;
  
  constructor(props: Props){
    super(props);
    const currDate = moment();
    const currYear = (currDate.format('YYYY'));
    const currMonth = ((new Date()).getMonth())
    this.state = { 
      company: "",
      title: "",
      requestInProgress: false,
      modalVisible: false,
      // locationPointer: null,
      locationPointer: {
        "latitude": 33.714372,
        "longitude": 73.159108
      },
      locationName: "Bani Gala, Islamabad",
      startYear: "1960",
      endYear: currYear,
      selectedYear: currYear.toString(),
      selectedMonth: currMonth.toString(),
      fromSelectedYear:"",
      fromSelectedMonth: "",
      toSelectedMonth: "",
      toSelectedYear: "",
      isFromSelected: true,

      enableSubmitButton: false,
    };
  }

  shouldComponentUpdate (nextProps, nextState) {
    const { profile, errorMessages, loadingRequest } = this.props;

    if (errorMessages !== nextProps.errorMessages) {
      if (!isEmpty(nextProps.errorMessages)) {
        const { email, password } = nextProps.errorMessages;
        this.setState({ requestInProgress: false });
        this.dropdown.alertWithType('error', 'Error!', JSON.stringify(nextProps.errorMessages.error));
        return true;
      }
    }

    if (loadingRequest !== nextProps.loadingRequest && nextProps.loadingRequest === false) {
      
      if (isEmpty(nextProps.errorMessages)) {
        Alert.alert('work added added');
        this.props.goBack();
      }
    }

    return true;
  }

  validateFromDate(t){
    let v = yearValidator(t);
  }

  validateStringInput(str: String): boolean{
    if (!isEmpty(str) && str.length >= 2)
      return true;
    else return false;
  }

  validateWorkExperience(){
    const { company, title, locationPointer, locationName, fromSelectedYear, fromSelectedMonth,
      toSelectedMonth, toSelectedYear, enableSubmitButton
    } = this.state;
    let flag: boolean = false;

    if (isEmpty(company) || isEmpty(title)
    // || isEmpty(locationName) || isEmpty(locationPointer)
      || isEmpty(fromSelectedYear) || isEmpty(fromSelectedMonth) 
      || isEmpty(toSelectedMonth) || isEmpty(toSelectedYear)) {
        flag = false;
    } else flag = true;

    console.log(flag);
    if (flag !== enableSubmitButton){
      this.setState({enableSubmitButton: flag});
    }
  }

  addWorkExperience(){
    const { company, title, locationPointer, locationName, fromSelectedYear, fromSelectedMonth,
      toSelectedMonth, toSelectedYear,
    } = this.state;
    
    // we are making sure it will reach here only when valid
    let fromYear = `01-${fromSelectedMonth}-${fromSelectedYear}`;
    let toYear = `01-${toSelectedMonth}-${toSelectedYear}`;

    this.props.requestAddWorkExperience({ company, title, locationPointer, 
      locationName, fromYear, toYear });
    
  }

  dropdownCallback(ref) {
    this.dropdown = ref;
  }

  onLocationSelect(data, details){
    let loc = {
      latitude: details.geometry.location.lat,
      longitude: details.geometry.location.lng,
    };
    this.setState({locationPointer: loc, locationName: data.description, modalVisible: false });
    this.validateWorkExperience();
  }

  renderLocationModal(){
    return(
      <View style={{flex:1}}>
        <GlobalKeys.Components.HeaderWSettings renderLeftButton={true}
          onLeftButtonPress={()=> this.setState({modalVisible: false})} title="Add Location"
          headerBackgroundColor={GlobalKeys.Colors.jobDetailHeaderBGColor} />
          <GooglePlacesAutocomplete
            placeholder='Search'
            minLength={2} // minimum length of text to search
            autoFocus={false}
            returnKeyType={'search'} // Can be left out for default return key https://facebook.github.io/react-native/docs/textinput.html#returnkeytype
            listViewDisplayed='auto'    // true/false/undefined
            fetchDetails={true}
            renderDescription={row => row.description} // custom description render
            onPress={(data, details = null) => this.onLocationSelect(data, details)}
            
            getDefaultValue={() => ''}
            
            query={{
              // available options: https://developers.google.com/places/web-service/autocomplete
              key: 'AIzaSyCzRdUGMvk-tGQeFSVjG62T1w8BVi-cGJ0',
              language: 'en', // language of the results
              types: '(cities)' // default: 'geocode'
            }}
            
            styles={{
              textInputContainer: {
                backgroundColor:Colors.white,
                width: '100%'
              },
              description: {
                fontWeight: 'bold'
              },
              predefinedPlacesDescription: {
                color: '#1faadb'
              },
              textInput: {
                fontFamily: Fonts.LIGHT,
                color: Colors.placeHolderTextColor,
              },
              // textInputContainer: {
              //   backgroundColor: '#C9C9CE',
              //   height: 44,
              //   borderTopColor: '#7e7e7e',
              //   borderBottomColor: '#b5b5b5',
              //   borderTopWidth: 1 / PixelRatio.get(),
              //   borderBottomWidth: 1 / PixelRatio.get(),
              //   flexDirection: 'row',
              // },
            }}
            
            currentLocation={true} // Will add a 'Current location' button at the top of the predefined places list
            currentLocationLabel="Current location"
            nearbyPlacesAPI='GooglePlacesSearch' // Which API to use: GoogleReverseGeocoding or GooglePlacesSearch
            GoogleReverseGeocodingQuery={{
              // available options for GoogleReverseGeocoding API : https://developers.google.com/maps/documentation/geocoding/intro
            }}
            GooglePlacesSearchQuery={{
              // available options for GooglePlacesSearch API : https://developers.google.com/places/web-service/search
              rankby: 'distance',
              types: 'food'
            }}

            filterReverseGeocodingByTypes={['locality', 'administrative_area_level_3']} // filter the reverse geocoding results by types - ['locality', 'administrative_area_level_3'] if you want to display only cities

            debounce={200} // debounce the requests in ms. Set to 0 to remove debounce. By default 0ms.
            // renderLeftButton={()  => <Image source={{uri:'https://cdn4.iconfinder.com/data/icons/defaulticon/icons/png/256x256/arrow-alt-left.png'}} />}
            // renderRightButton={() => <Text>Custom text after the input</Text>}
          />
        </View>

          // <View style={{marginTop: 22}}>
          //   <View>
          //     <Text>Hello World!</Text>

          //     <TouchableHighlight
          //       onPress={() => {
          //         this.setState({modalVisible: false});
          //       }}>
          //       <Text>Hide Modal</Text>
          //     </TouchableHighlight>
          //   </View>
          // </View>
    );
  }

  showPicker = () => {
    const { startYear, endYear, selectedYear, selectedMonth } = this.state;
    this.picker.show({startYear, endYear, selectedYear, selectedMonth})
      .then(({year, month}) => {
        if (this.state.isFromSelected){
          this.setState({
            selectedYear: year,
            selectedMonth: month,
            fromSelectedYear: year,
            fromSelectedMonth: month,
        })} else {
          this.setState({
            selectedYear: year,
            selectedMonth: month,
            toSelectedYear: year,
            toSelectedMonth: month,
        })}
        this.validateWorkExperience();
      });
  }

  onPressFromDate(){
    this.setState({isFromSelected: true});
    this.showPicker();
  }

  onPressToDate(){
    this.setState({isFromSelected: false});
    this.showPicker();
  }

  setTitle(title){
    this.setState({title});
    this.validateWorkExperience();
  }

  setCompany(company){
    this.setState({company});
    this.validateWorkExperience();
  }

  render(){
    const { fromSelectedMonth, fromSelectedYear,
       toSelectedMonth, toSelectedYear, requestInProgress} = this.state;
    return ( 
      <View style={styles.container}>
        {requestInProgress && (
        <View style={styles.overlay}>
          <ActivityIndicator style={styles.activityIndicator} size="large" color={GlobalKeys.Colors.appPrimaryBlueColor}/>
        </View>
        )}
        <GlobalKeys.Components.HeaderWSettings renderLeftButton={true}
          onLeftButtonPress={()=> this.props.goBack()} title="Add Work Experience"
          headerBackgroundColor={GlobalKeys.Colors.jobDetailHeaderBGColor} />

        <Content>
        <Modal
          animationType="slide"
          transparent={false}
          visible={this.state.modalVisible}
          onRequestClose={() => {
            Alert.alert('Modal has been closed.');
          }}>
          {this.renderLocationModal()}
          </Modal>
        <Form style={styles.form}>
          <Item style={styles.fullInputContainer}>
            <Input placeholder="Title" style={styles.inputStyle} onChangeText={(title) => this.setTitle(title)}/>
          </Item>
          <Item style={styles.fullInputContainer}>
            <Input placeholder="Company" style={styles.inputStyle} onChangeText={(company) => this.setCompany(company)} />
          </Item>
          <TouchableWithoutFeedback onPress={() => this.setState({modalVisible: true})}>
            {this.state.locationName === "" ?
              <View style={styles.locationContainer}>
                <Text style={styles.placeHolderText}>Location</Text>
              </View>
            :
            <View style={styles.locationContainer}>
              <Text style={styles.locationText}>{this.state.locationName}</Text>
            </View>
            }
          </TouchableWithoutFeedback>
          <View style={styles.halfContainer}>
            <TouchableWithoutFeedback onPress={() => this.onPressFromDate()}>
              {fromSelectedYear === "" ? 
              <View style={styles.datePickerContainer}>
                <Text style={styles.placeHolderText}>From</Text>
              </View>
              :
              <View style={styles.datePickerContainer}>
                <Text style={styles.locationText}>{fromSelectedYear}-{fromSelectedMonth}</Text>
              </View>
            }
            </TouchableWithoutFeedback>
            <TouchableWithoutFeedback onPress={() => this.onPressToDate()}>
              {toSelectedYear === "" ? 
              <View style={styles.datePickerContainer}>
                <Text style={styles.placeHolderText}>To</Text>
              </View>
              :
              <View style={styles.datePickerContainer}>
                <Text style={styles.locationText}>{toSelectedYear}-{toSelectedMonth}</Text>
              </View> 
              }
            </TouchableWithoutFeedback>
    
          </View>
            <Button style={styles.addEduButton} disabled={!this.state.enableSubmitButton} onPress={() => this.addWorkExperience()}>
              <Text>Add Work Experience</Text>
            </Button>
          {/* </Item> */}
          {/* <Item style={{ marginTop: deviceHeight * .03,}}>
            <Textarea rowSpan={3} bordered placeholder="Description" 
              style={styles.descriptionContainer}
            />
          </Item> */}
      </Form>
        </Content>
        <DropdownAlert
          ref={this.dropdownCallback.bind(this)}
        />
        <YearMonthPicker ref={(picker) => this.picker=picker}/>
      </View>
    );
  }
};

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = MediaQueryStyleSheet.create({
  container: {
    flex: 1,
    // width: deviceWidth * .9,
    backgroundColor: Colors.appPrimaryLightBackground,
  },
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 100,
    backgroundColor: Colors.appPrimaryLightBackground,
    opacity: 0.6,
    justifyContent:'center',
    alignItems:'center'
  },
  activityIndicator:{
    alignSelf:'center'
  },
  form:{
    width: deviceWidth * .9,
    // backgroundColor:'pink',
  },
  halfInputContainer:{
    justifyContent:'space-between', 
    borderColor: Colors.appPrimaryLightBackground,
    // backgroundColor:'red',
  },
  fullInputContainer:{
    borderColor: Colors.singinLightBorder,
  },
  halfInput:{
    width:'45%',
    maxWidth:'45%',
    borderBottomColor:Colors.singinLightBorder,
    borderBottomWidth: 1,
    fontFamily: Fonts.LIGHT,
    color: Colors.appHeaderDarkColor,
},
  descriptionContainer:{
    borderColor:Colors.singinLightBorder,
    width: '100%',
    fontFamily: Fonts.LIGHT,
    color: Colors.appHeaderDarkColor,
    // width: deviceWidth * .9
  },
  inputStyle:{
    fontFamily: Fonts.LIGHT,
    color: Colors.appHeaderDarkColor,
  },
  locationText:{
    fontFamily: Fonts.LIGHT,
    color: Colors.appHeaderDarkColor,
  },
  placeHolderText:{
    fontFamily: Fonts.LIGHT,
    color: Colors.placeHolderTextColor,
  },
  addEduButton:{
    marginTop: deviceHeight * .05,
    alignSelf: 'center',
  },
  locationContainer:{
    height: deviceHeight * .078,
    width: deviceWidth * .86,
    // backgroundColor:'red',
    alignSelf: 'flex-end',
    justifyContent:'center',
    paddingLeft:deviceWidth * .015,
    borderBottomColor: Colors.singinLightBorder,
    borderBottomWidth:1,
  },
  halfContainer:{
    flexDirection:'row',
    justifyContent:'space-between',
  },
  datePickerContainer:{
    height: deviceHeight * .078,
    width: deviceWidth * .4,
    // backgroundColor:'red',
    alignSelf: 'flex-end',
    justifyContent:'center',
    marginLeft:deviceWidth * .04,
    borderBottomColor: Colors.singinLightBorder,
    borderBottomWidth:1,
    paddingLeft:deviceWidth * .015,
  },
});

// num: 3 as number
const defaultStateProps = {
  // errorMessages: any,
  errorMessages:{} as any,
  profile: {} as any,
  loadingRequest: true as boolean,
}

type DispProps = {
  navigate: (any) => void
  requestAddWorkExperience: (any) => void,
  goBack: () => void,
}

const mapStateToProps = (state: Types.RootState): StateProps => {
  const {
    profile:{
      profile,
      errorMessages,
      loadingRequest
    }
  } = state;

  return {
    errorMessages,
    profile,
    loadingRequest,
  };
};

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>) : DispProps => ({
  navigate: (route) => dispatch(navigate(route)),
  requestAddWorkExperience: (any) => dispatch(requestAddWorkExperience(any)),
  goBack: () => dispatch(goBack()),
});

const enhance = connect<StateProps, DispProps>(
  mapStateToProps, mapDispatchToProps
)

export default enhance(AddWorkExperience);
// export default connect(mapStateToProps)(mapDispatchToProps)(AddWorkExperience);

