/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */
"use strict";
import React, { Component } from 'react';
import {
  Platform, FlatList, ImageBackground, TextInput, ViewStyle,
  StyleSheet,Alert, Image, TouchableOpacity, ActivityIndicator,
  View, Dimensions, Text, StatusBar, Modal as RNModal,
  KeyboardAvoidingView, ImageSourcePropType, SafeAreaView,
} from 'react-native';
import { connect } from 'react-redux';
import Types from 'Types';
import { Dispatch } from 'redux';
import Routes from '../../constants/Routes';

import { Container, Header, Left, Body, Right, Button, Icon,
  Title, Content, Input,Item, Form, Text as NBText
 } from 'native-base';

import GlobalKeys from '../../constants';
import styles from './styles';
import Icons from '../../constants/Icons';
import Images from '../../constants/Images';
import { navigate, resetRoute } from '../../redux/actions/nav';
import { isIphoneX } from 'react-native-iphone-x-helper';
import ButtonForwardIcon, {StylesT } from '../../components/ButtonFowardIcon';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const defaultProps = Object.freeze({
});

type OwnProps = typeof defaultProps;
type StateProps = typeof defaultStateProps;
type Props = StateProps & DispProps & OwnProps;

type State = {
  loading: boolean
};

class GettingStarted extends Component<Props, State> {

  subTitle: string = 'We are happy to see you joining our app to make your life faster and easier. Proceed ahead to sign up or log in.';

  constructor(props: Props){
    super(props);
    this.state = { 
      loading:false,
    };
  }

  componentDidMount() {
  }

  goToNextScreen(route: any): void{
    this.props.navigate(route);
  }

  goToHome(): void {
    this.props.resetRoute();
  }

  getBackgroundImage():ImageSourcePropType{
    let v: ImageSourcePropType = Images.Splash;
    if (isIphoneX()) {
      v = Images.SplashX;
    }
    return  v;
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <StatusBar backgroundColor="blue" barStyle="light-content" />
        <ImageBackground source={Images.splashBackground} resizeMode="cover" style={styles.splash}>
          <ActivityIndicator animating={this.state.loading} style={{paddingTop:deviceHeight * .49, zIndex:2, position:'absolute', alignSelf:'center'}} />
          <Image source={Images.logoHolo} style={styles.logoHolo}/>
          <Text style={styles.title}>Welcome to Book It Up!</Text>
          <Text style={styles.subTitle}>{this.subTitle}</Text>

          <Text style={styles.subTitle}>Continue as a</Text>
          <ButtonForwardIcon name="User" 
            style={styles.button as ViewStyle}
            onPress={() => this.props.navigate({ routeName: Routes.SignIn })} 
          />
          <ButtonForwardIcon name="Company"
            style={styles.button as ViewStyle}
            onPress={() => this.props.navigate({ routeName: Routes.SignIn })} 
          />
        </ImageBackground>
      </SafeAreaView>
    );
  }
}

const defaultStateProps = {
  // mapStateToProps objects
}

type DispProps = {
  navigate: (any) => void,
  resetRoute: () => void,
}

const mapStateToProps = (state: Types.RootState): StateProps => ({
});

const mapDispatchToProps = (dispatch: Dispatch<Types.RootAction>) : DispProps => ({
  navigate: (route: any) => dispatch(navigate(route)),
  resetRoute: () => dispatch(resetRoute()),
});

export default connect(mapStateToProps, mapDispatchToProps)(GettingStarted);

