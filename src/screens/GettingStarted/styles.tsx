import Colors from '../../constants/Colors';
import { Dimensions, Platform, ViewStyle, StyleSheet, TextStyle, ImageStyle } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

type Style = {
  container?: ViewStyle,
  splash?: ImageStyle,
  logoHolo?: ImageStyle,
  title?: TextStyle,
  subTitle?: TextStyle,
  button?: ViewStyle,
}

const styles = StyleSheet.create<Style>({
  container: {
    flex: 1,
    backgroundColor: Colors.quickRateBtnColor,
    alignItems: 'center',
  },
  splash:{
    width: deviceWidth,
    height:deviceHeight,
    flex:1,
    alignItems: 'center',
  },
  logoHolo:{
    width: deviceHeight * .254 * .56,
    height: deviceHeight * .254,
    marginTop: deviceHeight * .1,
  },
  title:{
    ...ifIphoneX({
      fontSize: responsiveFontSize(3.4),
    }, {
      fontSize: responsiveFontSize(4),
      // letterSpacing: 1.4,
    }),
    color: Colors.white,
    marginTop: deviceHeight * .053,
    width: deviceWidth * .894,
    textAlign:'center',
    fontFamily:'Raleway-Light',
  },
  subTitle:{
    ...ifIphoneX({
      fontSize: responsiveFontSize(2.3),
    }, {
      fontSize: responsiveFontSize(2.5),
    }),
    color: Colors.white,
    marginTop: deviceHeight *.024,
    textAlign:'center',
    width: deviceWidth * .74,
    fontFamily:'Raleway-Light',
  },
  button: {
    marginTop: deviceHeight * .03
  }
});

export default styles;