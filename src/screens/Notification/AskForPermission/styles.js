import { MediaQueryStyleSheet } from 'react-native-responsive';
import Colors from '../../../constants/Colors';
import { Dimensions, Platform } from 'react-native';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import Fonts from '../../../constants/Fonts';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = MediaQueryStyleSheet.create({
    container: {
      flex: 1,
      backgroundColor: Colors.white,
    },
    bellContainer:{
      height: deviceWidth * .279,
      // backgroundColor:'red',
      marginTop: deviceHeight * .06,
      alignItems: 'center',
    },
    bell:{
      width: deviceWidth * .256,
      height: deviceWidth * .279,
    },
    form:{
      // backgroundColor:'purple',
      alignItems: 'center',
      alignSelf: 'center',
      width: deviceWidth * .846,
    },
    update:{
      fontSize: responsiveFontSize(2.3),
      fontFamily: Fonts.SEMIBOLD,
      color: Colors.signUpEntryTitle,
      marginTop: deviceHeight * .06,
      ...ifIphoneX({
        fontSize: responsiveFontSize(1.9),
      }, {
      fontSize: responsiveFontSize(2.2),
      })
    },
    detail:{
      ...ifIphoneX({
        fontSize: responsiveFontSize(1.5),
      }, {
      fontSize: responsiveFontSize(1.8),
      }),
      fontFamily: Fonts.REGULAR,
      color: Colors.optDescriptionText,
      marginTop: deviceHeight * .034,
      textAlign:'center',
      lineHeight: deviceHeight * .04,
      width: deviceWidth * .78,
    },
    buttonSetting:{
      height: deviceHeight * .083,
      backgroundColor: Colors.appPrimaryBlueColor,
      marginTop: deviceHeight * .096,
    },
    settingText:{
      fontFamily: Fonts.BOLD,
      fontSize: responsiveFontSize(2.4),
    },
    buttonNotNow:{
      height: deviceHeight * .083,
      backgroundColor: Colors.white,
      borderColor: Colors.signUpEntryTitle,
      borderWidth:2,
      marginTop: deviceHeight * .04,
    },
    notNowText:{
      fontFamily: Fonts.BOLD,
      fontSize: responsiveFontSize(2.4),
      color: Colors.signUpEntryTitle,
    },
});

export default styles;