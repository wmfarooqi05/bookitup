
import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  StyleSheet,
  View,
  TouchableOpacity, TouchableHighlight,
  Image, StatusBar,
  Dimensions, Alert, PermissionsAndroid,
  AsyncStorage, ActivityIndicator, 
} from 'react-native';

import { Container, Header, Left, Body, Right, Button, Icon,
  Title, Content, Input,Item, Form,
  Text } from 'native-base';



import styles from './styles';

import Components from '../../../constants/Components';
import Images from '../../../constants/Images';
import Icons from '../../../constants/Icons';
import Routes from '../../../constants/Routes';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;




const navigatorStyle = { navBarHidden: true };

class NotifcationPermission extends Component {
  
  welomeMessageString = 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.';

  constructor(props){
    super(props);
    this.state = { 
    };
  }
  
  detail = 'We will send you the latest jobs with 2 daily notifications. You can change his within your settings';

  render() {

    return(
      <Container style = {styles.container}>
        <StatusBar/>
        <Components.StatusBar/>
        
        <Form style={styles.form}>
        <View style={styles.bellContainer}>

        <Image source={Icons.BellNotificaiton} style={styles.bell}/>
        </View>
        <Text style={styles.update}>Keep Up to Date</Text>
        <Text style={styles.detail}>{this.detail}</Text>
        <Button rounded block style={styles.buttonSetting}>
          <Text style={styles.settingText}>Go To Setting</Text>
        </Button>
        <Button rounded block style={styles.buttonNotNow}>
          <Text style={styles.notNowText}>Not Now</Text>
        </Button>
        </Form>
      </Container>
    )
  }
  
}

const mapStateToProps = state => ({
});

const mapDispatchToProps = dispatch => ({
});

export default connect(mapStateToProps, mapDispatchToProps)(NotifcationPermission);
