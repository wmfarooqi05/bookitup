import { 
  createBottomTabNavigator,
} from 'react-navigation';
import JobSearchResult from '../screens/User/Jobs/JobSearchResult';
import NotifcationPermission from '../screens/Notification/AskForPermission';
import MyJobs from '../screens/User/Jobs/MyJobs';
import Settings from '../screens/User/Settings/Settings';
import UserProfileView from '../screens/User/Profile/ProfileView';
import Colors from '../constants/Colors';
import { View, Image, StyleSheet, ImageBackground, Dimensions, } from 'react-native';
import Icons from '../constants/Icons';
import React from 'react';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const BottomTabs = createBottomTabNavigator(
  {
    JobSearchResult: {
      screen: JobSearchResult,
      navigationOptions: {
        // tabBarLabel: Strings.mainpage,
        tabBarIcon: ({ tintColor }) =>
          (<Image source={Icons.NavHomeIcon} style={[styles.icon, { tintColor }]} />)
      }
    },
    Notification: {
      screen: NotifcationPermission,
      navigationOptions: ({ screenProps }) => ({
        tabBarIcon: ({ tintColor }) => 
          (
            <View>
              <Image source={Icons.NavNotificationIcon} resizeMode="contain" style={[styles.icon, { tintColor }]} >
              </Image>
              {screenProps.badgeActive ? getNotifBadge() : null }
            </View>
          )
      })
    },
    MyJobs: {
      screen: MyJobs,
      navigationOptions: {
        // tabBarLabel: Strings.offers,
        tabBarIcon: ({ tintColor }) =>
          (<Image source={Icons.IconLoc} style={[styles.pointerIcon, { tintColor }]} />)
      }
    },
    Settings: {
      screen: Settings,
      navigationOptions: ({ screenProps }) => ({
        // tabBarLabel: Strings.orders,
        tabBarIcon: ({ tintColor }) =>
          (<Image source={Icons.SettingsIcon} style={[styles.icon, { tintColor }]} />)
      })
    },
    Profile: {
      screen: UserProfileView,
      navigationOptions: {
        // tabBarLabel: Strings.more,
        tabBarIcon: ({ tintColor }) =>
          (<Image source={Icons.NavUserIcon} resizeMode="contain" style={[styles.icon, { tintColor }]} />)
      }
    }
  },
  {
    // navigationOptions: ({ navigation }) => ({
    //   // tabBarIcon: ({ focused, tintColor }) => {
    //   tabBarIcon: () => {
    //     const { routeName } = navigation.state;
    //     let iconName;
    //     if (routeName === 'Offers') {
    //       iconName = Icons.offers;
    //     }
    //     return <Image source={iconName} style={{ width: 25, height: 25 }} />;
    //   },
    // }),
    initialRouteName: 'JobSearchResult',
    tabBarPosition: 'bottom',
    swipeEnabled: false,
    animationEnabled: true,
    tabBarOptions: {
      activeTintColor: Colors.navIconActiveTint,
      inactiveTintColor: Colors.navIconInactiveTint,
      showLabel: false,
      style: {
        // paddingTop: 10,
      }
    }
  }
);

export default BottomTabs;


const styles = StyleSheet.create({
	tab: {
		padding: 5
	},
	indicator: {
		width: 0,
		height: 0
	},
	label: {
		fontSize: 10
	},
	icon: {
		width: 20,
		height: 20,
		backgroundColor: Colors.transparent,
  },
  pointerIcon:{
		width: 40,
		height: 40,
		backgroundColor: Colors.transparent,
  },
	tabBar: {
		backgroundColor: Colors.darkBlue,
	},
  badge:{
    height: deviceWidth * .014,
    width: deviceWidth * .014,
    borderRadius: deviceWidth * .007,
    backgroundColor:'red',
    position:'absolute',
    right: 1,
    top: 4,
    // marginLeft: deviceWidth * .037,
    // marginTop: deviceWidth * .01,
  },
});