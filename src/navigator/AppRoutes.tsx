import { View, Image, StyleSheet, ImageBackground, Dimensions, } from 'react-native';

//import Main from '../screens';
import Colors from '../constants/Colors';

import GettingStarted from '../screens/GettingStarted';
import Splash from '../screens/SplashScreen';
import SignUp from '../screens/User/Auth/SignUp';
import SignIn from '../screens/User/Auth/SignIn';
import ForgotPassword from '../screens/User/Auth/ForgotPassword';

import Hello from '../Hello';

import Routes from '../constants/Routes';
import Icons from '../constants/Icons';
const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const AppRoutes = {
  Splash: {
    screen: Splash,
    navigationOptions: {
      header: null,
    }
  },
  Hello: {
    screen: Hello,
    navigationOptions: {
      header: null,
    }
  },
  GettingStarted: {
    screen: GettingStarted,
    navigationOptions: {
      header: null,
    }
  },
  SignIn: {
    screen: SignIn,
    navigationOptions: {
      header: null,
    }
  },
  SignUp: {
    screen: SignUp,
    navigationOptions: {
      header: null,
    }
  },
  ForgotPassword:{
    screen: ForgotPassword,
    navigationOptions: {
      header: null,
    }
  },
};

export default AppRoutes;