import AppRoutes from './AppRoutes';
import { 
    createStackNavigator,
  } from 'react-navigation';
import Routes, { initialRouteName } from '../constants/Routes';

//const AppNavigator = createStackNavigator(AppRouteConfigs);


export const AppNavigator = new createStackNavigator(AppRoutes, {
  initialRouteName,
});

