import React, { Component } from 'react';
import { View, Image, Text } from 'react-native';

import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { createStackNavigator } from 'react-navigation';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';

import { AppNavigator } from './AppNavigator';

const navReducer = createNavigationReducer(AppNavigator);

const middleware = createReactNavigationReduxMiddleware(
  'root',
  state => state.nav
);

const App = reduxifyNavigator(AppNavigator, 'root');

const mapStateToProps = state => ({
  state: state.nav,
});

export default AppWithNavigationState = connect(mapStateToProps)(App);

export { middleware, navReducer };