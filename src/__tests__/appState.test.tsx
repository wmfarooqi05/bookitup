import appState from '../redux/reducers/appState';

interface IState {
    appState: string,
  }
  
interface IAction {
    type: string;
    state: any;
}

const INITIAL_STATE: IState = { appState: 'NO_STATE'};
const UNDEF_STATE: IAction = { type: 'unexpected', state: null };

// describe('Return NO_STATE as default', () => {
//     test('it will return NO_STATE', () => {
//         expect(appState(undefined, UNDEF_STATE )).toEqual( INITIAL_STATE );
//     }); 
// });

describe('Return passed state if pass ADD_NEW_STATE', () => {
    test('will return passed state', () => {
        // expect(appState({state: 'TEST_STATE'}, { type: 'ADD_NEW_STATE'})).toEqual('ADD_NEW_STATE');
        expect(appState(undefined, { type: 'ADD_NEW_STATE', state: 'TEST_STATE'})).toEqual({ lastDispatchedAction: 'TEST_STATE' });
    }); 
});
