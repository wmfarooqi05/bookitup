import deepFreeze from 'deep-freeze';

function addCounter (list: Array<number>): Array<number> {
    return [...list, 0]; // new es6 syntax
}



function testAddCounter() {
    const listBefore: Array<number> = [];
    const listAfter: Array<number>= [0];

    deepFreeze(listBefore);

    expect( addCounter(listBefore)).toEqual(listAfter);
}

function removeCounter (list: Array<number>, index: number) {
    // splice mutate the array, that's why we use slice. 
    return [
        ...list.slice(0, index).concat(...list.slice(index+1, list.length))
    ]
};

function testRemoveCounter() {
    const listBefore: Array<number> = [0, 10, 20, 30];
    const listAfter: Array<number>= [0, 10, 30];

    deepFreeze(listBefore);

    expect( removeCounter(listBefore, 2)).toEqual(listAfter);

}

function incrementCounter(list: Array<number>, index: number){
    return [...list.slice(0, index), list[index] + 1, ...list.slice(index+1)];
}

function testIncrementCounter() {
    const listBefore: Array<number> = [0, 10, 20, 30];
    const listAfter: Array<number>= [0, 10, 21, 30];

   deepFreeze(listBefore);

    expect( incrementCounter(listBefore, 2)).toEqual(listAfter);
}

function toggleTodo( todo ){
    return {...todo, status: !todo.status}
}

function testToggleTodo(){
    const todoBefore = {
        id: 0,
        text: 'Learn Redux',
        status: false
    };
    const todoAfter = {
        id: 0,
        text: 'Learn Redux',
        status: true
    };
    deepFreeze(todoBefore);
    expect(toggleTodo(todoBefore)).toEqual(todoAfter);
}

describe('Return passed state if pass ADD_NEW_STATE', () => {
    test('will return passed state', () => {
        // expect(appState({state: 'TEST_STATE'}, { type: 'ADD_NEW_STATE'})).toEqual('ADD_NEW_STATE');
        testAddCounter();
        testRemoveCounter();
        testIncrementCounter();
        testToggleTodo();
    }); 
});