import Api from '../services/request';
import { Platform, NetInfo } from 'react-native';
import FormData from 'form-data';

function makePostRequest(
  url = '',
  requestPayload = {},
  needsAuthentication = false,
  customURL = '',
  authToken = null,
) {
  return Api.post({
    url,
    requestPayload,
    needsAuthentication,
    customURL,
    authToken,
  });
}

function makeGetRequest(url = '', queryString = '', needsAuthentication = false, authToken=null) {
  // return " ";
   Api.get({
    url,
    queryString,
    needsAuthentication,
    authToken,
  });
}

function makeDeleteRequest(url = '', queryString = '', needsAuthentication = false) {
  return Api.delete({
    url,
    queryString,
    needsAuthentication,
  });
}

function isNetworkConnected() {
  if (Platform.OS === 'ios') {
    return new Promise(resolve => {
      const handleFirstConnectivityChangeIOS = isConnected => {
        NetInfo.isConnected.removeEventListener('change', handleFirstConnectivityChangeIOS);
        resolve(isConnected);
      };
      NetInfo.isConnected.addEventListener('change', handleFirstConnectivityChangeIOS);
    });
  }

  return NetInfo.isConnected.fetch();
}

function makeFormDataObject(requestPayload: {}) {
  let formData = new FormData()
  for (var property in requestPayload) {
    if (requestPayload.hasOwnProperty(property)) {
      formData.append(property, requestPayload[property]);
        // console.log(requestPayload[property]);
        // console.log(property)
    }
  }
  return formData;
}

export {
  makeDeleteRequest,
  makeGetRequest,
  makePostRequest,
  isNetworkConnected,
  makeFormDataObject,
};

