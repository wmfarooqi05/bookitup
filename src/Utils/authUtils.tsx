import URLS from '../constants/URLS';

const isNeededToken = needAuth => needAuth;

function needsUrlApiPrefix(customURL){

  let apiURL = URLS.apiURL;

  try{
  if (customURL.includes(apiURL)){
  	return false;
  } else {
 		return true;
  }
  } catch(e){return true};
}

function buildURL(method, apiURL, url){
  if (url.includes(apiURL))
    return url;
  else {
    let str="";
    if (url.charAt(0) === "/")
      str = apiURL + url;
    else str = apiURL + "/" + url;
    
    return str;
  }
}

function embedTokenInHeaders(headers, authToken){
  console.log('inside embed');
  // TODO: get auth token from reducer
  let _auth = JSON.stringify({'x_auth_token': authToken});
  headers = {
    ...headers,
    "x_auth_token": authToken,
  }
  console.log(headers);
  return headers;
}

export { needsUrlApiPrefix, buildURL, embedTokenInHeaders };

export default isNeededToken;
