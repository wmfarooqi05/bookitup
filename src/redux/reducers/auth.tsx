/* TODOS
  - @TODO create a `LocalData` dir and move all the AsyncStorage stuff there.
  - @TODO Move all the initial States to the global file `InitialStates`
  and make a switch among all the reducers.
*/

import { AsyncStorage } from 'react-native';
import { isUndefined } from 'lodash';
import User from '../../services/User';
import URLS from '../../constants/URLS';
import deepFreeze from 'deep-freeze';

import {
  // Successes (Alphabetical)
  FETCH_USER_INFO_SUCCESS,
  FINISH_ONBOARDING_SUCCESS,
  GUEST_AUTH_SUCCESS,
  LOCAL_SIGNIN_SUCCESS,
  AUTH_TOKEN_SIGNIN_SUCCESS,
  UPDATE_USER_EMAIL_SUCCESS,
  UPDATE_USERNAME_SUCCESS,
  SIGNUP_SUCCESS,

  // Failures (Alphabetical)
  FETCH_USER_INFO_FAIL,
  GUEST_AUTH_FAIL,
  LOCAL_SIGNIN_FAIL,
  SIGNUP_FAIL,

  // Miscellaneous (Alphabetical)
  ACTIVATE_AUTH_LOADER,
  SET_USER_TOKEN,
  LOGIN_AFTER_EFFECTS_COMPLETED,
  SIGN_OUT,
  UPDATE_USER_SUBSCRIPTION,
  SIGNUP_INPROGRESS,

  UPDATE_USER_PROFILE_SUCCESS,

  USER_ID,

  ADD_USER_EDUCATION_SUCCESS,
  ADD_USER_EDUCATION_FAIL,
  ADD_USER_EDUCATION_INPROGRESS,
} from '../constants';

export type CURR_USER_TYPE = {
  profile: any, // undecided what to do, will be decided later
  userID: string
};

export const INITIAL_USER: CURR_USER_TYPE = {
  profile: {},
  userID: "",
}

type AUTH_STATE_TYPE = {
  currentUser: CURR_USER_TYPE,
  error: boolean,
  errorMessages: {} | null,
  loadingRequest: boolean,
  hasLoginAfterEffectsCompleted: boolean,
}

const INITIAL_AUTH_STATE: AUTH_STATE_TYPE = {
  currentUser: INITIAL_USER,
  error: false,
  errorMessages: {},
  loadingRequest: false,
  hasLoginAfterEffectsCompleted: true,
};

export default function (state = INITIAL_AUTH_STATE, action) {
  switch (action.type) {

    case LOCAL_SIGNIN_SUCCESS:
      // return state;
      if (action.payload.error) {
        // check this part
         return {
          ...state,
          currentUser: {
            ...state.currentUser,

            profile: {},
            userID: {},
            hasLoginAfterEffectsCompleted: false,
          },
          error: true,
          errorMessages: action.payload.message,
          hasLoginAfterEffectsCompleted: false,
        };
      }
      AsyncStorage.setItem(USER_ID, JSON.stringify(action.payload.data.UserID));
      User.setUserToken(action.payload.data.UserID);
      return setSignInSuccessPayload(state, action);

    case LOCAL_SIGNIN_FAIL:
      return {
        ...state,
        error: true,
        errorMessages: action.payload.error,
        hasLoginAfterEffectsCompleted: false,
      };

    case SIGNUP_SUCCESS:
      return {
        ...state,
        error: true,
        errorMessages: action.payload.message,
        hasLoginAfterEffectsCompleted: false,
      };
    case SIGNUP_FAIL:
      return {
        ...state,
        error: true,
        errorMessages: action.payload.message,
        hasLoginAfterEffectsCompleted: false,
      };

    case SIGNUP_INPROGRESS:
      return {
        ...state,
        error: true,
        errorMessages: action.payload.message,
        hasLoginAfterEffectsCompleted: false,
      };

    //Miscellaneous
    case ACTIVATE_AUTH_LOADER:
      return {
        ...state,
        loadingRequest: true,
      };

    case LOGIN_AFTER_EFFECTS_COMPLETED:
      return {
        ...state,
        hasLoginAfterEffectsCompleted: true,
      };
    case SIGN_OUT:
      AsyncStorage.removeItem(USER_ID);
      AsyncStorage.removeItem('currentUser');
      return INITIAL_AUTH_STATE;
    default:
      return state;
  }
}


function setSignInSuccessPayload(state, action): AUTH_STATE_TYPE {
  const userID = action.payload.data.UserId;
  const obj = {
    ...state,
    currentUser: {
      ...state.currentUser,
      userID: action.payload.data.UserId,
    },
    hasLoginAfterEffectsCompleted: false,
  }

  return obj;
}