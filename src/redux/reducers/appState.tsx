'use-strict';
import { ADD_NEW_STATE, NO_STATE, STATE_INITIALIZE } from '../actions/appSate';
 
interface IAction {
  type: string;
  state: any;
}

interface IState {
  lastDispatchedAction: string,
}

export const INITIAL_STATE: IState = { lastDispatchedAction: "NEW_STATE2"};

export default function(state = INITIAL_STATE, action: IAction) {
   
  switch(action.type) {
    
    case ADD_NEW_STATE:
      let _state: IState = { lastDispatchedAction: action.state };
      return _state;

    case STATE_INITIALIZE: 
      return state;
    default:
      return state;
   }
};