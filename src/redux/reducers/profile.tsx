// import { STATE_TYPE } from './appState';
// import deepFreeze from 'deep-freeze';

import {
    // Successes (Alphabetical)
    ADD_USER_EDUCATION_SUCCESS,
    FETCH_USER_INFO_SUCCESS,
    AUTH_TOKEN_SIGNIN_SUCCESS,
    ADD_USER_WORK_SUCCESS,
    ADD_USER_PERSONAL_STATEMENT_SUCCESS,
    ADD_USER_COVER_LETTER_SUCCESS,
    LOCAL_SIGNIN_SUCCESS,

    // Failures (Alphabetical)
    ADD_USER_EDUCATION_FAIL,
    FETCH_USER_INFO_FAIL,
    ADD_USER_WORK_FAIL,
    // Miscellaneous (Alphabetical)
    ACTIVATE_AUTH_LOADER,
    ADD_USER_COVER_LETTER_FAIL,
    ADD_USER_PERSONAL_STATEMENT_FAIL,

    // In Progress (Alphabetical)
    ADD_USER_EDUCATION_INPROGRESS,
    ADD_USER_WORK_INPROGRESS,
    ADD_USER_COVER_LETTER_INPROGRESS,
    ADD_USER_PERSONAL_STATEMENT_INPROGRESS,
  } from '../constants';
import { any } from 'prop-types';
  
export type PROFILE_STATE_TYPE = {

  fullName: string,
  address: string,
  contactNo: string,
  email: string,
  gender: string,
  country: string,
  birthDate: string,
  thumb: string,
  UserId: string,
  password: string,
}

export const initialProfileState: PROFILE_STATE_TYPE = {
  fullName: "",
  address: "",
  contactNo: "",
  email: "",
  gender: "",
  country: "",
  birthDate: "",
  thumb: "",
  UserId: "",
  password: "",
};

type STATE_TYPE = {
    profile?: PROFILE_STATE_TYPE | {},
    error: boolean,
    errorMessages: any | null,
    loadingRequest: boolean,
};

export const initialState: STATE_TYPE = {
    profile: initialProfileState,
    error: false,
    errorMessages: {},
    loadingRequest: false,
}

const ADD_PROFILE = 'ADD_PROFILE';

//TODO: update api data path in reducer
// i.e. education: payload.data......

  // revert back state to STATE_TYPE
const profile = (state: STATE_TYPE = initialState, action: any):STATE_TYPE => {
  switch (action.type) {

    case LOCAL_SIGNIN_SUCCESS:
    return {
      profile: addProfile(action),
      error: false,
      errorMessages:{},
      loadingRequest: false,
    };

    case AUTH_TOKEN_SIGNIN_SUCCESS:
      return {
        profile: addProfile(action),
        error: false,
        errorMessages:{},
        loadingRequest: false,
      };

    case FETCH_USER_INFO_SUCCESS:
    // console.log('fetch user calling');
      // AsyncStorage.setItem('currentUser', JSON.stringify(action.payload.newGuest));
      return {
        profile: addProfile(action),
        loadingRequest: false,
        errorMessages: {},
        error: false,
      }

    case ADD_USER_WORK_SUCCESS:
      return {
        profile: addWork(state, action),
        loadingRequest: false,
        errorMessages: {},
        error: false,
      }

    case ADD_USER_WORK_INPROGRESS:
      return {
        ...state,
        loadingRequest: true,
        errorMessages: {},
        error: false,
      }
    case ADD_USER_WORK_FAIL:
      return {
        ...state,
        error: true,
        loadingRequest: false,
        errorMessages: action.payload.error,
      }
      
    case FETCH_USER_INFO_FAIL:
      return {
        ...state,
        error: true,
        loadingRequest: false,
        errorMessages: action.payload.error,
      }

    case ADD_USER_EDUCATION_SUCCESS:
      return {
        profile: addEducation(state, action),
        loadingRequest: false,
        errorMessages: {},
        error: false,
      }

    case ADD_USER_EDUCATION_INPROGRESS:
      return {
        ...state,
        loadingRequest: true,
        errorMessages: {},
        error: false,
      }
    case ADD_USER_EDUCATION_FAIL:
      return {
        ...state,
        error: true,
        loadingRequest: false,
        errorMessages: action.payload.error,
      }

    case ADD_USER_COVER_LETTER_SUCCESS:
      return {
        profile: addCoverLetter(state, action),
        loadingRequest: false,
        errorMessages: {},
        error: false,
      }

    case ADD_USER_COVER_LETTER_INPROGRESS:
      return {
        ...state,
        loadingRequest: true,
        errorMessages: {},
        error: false,
      }
    case ADD_USER_COVER_LETTER_FAIL:
      return {
        ...state,
        error: true,
        loadingRequest: false,
        errorMessages: action.payload.error,
      }

    case ADD_USER_PERSONAL_STATEMENT_SUCCESS:
      return {
        profile: addPersonalStatement(state, action),
        loadingRequest: false,
        errorMessages: {},
        error: false,
      }

    case ADD_USER_PERSONAL_STATEMENT_INPROGRESS:
      return {
        ...state,
        loadingRequest: true,
        errorMessages: {},
        error: false,
      }
    case ADD_USER_PERSONAL_STATEMENT_FAIL:
      return {
        ...state,
        error: true,
        loadingRequest: false,
        errorMessages: action.payload.error,
      }

      default:
      return state;
  }
};

export default profile;

function addProfile(action: any): PROFILE_STATE_TYPE{
  return {
    fullName: action.payload.data.fullName || "",
    address: action.payload.data.address || "",
    contactNo: action.payload.data.contactNo || "",
    email: action.payload.data.email || "",
    gender: action.payload.data.gender || "",
    country: action.payload.data.country || "",
    birthDate: action.payload.data.birthDate || "",
    thumb: action.payload.data.thumb || "",
    UserId: action.payload.data.UserId || "",
    password: action.payload.data.password || "",  
  }

}

function addWork(state, action){

  let _state = state.profile;
  if (_state === undefined){
    alert('error: profile undefined');
    return tempProfileObj;
  }
  try{
  let obj = {
      ...state.profile, 
      experiences: [...state.profile.experiences, action.payload.data.profile.payload]
    };
    if (typeof obj !== undefined || obj !== null)
      return obj;
    else {
      return _state;
    }
  } catch(e){
    return _state;
  }
}

function addEducation(state, action):PROFILE_STATE_TYPE{
  
  let _state  = state.profile;
  if (_state === undefined){
    alert('error: profile undefined');
    return tempProfileObj;
  }
  try{
  let obj: PROFILE_STATE_TYPE = {
      ...state.profile, 
      education: [...state.profile.education, action.payload]
    };
    if (typeof obj !== undefined || obj !== null)
      return obj;
    else {
      return _state;
    }
  } catch(e){
    return _state;
  }
}

function addCoverLetter(state: STATE_TYPE, action: any):PROFILE_STATE_TYPE{

  let _state  = state.profile;
  if (_state === undefined){
    alert('error: profile undefined, temp profile added');
    return tempProfileObj;
  }
  try{
  let obj: PROFILE_STATE_TYPE = {
      ...state.profile, 
      coverLetter: action.payload.coverLetter,
    };
    if (typeof obj !== undefined || obj !== null)
      return obj;
    else {
      return _state;
    }
  } catch(e){
    return _state;
  }
}

function addPersonalStatement(state: STATE_TYPE, action: any):PROFILE_STATE_TYPE{

  let _state  = state.profile;
  if (_state === undefined){
    alert('error: profile undefined, temp profile added');
    return tempProfileObj;
  }
  try{
  let obj: PROFILE_STATE_TYPE = {
      ...state.profile, 
      personalStatement: action.payload.personalStatement,
    };
    if (typeof obj !== undefined || obj !== null)
      return obj;
    else {
      return _state;
    }
  } catch(e){
    return _state;
  }
}




const tempProfileObj: PROFILE_STATE_TYPE = {
  "_id": "5bc87c2526e9d51a8cbf37a1",
  "firstName": "ali",
  "lastName": "ali",
  "userTypeName": "HR",
  "email": "aliali@gmail.com",
  "contactNo": "03049798227",
  "userImage": "https://cap.stanford.edu/profiles/viewImage?profileId=19141&type=square&ts=1509532892453",
  "registrationDate": "2018-10-18T12:27:17.159Z",
  "experiences": [
    {
      "_id": "5be3f847e4f8f427b402dccc",
      "company": "Arkotech",
      "title": "React Native Developer",
      "address": {
          "mapLocation": {
              "latitude": 33.714372,
              "longitude": 73.159108
          },
          "country": "pakistan",
          "city": "Islamabad"
      },
      "instituteUniversityName": "FAST NUCES",
      "fromYear": "2012-10-18T12:27:17.159Z",
      "toYear": "2016-10-18T12:27:17.159Z",
    },
  ],
  "education": [
    {
      "_id": "5be3f847e4f8f427b402dccc",
      "certificateDegreeName": "BSCS",
      "major": "BSCS",
      "instituteUniversityName": "FAST NUCES",
      "startingDate": "2012-10-18T12:27:17.159Z",
      "endingDate": "2016-10-18T12:27:17.159Z",
      "percentage": 85,
      "cgpa": 3.4
    },
    {
      "_id": "5be41605e4f8f427b402dccd",
      "certificateDegreeName": "Metric",
      "major": "Metric",
      "instituteUniversityName": "FAST NUCES",
      "startingDate": "2012-10-18T12:27:17.159Z",
      "endingDate": "2016-10-18T12:27:17.159Z",
      "percentage": 85,
      "cgpa": 3.4
    }
  ],
  "currentSalary": null,
  "currency": null,
  "cv": null,
  "personalStatement": null,
  "coverLetter": null,
  "skills": null
}