import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import autoMergeLevel2 from 'redux-persist/lib/stateReconciler/autoMergeLevel2';
import { navReducer } from '../../navigator/AppWithNavState';
import appState from './appState';
import profile from './profile';
import auth from './auth';

const config = {
  key: 'primary',
  storage,
  // whitelist: ['auth', 'cart'],
  blacklist:["nav"],
  stateReconciler: autoMergeLevel2
};

export default persistCombineReducers(config, {
    appState,
    nav: navReducer,
    profile,
    auth,
});
