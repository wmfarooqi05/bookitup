/* eslint no-underscore-dangle: 0 */

import { take, select, call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { isEmpty } from 'lodash';
import {
  isJson,
} from '../../Utils';
import {
} from '../../services/auth';
import {
  ADD_USER_EDUCATION_REQUEST, 
  ADD_USER_EDUCATION_SUCCESS,
  ADD_USER_EDUCATION_FAIL,
  ADD_USER_EDUCATION_INPROGRESS,
  LOGIN_AFTER_EFFECTS_COMPLETED,
  ADD_USER_WORK_REQUEST,
  ADD_USER_WORK_INPROGRESS,
  ADD_USER_WORK_FAIL,
  ADD_USER_WORK_SUCCESS,

  ADD_USER_COVER_LETTER_REQUEST,
  ADD_USER_COVER_LETTER_FAIL,
  ADD_USER_COVER_LETTER_INPROGRESS,
  ADD_USER_COVER_LETTER_SUCCESS,

  ADD_USER_PERSONAL_STATEMENT_REQUEST,
  ADD_USER_PERSONAL_STATEMENT_FAIL,
  ADD_USER_PERSONAL_STATEMENT_INPROGRESS,
  ADD_USER_PERSONAL_STATEMENT_SUCCESS,
} from '../constants';

import API from '../../services/request';
import URLS from '../../constants/URLS';
import { requestAddEducation, requestAddWorkExperienceApi } from '../../services/profile';
import { getAuthToken } from '../../services/auth';
import { AsyncStorage, Alert } from 'react-native';

// onGettingAuthError(error?: Error | undefined, result?: string | undefined): void{
//   console.log(error);
// }
const delay = (ms) => new Promise(resolve => setTimeout(resolve, ms));

function* addEducation(reqPayload){
  try {
    console.log('b');
    const url = `${API.apiUrl + URLS.EDUCATION}`;

    yield put({ type: ADD_USER_EDUCATION_INPROGRESS });
    console.log('c');
    // const _auth = yield call(getAuthToken);
    const _auth = yield select(state => state.auth.currentUser.token);
    const { requestPayload: { school, degree, major, grade, fromYear, toYear }} = reqPayload;

    const requestPayload = {
      "certificateDegreeName": degree,
      "major": major,
      "startingDate": fromYear,
      "endingDate": toYear,
      "instituteUniversityName": school,
      // "cgpa": grade,
      "percentage": grade
    }
    console.log("auth befire", _auth);
    /*
    / working code
    // remove comment when connected with api network
    const resp = yield call(requestAddEducation, { 
      url, 
      requestPayload, 
      authToken: _auth ,
      needsAuthentication: true,
    });

    */
    yield call(delay, 5000);
    const payload = {
      "_id": "5be3f847e4f8f427b402dccc",
      "certificateDegreeName": "BSCS",
      "major": "BSCS",
      "instituteUniversityName": "FAST NUCES",
      "startingDate": "2012-10-18T12:27:17.159Z",
      "endingDate": "2016-10-18T12:27:17.159Z",
      "percentage": 85,
      "cgpa": 3.4
    };
    yield put({ type: ADD_USER_EDUCATION_SUCCESS, payload });
    /*
    let payload;
    const userData = resp;
    let responseData;

    if (isJson(userData._bodyText)) {
      responseData = JSON.parse(userData._bodyText);
    } else {
      responseData = { message: userData._bodyText };
    }

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            error:"Error 404, Not found"
          }
        }
      } else {
        payload = {
          error: responseData,
        };
      }
      yield put({ type: ADD_USER_EDUCATION_FAIL, payload });
    } else {
      payload = {
        data: responseData,
      };
      yield put({ type: ADD_USER_EDUCATION_SUCCESS, payload });
    }
    */
  } catch (error) {
    yield put({ type: ADD_USER_EDUCATION_FAIL, payload: { error } });
  }
}

function* addWorkExperience(reqPayload){
  console.log('b');
  try {
    const url = `${API.apiUrl + URLS.WORK}`;

    yield put({ type: ADD_USER_WORK_INPROGRESS });
    console.log('c');
    // const _auth = yield call(getAuthToken);
    const _auth = yield select(state => state.auth.currentUser.token);
    const { requestPayload: { company, title, locationPointer, locationName, fromYear, toYear }} = reqPayload;

    const requestPayload = {
      "company": company,
      "title": title,
      "locationPointer": locationPointer,
      "locationName": locationName,
      "fromYear": fromYear,
      "toYear": toYear
    }
    console.log("auth befire", _auth);
    const payload = {
      "_id": "5be3f847e4f8f427b402dccc",
      "company": company,
      "title": title,
      "address": {
          "mapLocation": locationPointer,
          "city": locationName
      },
      "fromYear": fromYear,
      "toYear": toYear,
    };
    console.log('before delay');

    yield call(delay, 5000);
    console.log('after delay');
    yield put({ type: ADD_USER_WORK_SUCCESS, payload });
    /*
    const resp = yield call(requestAddEducation, { 
      url, 
      requestPayload, 
      authToken: _auth ,
      needsAuthentication: true,
    });

    let payload;
    const userData = resp;
    let responseData;

    if (isJson(userData._bodyText)) {
      responseData = JSON.parse(userData._bodyText);
    } else {
      responseData = { message: userData._bodyText };
    }

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            error:"Error 404, Not found"
          }
        }
      } else {
        payload = {
          error: responseData,
        };
      }
      yield put({ type: ADD_USER_EDUCATION_FAIL, payload });
    } else {
      payload = {
        data: responseData,
      };
      yield put({ type: ADD_USER_EDUCATION_SUCCESS, payload });
    }
    */
  } catch (error) {
    yield put({ type: ADD_USER_EDUCATION_FAIL, payload: { error } });
  }

}

function* addCoverLetter(reqPayload){
  // alert('write this method');
  console.log('b');
  try {
    const url = `${API.baseUrl + URLS.COVER_LETTER}`;

    yield put({ type: ADD_USER_COVER_LETTER_INPROGRESS });
    console.log('c');
    // const _auth = yield call(getAuthToken);
    const _auth = yield select(state => state.auth.currentUser.token);
    const { requestPayload: { coverLetter }} = reqPayload;

    const requestPayload = {
      "coverLetter": coverLetter,
    }
    console.log("auth befire", _auth);
    const payload = {
      coverLetter
    };
    console.log('before delay');

    yield call(delay, 5000);
    console.log('after delay');
    yield put({ type: ADD_USER_COVER_LETTER_SUCCESS, payload });
    /*
    const resp = yield call(requestAddEducation, { 
      url, 
      requestPayload, 
      authToken: _auth ,
      needsAuthentication: true,
    });

    let payload;
    const userData = resp;
    let responseData;

    if (isJson(userData._bodyText)) {
      responseData = JSON.parse(userData._bodyText);
    } else {
      responseData = { message: userData._bodyText };
    }

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            error:"Error 404, Not found"
          }
        }
      } else {
        payload = {
          error: responseData,
        };
      }
      yield put({ type: ADD_USER_EDUCATION_FAIL, payload });
    } else {
      payload = {
        data: responseData,
      };
      yield put({ type: ADD_USER_EDUCATION_SUCCESS, payload });
    }
    */
  } catch (error) {
    yield put({ type: ADD_USER_COVER_LETTER_FAIL, payload: { error } });
  }
}

function* addPersonalStatement(reqPayload){
  // alert('write this method');
  console.log('b');
  try {
    const url = `${API.baseUrl + URLS.COVER_LETTER}`;

    yield put({ type: ADD_USER_PERSONAL_STATEMENT_INPROGRESS });
    console.log('c');
    // const _auth = yield call(getAuthToken);
    const _auth = yield select(state => state.auth.currentUser.token);
    const { requestPayload: { personalStatement }} = reqPayload;

    const requestPayload = {
      "personalStatement": personalStatement,
    }
    console.log("auth befire", _auth);
    const payload = {
      personalStatement
    };
    console.log('before delay');

    yield call(delay, 5000);
    console.log('after delay');
    yield put({ type: ADD_USER_PERSONAL_STATEMENT_SUCCESS, payload });
    /*
    const resp = yield call(requestAddEducation, { 
      url, 
      requestPayload, 
      authToken: _auth ,
      needsAuthentication: true,
    });

    let payload;
    const userData = resp;
    let responseData;

    if (isJson(userData._bodyText)) {
      responseData = JSON.parse(userData._bodyText);
    } else {
      responseData = { message: userData._bodyText };
    }

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            error:"Error 404, Not found"
          }
        }
      } else {
        payload = {
          error: responseData,
        };
      }
      yield put({ type: ADD_USER_EDUCATION_FAIL, payload });
    } else {
      payload = {
        data: responseData,
      };
      yield put({ type: ADD_USER_EDUCATION_SUCCESS, payload });
    }
    */
  } catch (error) {
    yield put({ type: ADD_USER_PERSONAL_STATEMENT_FAIL, payload: { error } });
  }
}

// export default function* authSaga() {
//   yield takeEvery(ADD_USER_EDUCATION_REQUEST, addEducation);
//   yield takeEvery(ADD_USER_WORK_REQUEST, addWorkExperience);
// }

export default function* authSaga() {
  yield takeEvery(ADD_USER_EDUCATION_REQUEST, addEducation);
  yield takeEvery(ADD_USER_WORK_REQUEST, addWorkExperience);
  yield takeEvery(ADD_USER_COVER_LETTER_REQUEST, addCoverLetter);
  yield takeEvery(ADD_USER_PERSONAL_STATEMENT_REQUEST, addPersonalStatement);
}
