/* eslint no-underscore-dangle: 0 */

import { take, select, call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { isEmpty } from 'lodash';
import {
  isJson,
} from '../../Utils';
import {
  getGuestAuthToken,
  requestCreateGuestUser,

  requestFetchUserInfo,

  requestlocalSignIn,
  requestAuthTokenSignIn,
  convertFacebookTokenToAuthToken,
  requestSignup,
  requestEditPassword,
} from '../../services/auth';
import {
  GUEST_AUTH_REQUEST,
  GUEST_AUTH_SUCCESS,
  GUEST_AUTH_FAIL,

  ACTIVATE_AUTH_LOADER,

  CREATE_GUEST_USER_REQUEST,
  CREATE_GUEST_USER_SUCCESS,
  CREATE_GUEST_USER_FAIL,

  FETCH_USER_INFO_REQUEST,
  FETCH_USER_INFO_SUCCESS,
  FETCH_USER_INFO_FAIL,

  LOCAL_SIGNIN_REQUEST,
  LOCAL_SIGNIN_SUCCESS,
  LOCAL_SIGNIN_FAIL,

  LOGIN_FROM_AUTH_TOKEN_REQUEST,
  AUTH_TOKEN_SIGNIN_SUCCESS,

  FACEBOOK_SIGNIN_REQUEST,
  SIGNUP_REQUEST,
  SIGNUP_INPROGRESS,
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
  LOGIN_AFTER_EFFECTS_COMPLETED,
  EDIT_PASSWORD_INPROGRESS,
  EDIT_PASSWORD_SUCCESS,
  EDIT_PASSWORD_FAIL,
  EDIT_PASSWORD_REQUEST,
  SIGN_OUT,

  FETCH_MY_PROFILE_SUCCESS,
} from '../constants';

import API from '../../services/request';
import URLS from '../../constants/URLS';

export function activateLoader() {
  return { type: ACTIVATE_AUTH_LOADER };
}

function authTokenTokenSuccess(payload) {
  return { type: GUEST_AUTH_SUCCESS, payload };
}

function authTokenTokenFail(payload) {
  return { type: GUEST_AUTH_FAIL, payload };
}

function* authTokenForGuest() {
  const url = 'me';
  try {
    const guestUserToken = yield call(getGuestAuthToken, { url, requestPayload: { trial: true } });

    const guestToken = JSON.parse(guestUserToken._bodyText);

    yield put(authTokenTokenSuccess({ guestToken }));
  } catch (error) {
    yield put(authTokenTokenFail({ error }));
  }
}

function createGuestUserSuccess(payload) {
  return { type: CREATE_GUEST_USER_SUCCESS, payload };
}

function createGuestUserFail(payload) {
  return { type: CREATE_GUEST_USER_FAIL, payload };
}

function* createGuestUser() {
  try {
    const url = 'me';

    yield put(activateLoader());

    const guestUser = yield call(requestCreateGuestUser, {
      url,
      requestPayload: {
        trial: true,
      },
    });

    yield put(createGuestUserSuccess({ guestUser }));
  } catch (error) {
    yield put(createGuestUserFail({ error }));
  }
}

function fetchUserInfoSuccess(payload) {
  return { type: FETCH_USER_INFO_SUCCESS, payload };
}

function fetchUserInfoFail(payload) {
  return { type: FETCH_USER_INFO_FAIL, payload };
}

function* fetchUserInfo(requestPayload) {
  try {
    const url = `${API.apiURL}/profile/myprofile`;
    // remove this
   const { payload: { token } } = requestPayload;

   yield put(activateLoader());

    const userLoginCall = yield call(requestAuthTokenSignIn, {
      url,
      queryString:`/profile/myprofile`,
      needsAuthentication: true,
      authToken: token ,
    });

    let payload;
    const userData = userLoginCall;
    let responseData;

    if (isJson(userData._bodyText)) {
      responseData = JSON.parse(userData._bodyText);
    } else {
      responseData = { message: userData._bodyText };
    }

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            error:"Error 404, Not found"
          }
        }
      } else {
        payload = {
          error: responseData,
        };
      }
      yield put(fetchUserInfoFail(payload));
    } else {
      payload = {
        data: responseData,
      };
      yield put(fetchUserInfoSuccess(payload));
    }
    
    // TODO: Guest Mode
    // const newGuest = JSON.parse(userInfo._bodyText);
    // yield put(fetchUserInfoSuccess({ newGuest }));

  } catch (error) {
    yield put(fetchUserInfoFail({ error }));
  }
}

function localSignInSuccess(payload) {
  return { type: LOCAL_SIGNIN_SUCCESS, payload };
}

function authTokenSignInSuccess(payload){
  return { type: AUTH_TOKEN_SIGNIN_SUCCESS, payload }
}

function localSigninFail(payload) {
  return { type: LOCAL_SIGNIN_FAIL, payload };
}

const dummyEmail = "alialialsi@gmail.com";
const dummyPassword = "123456";

function* localSignIn(requestPayload) {
  try {
    const url = URLS.SIGNIN;
   const { requestPayload: { email, password } } = requestPayload;

    const userLoginCall = yield call(requestlocalSignIn, {
      url,
      requestPayload: {
        email,
        password,
      },
      customURL: url,
    });

    console.log('payload reaced');

    let payload;
    const userData = userLoginCall;
    let responseData;

    if (isJson(userData._bodyText)) {
      responseData = JSON.parse(userData._bodyText);
    } else {
      responseData = { message: userData._bodyText };
    }

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            message:"Error 404, Not found"
          }
        }
      } else if (userData.status === 500){
        payload = {
          error: {
            message:"Error 500, Internal Server Error"
          }
        }
      } else {
        payload = {
          error: responseData,
        };
      }
      yield put(localSigninFail(payload));
    } else {
      payload = {
        data: responseData,
      };
      yield put(localSignInSuccess(payload));
    }
  } catch (error) {
    yield put(localSigninFail({ error }));
  }
}

function* signinFromAuthToken(requestPayload){
  console.log('b');
  try {
    const url = `${API.apiURL}/profile/myprofile`;
    // remove this
   const { payload: { token } } = requestPayload;
   console.log('c');

    const userLoginCall = yield call(requestAuthTokenSignIn, {
      url,
      queryString:`/profile/myprofile`,
      needsAuthentication: true,
      authToken: token ,
    });

    // let user12 = yield select(state => state.profile);

    console.log('payload reaced');

    let payload;
    const userData = userLoginCall;
    let responseData;

    if (isJson(userData._bodyText)) {
      responseData = JSON.parse(userData._bodyText);
    } else {
      responseData = { message: userData._bodyText };
    }

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            message:"Error 404, Not found"
          }
        }
      } else if (userData.status === 500){
        payload = {
          error: {
            message:"Error 500, Internal Server Error"
          }
        }
      }  else {
        payload = {
          error: responseData,
        };
      }
      yield put(localSigninFail(payload));
    } else {
      payload = {
        data: responseData,
      };
      yield put(authTokenSignInSuccess(payload));
    }
  } catch (error) {
    yield put(localSigninFail({ error }));
  }
}

function* facebookSignIn(requestPayload) {
  try {
    const customURL = `${API.apiURL}/auth/facebook/token`;
    // eslint-disable-next-line
    let { requestPayload: { access_token } } = requestPayload;

    const userLoginCall = yield call(convertFacebookTokenToAuthToken, {
      url: '',
      requestPayload: {
        access_token,
      },
      customURL,
    });

    let payload;
    const userData = userLoginCall;
    let responseData;

    if (isJson(userData._bodyText)) {
      responseData = JSON.parse(userData._bodyText);
    } else {
      responseData = { message: userData._bodyText };
    }

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            error:"Error 404, Not found"
          }
        }
      } else if (userData.status === 500){
        payload = {
          error: {
            message:"Error 500, Internal Server Error"
          }
        }
      } else {
        payload = {
          error: responseData,
        };
      }
      yield put(localSigninFail(payload));
    } else {
      payload = {
        data: responseData,
      };
      yield put(localSignInSuccess(payload));
    }
  } catch (error) {
    yield put(localSigninFail({ error }));
  }
}

function* signup(requestPayload) {
  try {
    const url = `${API.apiURL}/user/`;
    // requestPayload.requestPayload = {
    //   email: emai,
    //   firstName: 'Waleed',
    //   lastName: 'Farooqi',
    //   password: dummyPassword,
    //   userTypeName: 'JobUser' // always will be JobUser in mobile
    // };
    const { requestPayload: { firstName, lastName, email, password, userTypeName } } = requestPayload;

    yield put({ type: SIGNUP_INPROGRESS });

    const signupCall = yield call(requestSignup, {
      url,
      requestPayload: {
        firstName,
        lastName,
        email,
        password,
        userTypeName,
      },
    });

    let payload;
    const userData = signupCall;
    const responseData = JSON.parse(signupCall._bodyText);

    if (!userData.ok) {
      if (userData.status === 404){
        payload = {
          error: {
            error:"Error 404, Not found"
          }
        }
      } else {
        payload = {
          error: responseData,
        };
      }
      yield put({ type: SIGNUP_FAIL, payload });
    } else {
      payload = {
        error: null,
        data: responseData,
      };

      yield put(localSignInSuccess(payload));
      yield put({ type: SIGNUP_SUCCESS, payload });
    }
  } catch (error) {
    yield put({ type: SIGNUP_FAIL, payload: { error } });
  }
}


function* editPassword(reqPayload) {
  try {
    const { requestPayload } = reqPayload;

    const url = 'me/password';

    yield put({ type: EDIT_PASSWORD_INPROGRESS });

    const resp = yield call(requestEditPassword, { url, requestPayload });

    let responseData;
    if (isJson(resp._bodyText)) {
      responseData = JSON.parse(resp._bodyText);
    } else {
      responseData = { message: resp._bodyText };
    }

    if (!resp.ok) {
      if (resp.status === 404){
        payload = {
          error: {
            error:"Error 404, Not found"
          }
        }
      } else {
        const payload = {
          error: responseData,
        };
      }
      yield put({ type: EDIT_PASSWORD_FAIL, payload });
    } else {
      const payload = {
        data: responseData,
      };
      yield put({ type: EDIT_PASSWORD_SUCCESS, payload });
    }
  } catch (error) {
    yield put({ type: EDIT_PASSWORD_FAIL, payload: { error } });
  }
}

function* profileLoadingAfterEffects(){

  yield take(AUTH_TOKEN_SIGNIN_SUCCESS);
  try {
    yield call(fetchUserInfo);
    let user = yield select(state => state.profile);
    if (isEmpty(user.profile)) {
      alert('fetchUserInfo failed');
    }

    yield put({ type: LOGIN_AFTER_EFFECTS_COMPLETED });
  } catch (e) {
    yield put({ type: LOGIN_AFTER_EFFECTS_COMPLETED });
  }
}

/* used for API calls that needs to happen imediatly after successful login */
function* loginAfterEffects() {
  yield take(LOCAL_SIGNIN_SUCCESS);
  try {
    yield call(fetchUserInfo);
    let user = yield select(state => state.profile);
    if (isEmpty(user.profile)) {
      alert('fetchUserInfo failed');
    }

    yield put({ type: LOGIN_AFTER_EFFECTS_COMPLETED });
  } catch (e) {
    yield put({ type: LOGIN_AFTER_EFFECTS_COMPLETED });
  }
}
function* signout() {
  // write what to do on signout
}


export default function* authSaga() {
  yield takeEvery(CREATE_GUEST_USER_REQUEST, createGuestUser);
  yield takeEvery(LOGIN_FROM_AUTH_TOKEN_REQUEST, signinFromAuthToken)
  yield takeEvery(FETCH_USER_INFO_REQUEST, fetchUserInfo);
  yield takeEvery(GUEST_AUTH_REQUEST, authTokenForGuest);
  yield takeEvery(LOCAL_SIGNIN_REQUEST, localSignIn);
  yield takeEvery(FACEBOOK_SIGNIN_REQUEST, facebookSignIn);
  yield takeEvery(EDIT_PASSWORD_REQUEST, editPassword);
  yield takeEvery(SIGNUP_REQUEST, signup);
  yield takeEvery(SIGN_OUT, signout);
  yield takeLatest([
    LOCAL_SIGNIN_REQUEST,
    SIGNUP_REQUEST,
    FACEBOOK_SIGNIN_REQUEST, ], loginAfterEffects);
  yield takeLatest([LOGIN_FROM_AUTH_TOKEN_REQUEST], profileLoadingAfterEffects);
}
