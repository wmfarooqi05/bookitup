import { all } from 'redux-saga/effects';
import jobsSaga from './jobs';
import authSaga from './auth';

export default function* rootSaga() {
  yield all([
    authSaga(),
    jobsSaga(),
    // profileSaga(),
  ]);
}
