
import { take, select, call, put, takeEvery, takeLatest } from 'redux-saga/effects';
import { isEmpty } from 'lodash';
import {
  isJson,
} from '../../Utils';

import { FETCH_JOBS_REQUEST, FETCH_JOBS_SUCCESS, FETCH_JOBS_FAIL, SEARCH_JOBS, FETCH_JOBS_INPROGRESS,
  FETCH_MY_JOBS_REQUEST, FETCH_MY_JOBS_SUCCESS, FETCH_MY_JOBS_FAIL, FETCH_MY_JOBS_INPROGRESS
} from '../constants';
import { requestFetchJobs } from '../../services/jobs';

import API from '../../services/request';
import URLS from '../../constants/URLS';

import { isNetworkConnected } from '../../Utils/requestUtils';

function fetchJobsFailed(payload){
  return { type: FETCH_JOBS_FAIL, payload };
}

function fetchJobsSuccess(payload){
  return { type: FETCH_JOBS_SUCCESS, payload };
}

function fetchMyJobsFailed(payload){
  return { type: FETCH_MY_JOBS_FAIL, payload };
}

function fetchMyJobsSuccess(payload){
  return { type: FETCH_MY_JOBS_SUCCESS, payload };
}

function* fetchJobs(requestPayload){
  try {
    const url = `${API.baseUrl+URLS.GET_JOBS}`;
    const { requestPayload: { authToken } } = requestPayload;
    yield put({ type: FETCH_JOBS_INPROGRESS });

    const getNetworkStatus = yield call(isNetworkConnected);

    const status = getNetworkStatus;
    
    if (status) {
      const fetchJobsCall = yield call(requestFetchJobs, {
        url,
        queryString: null,
        needsAuthentication: true,
        authToken: authToken,
      });

      let payload;
      const jobsData = fetchJobsCall;
      let responceData;

      if (isJson(jobsData._bodyText)){
        responceData = JSON.parse(jobsData._bodyText);
      } else {
        responceData = { message: jobsData._bodyText }
      }

      if (!jobsData.ok) {
        if (jobsData.status === 404){
          payload = {
            error: {
              error:"Error 404, Not found"
            }
          }
        } else {
          payload = {
            error: responceData,
          };
        }
        yield put(fetchJobsFailed(payload));
      } else {
        payload = {
          data: responceData,
        }
        yield put(fetchJobsSuccess(payload));
      }
    } else {
      payload = {
        error: {
          error:"Error 404, Not found"
        }
      };
      yield put(fetchJobsFailed(payload));
    }
  } catch (error) {
    yield put(fetchJobsFailed({ error }));
  }
}

export default function* jobsSaga() {
  yield takeEvery('FETCH_JOBS_REQUEST', fetchJobs);
  // yield takeEvery(FETCH_MY_JOBS_REQUEST, fetchMyJobs);
}
