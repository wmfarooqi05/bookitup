export const ADD_NEW_STATE: string = 'ADD_NEW_STATE';
export const NO_STATE: string = 'NO_STATE';
export const STATE_INITIALIZE: string = 'STATE_INITIALIZE';

interface IAction {
  type: string;
  state: any;
}

interface IActionDefault {
  type: string;
}

export function addNewState(state: any): IAction {
  return {
    type: ADD_NEW_STATE,
    state,
  }
}

export function intializeState(): IActionDefault{
  return {
    type: STATE_INITIALIZE,
  }
}