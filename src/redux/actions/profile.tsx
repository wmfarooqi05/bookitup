import {
  ADD_USER_EDUCATION_REQUEST,
  ADD_USER_WORK_REQUEST,
  ADD_USER_COVER_LETTER_REQUEST,
  ADD_USER_PERSONAL_STATEMENT_REQUEST,
} from '../constants';
import Types from 'Types';
import { Dispatch } from 'redux';

function requestAddWork(requestPayload): any{
  return { type: ADD_USER_WORK_REQUEST, requestPayload }
}

export function requestAddEducation(requestPayload): Dispatch<Types.RootAction> {
  return dispatch => dispatch({
    type: ADD_USER_EDUCATION_REQUEST,
    requestPayload
  });
}

export function requestAddWorkExperience(requestPayload): Dispatch<Types.RootAction> {
  return dispatch => dispatch(requestAddWork(requestPayload));
}

export function requestAddCoverLetter(requestPayload): Dispatch<Types.RootAction>{
  return dispatch => dispatch({ type: ADD_USER_COVER_LETTER_REQUEST, requestPayload });
}

export function requestAddPersonalStatement(requestPayload): Dispatch<Types.RootAction>{
  return dispatch => dispatch({ type: ADD_USER_PERSONAL_STATEMENT_REQUEST, requestPayload });
}