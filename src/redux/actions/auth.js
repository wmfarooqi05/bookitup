import {
  GUEST_AUTH_REQUEST,
  CREATE_GUEST_USER_REQUEST,

  FETCH_USER_INFO_REQUEST,
  FETCH_USER_INFO_SUCCESS,

  LOCAL_SIGNIN_REQUEST,
  LOCAL_SIGNIN_SUCCESS,
  LOCAL_SIGNIN_FAIL,

  SET_USER_TOKEN,
  LOGIN_FROM_AUTH_TOKEN_REQUEST,

  FACEBOOK_SIGNIN_REQUEST,
  SIGNUP_REQUEST,
  SIGNUP_SUCCESS,
  SIGNUP_FAIL,
  SIGNUP_INPROGRESS,

  EDIT_PASSWORD_REQUEST,
  EDIT_PASSWORD_SUCCESS,
  EDIT_PASSWORD_FAIL,
  EDIT_PASSWORD_INPROGRESS,

  SIGN_OUT,
} from '../constants';

function createRequestGuestAuth() {
  return { type: GUEST_AUTH_REQUEST };
}

function createGuestUserRequest() {
  return { type: CREATE_GUEST_USER_REQUEST };
}

function fetchUserInfoRequest() {
  return { type: FETCH_USER_INFO_REQUEST };
}

function createLocalSignInRequest(requestPayload) {
  return { type: LOCAL_SIGNIN_REQUEST, requestPayload };
}

function requestUserTokenSet(payload) {
  return { type: SET_USER_TOKEN, payload };
}

export function requestFacebookSignIn(requestPayload) {
  return { type: FACEBOOK_SIGNIN_REQUEST, requestPayload };
}

export function requestGuestAuth() {
  return dispatch => dispatch(createRequestGuestAuth());
}

export function createGuestUser() {
  return dispatch => dispatch(createGuestUserRequest());
}

export function fetchUserInfo() {
  return dispatch => dispatch(fetchUserInfoRequest());
}

export function requestLocalSignIn(requestPayload) {
  return dispatch => dispatch(createLocalSignInRequest(requestPayload));
}

export function requestLoginFromAuthToken(payload){
  console.log('a')
  return dispatch => dispatch({ type: LOGIN_FROM_AUTH_TOKEN_REQUEST, payload });
}

export function dispatchToken(token) {
  return dispatch => dispatch(requestUserTokenSet(token));
}

export function dispatchCurrentUser(payload) {
  return dispatch => dispatch({ type: FETCH_USER_INFO_SUCCESS, payload });
}

function loginFromAuth(payload){
  return { type: LOGIN_FROM_AUTH_TOKEN_REQUEST, payload };
}


export function signup(requestPayload) {
  return { type: SIGNUP_REQUEST, requestPayload };
}

export function editPassword(requestPayload) {
  return { type: EDIT_PASSWORD_REQUEST, requestPayload };
}

export function signout() {
  return { type: SIGN_OUT };
}
