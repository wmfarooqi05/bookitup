import {
  FETCH_JOBS_REQUEST, SEARCH_JOBS,
  FETCH_MY_JOBS_REQUEST,
} from '../constants';

import jobList from '../../dummy/jobList';

export function addJob(job){
  return {
    type: ADD_JOB,
    job
  }
}

export function setJobLoading(){
  return {
    type: SET_JOB_LOADING,
  }
}

export function setJobLoaded(){
  return {
    type: SET_JOB_LOADED,
  }
}

export function fetchAllJobsRequest(requestPayload){
  return { type: FETCH_JOBS_REQUEST, requestPayload }
}

export function fetchMyJobsRequest(requestPayload){
  return { type: FETCH_MY_JOBS_REQUEST, requestPayload }
}

export const searchJobs = (requestPayload) => {
  return { type: SEARCH_JOBS, requestPayload }
}

export function loadJobs(){
  return (dispatch) => {
    dispatch(setJobLoading());
    let _jobList = jobList;

    dispatch(addJob(_jobList));
    dispatch(setJobLoaded());
  }
}