import { StackActions, NavigationActions } from 'react-navigation';
import Routes from '../../constants/Routes';

export type ROUTE_TYPE = string | null;

export function goBack(_key = null) {
  return NavigationActions.back({
    key: _key,
  });
}

export function navigate(_route) {
  return NavigationActions.navigate(_route);
}

export function resetRoute(_screen: ROUTE_TYPE = Routes.Home) {
  return StackActions.reset({
      index: 0,
      key: null,
      actions: [
        NavigationActions.navigate({ routeName: _screen}),
      ],
    });
}

export function push(route){
  return StackActions.push(route);
}

export function popAction( _n = 1){
  return StackActions.pop({
  n: _n,
  });
}

export function popToTop(){
  return StackActions.popToTop(); 
}

