/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import {
  ActivityIndicator,
  Platform, Dimensions,
  StyleSheet, Image,
  Text, TouchableHighlight,
  View
} from 'react-native';

import { Provider } from 'react-redux';
import { persistStore } from 'redux-persist';

import configureStore from './config/store';
import AppNavWithProps from './navigator/AppNavWithProps';

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;
import { isIphoneX } from 'react-native-iphone-x-helper'

// import GlobalKeys from './constants';
import Images from './constants/Images';

type Props = {};
export default class Main extends Component {
  
  constructor(props) {
    super(props);
    this.state = {
     store: configureStore(),
      loading: false,
      timePassed: false,
    }
  }

  componentDidMount() {
  //   setTimeout( () => {
  //     this.setTimePassed();
  //  },2000);
    // persistStore(
    //   this.state.store,
    //   { timeout: 3000 },
    //   () => { 
    //     this.setState({ loading: false }); 
    //   }
    // );
  }

  setTimePassed()
  {
    this.setState({timePassed: false});
  }

  render() {
    if (this.state.loading || this.state.timePassed){
      return(
        <View style= {{ flex:1, backgroundColor:'white', justifyContent:'center', alignItems:'center' }}>
          <Image source = {Images.logoSplash} 
            resizeMode="cover" style={{height: deviceHeight, width: deviceWidth}}/>
          <ActivityIndicator style={{zIndex:1, position:'absolute',alignSelf:'center', marginTop: deviceHeight * .4}} size="large" color={'red'} />
        </View>
      );
    } else {
      return(
        <Provider store={this.state.store}>
          {/* <Text>Waleed</Text> */}
          <AppNavWithProps/>
        </Provider>
      );
    }
   } 
}
 
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  seperator:{
    marginBottom: 20
  }
});