export default employee_profile = {
  email: 'wmfarooqi05@gmail.com',
  name: 'Terry Platchek',
  location : [
    33.715085,
    73.1500102
  ],
  skills : ['Web', 'Mobile', 'React Native'],
  about : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
  profile_image : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLgSlSXWs5SPC4JaDR5L0BX8e6i1rzfhFQjwa92N6-5ohlSESz',
  cover_image : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDWzOYurk6FEQkN8CRXbzY5-bPXmpQA4sDHiPTIw5KL1MfIh4ZhQ',
  rate : 4.5,
  prev_jobs : 14,
  job_title : 'Software Engineer',
  job_location : 'New York',
};
