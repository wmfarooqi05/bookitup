export default company_profile_data = {
  email: 'wmfarooqi05@gmail.com',
  name: 'Arkotech Softwares',
  location : [
    33.715085,
    73.1500102
  ],
  nature_of_work : ['Web', 'Mobile', 'React Native'],
  taxID : '4300-2312-4324-1233',
  missionStat : 'To offer designer eyewear at a revolutionary price, while leading the way for socially-conscious businesses.',
  about : 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s',
  businessAddress : 'It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout',
  profile_image : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTLgSlSXWs5SPC4JaDR5L0BX8e6i1rzfhFQjwa92N6-5ohlSESz',
  cover_image : 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRDWzOYurk6FEQkN8CRXbzY5-bPXmpQA4sDHiPTIw5KL1MfIh4ZhQ',
  companyID: 3,
  totalJobsPosted: 45,
  currOpenJobs:3,
  currOpenJobsList:[1,2,4], // Job Ids
};
