export default user_profile = {
  "status": 200,
  "profile": {
    "user_account_id": 8,
    "first_name": "usman",
    "last_name": "iqbal",
    "user_type": {
      "user_type_id": 2,
      "user_type_name": "JobSeeker"
    },
    "email": "usmaniq@gmail.com",
    "date_of_birth": "2018-10-09T19:00:00.000Z",
    "gender": null,
    "user_image": "uploads/accountImages/8.png",
    "contact_number": null,
    "registration_date": "2018-11-27T19:00:00.000Z",
    "current_salary": 500,
    "currency": {
      "currency_id": 1,
      "currency_name": "pakistan"
    },
    "cv": null,
    "personal_statement": null,
    "cover_letter": "usman iqbal cover leter",
    "educations": [
      {
        "education_id": 6,
        "certificate_degree": {
          "certificate_degree_name_id": 1,
          "certificate_degree_name": "Middle"
        },
        "major": "CE",
        "Institute_university_name": "Civil line lahore",
        "starting_date": "2018-10-08T19:00:00.000Z",
        "completion_date": "2018-10-08T19:00:00.000Z",
        "percentage": 56,
        "cgpa": null
      },
      {
        "education_id": 10,
        "certificate_degree": {
          "certificate_degree_name_id": 1,
          "certificate_degree_name": "Middle"
        },
        "major": "EE",
        "Institute_university_name": "Civil line lahore",
        "starting_date": "2018-10-08T19:00:00.000Z",
        "completion_date": "2018-10-08T19:00:00.000Z",
        "percentage": 56,
        "cgpa": null
      },
      {
        "education_id": 12,
        "certificate_degree": {
          "certificate_degree_name_id": 8,
          "certificate_degree_name": "others"
        },
        "major": "EE",
        "Institute_university_name": "Civil line lahore",
        "starting_date": "2018-10-08T19:00:00.000Z",
        "completion_date": "2018-10-08T19:00:00.000Z",
        "percentage": 56,
        "cgpa": null
      },
      {
        "education_id": 13,
        "certificate_degree": {
          "certificate_degree_name_id": 7,
          "certificate_degree_name": "Mphil"
        },
        "major": "EE",
        "Institute_university_name": "Civil line lahore",
        "starting_date": "2018-10-08T19:00:00.000Z",
        "completion_date": "2018-10-08T19:00:00.000Z",
        "percentage": 56,
        "cgpa": null
      }
    ],
    "experiences": [
      {
        "experience_id": 3,
        "job_title": "react native developer",
        "company_name": "  solution arkotech solution",
        "start_date": "2018-11-09T19:00:00.000Z",
        "end_date": "2018-11-10T19:00:00.000Z",
        "is_current_job": 0,
        "description": "i am react native developer and pase2"
      },
      {
        "experience_id": 5,
        "job_title": "react native",
        "company_name": " solution arkotech",
        "start_date": "2018-11-08T19:00:00.000Z",
        "end_date": "2018-10-08T19:00:00.000Z",
        "is_current_job": 0,
        "description": "i am react native developer and pase2"
      },
      {
        "experience_id": 7,
        "job_title": "react native developer",
        "company_name": " solution arkotech",
        "start_date": "2018-11-08T19:00:00.000Z",
        "end_date": "2018-10-08T19:00:00.000Z",
        "is_current_job": 0,
        "description": "i am react native developer and pase2"
      }
    ]
  }

}