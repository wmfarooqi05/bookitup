import Icons from "../constants/Icons";
import Images from "../constants/Images";

export default jobList = [
  {
      "address": {
          "mapLocation": {
              "latitude": 33,
              "longitude": 33
          },
          "country": "pakistan",
          "city": "Lahore",
          "address": 'Bani Gala Islamabad Pakistan'
      },
      "skills": [
          "C++",
          "java"
      ],
      "_id": "5bc6ed93f6396d27c4acd2f4",
      "jobTitle": "react native",
      "jobPostedBy": "5bc462797017c7291846395e",
      "jobTypeName": "full Time",
      "company": "5bc462797017c7291846395e",
      "isCompanyNameHidden": false,
      "createdDate": "2018-10-17T08:06:43.031Z",
      "applyDate": "2018-10-18T00:00:00.000Z",
      "description": null,
      "isActive": true,
      "minSalary": 0,
      "maxSalary": 0,
      "__v": 0
  },
  {
      "address": {
          "mapLocation": {
              "latitude": 33,
              "longitude": 33
          },
          "country": "pakistan",
          "city": "Lahore"
      },
      "skills": [
          "C++",
          "java"
      ],
      "_id": "5bc7129f7602db0eb058f453",
      "jobTitle": "react native job",
      "jobPostedBy": "5bc462797017c7291846395e",
      "jobTypeName": "part Time",
      "company": "5bc462797017c7291846395e",
      "isCompanyNameHidden": false,
      "createdDate": "2018-10-17T10:44:47.450Z",
      "applyDate": "2018-10-18T00:00:00.000Z",
      "description": null,
      "isActive": true,
      "minSalary": 0,
      "maxSalary": 0,
      "__v": 0
  },
  {
      "address": {
          "mapLocation": {
              "latitude": 33,
              "longitude": 33
          },
          "country": "pakistan",
          "city": "Lahore"
      },
      "skills": [
          "C++",
          "java"
      ],
      "_id": "5bc72c9fb92d2114c4a2963d",
      "jobTitle": "react native and react job",
      "jobPostedBy": "5bc462797017c7291846395e",
      "jobTypeName": "part Time",
      "company": "5bc462797017c7291846395e",
      "isCompanyNameHidden": false,
      "createdDate": "2018-10-17T12:35:43.017Z",
      "applyDate": "2018-10-18T00:00:00.000Z",
      "description": null,
      "isActive": true,
      "minSalary": 0,
      "maxSalary": 0,
      "__v": 0
  }
]