import * as React from "react"
import { View, TouchableOpacity, Text, Image, StyleSheet,
  Dimensions, ViewStyle, TextStyle, ImageStyle,
} from 'react-native';
import Icons from '../../constants/Icons';
import Colors from '../../constants/Colors';
import Fonts from '../../constants/Fonts';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'

interface ButtonProps {
  title?: string, // Change the required prop to an optional prop.
  showForwardIcon?: boolean,
  onPress?: () => void,
  style?: ViewStyle | Array<ViewStyle>,
  fontSize?: number,
  color?: string,
  disabled?: boolean,
}

const ButtonFowardIcon: React.SFC<ButtonProps> = (props) => {
  return (
    <View style={props.style}>
      <TouchableOpacity style={styles.buttonContainer}
        onPress={props.onPress}
      >
        <Text style={[styles.buttonText, {fontSize: props.fontSize, color: props.color}] as TextStyle[]}>
          {props.title}
        </Text>
        {
          props.showForwardIcon ? 
          <Image source={Icons.iconForward} style={styles.forwardIcon} />
          : null
        }
      </TouchableOpacity>

    </View>
  );
}

ButtonFowardIcon.defaultProps = {
  title: "Button", // This value is adopted when title prop is omitted.
  onPress: () => alert('button'),
  showForwardIcon: true,
  style:{},
  fontSize: responsiveFontSize(2.6),
  color: Colors.white,
}

export type StylesT = {
  buttonContainer?: ViewStyle,
  buttonText?: TextStyle,
  forwardIcon?: ImageStyle,
}

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create<StylesT>({
  buttonContainer: {
    minWidth: deviceWidth * .3,
    maxWidth: deviceWidth * .73,
    height: deviceHeight * .067,
    borderWidth:.3,
    borderColor: Colors.buttonForwardBorder,
    // backgroundColor:'red',
    justifyContent: 'center',
    alignItems:'center',
    borderRadius:2,
  },
  buttonText: {
    textAlign:'center',
    fontSize: responsiveFontSize(2.6),
    color: Colors.buttonForwardText,
    fontFamily: Fonts.LIGHT,
  },
  forwardIcon: {
    position:'absolute',
    right: deviceWidth * .03,
    height: '60%',
    resizeMode:"contain",
    // backgroundColor:'blue',
    width: deviceWidth*.04,
  },

});

export default ButtonFowardIcon;