import * as React from "react"
import {
  View, TouchableOpacity, Text, Image, StyleSheet,
  Dimensions, ViewStyle, TextStyle, ImageStyle,
} from 'react-native';
import Icons from '../../constants/Icons';
import Colors from '../../constants/Colors';
import Fonts from '../../constants/Fonts';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'

interface HeaderProps {
  onPress?: () => void,
  renderBackIcon?: boolean,
}

const Header: React.SFC<HeaderProps> = (props) => {
  
  function renderBackIcon(): React.ReactNode{
    if (props.renderBackIcon){
      return(
        <TouchableOpacity activeOpacity={.7} onPress={props.onPress} 
          style={styles.imageContainer}>
          <Image source={Icons.iconBack} style={styles.backIcon}/>
        </TouchableOpacity>
      )
    } else return (
      <View style={styles.imageContainer}>

      </View>
    );
  }

  return (
    <View style={styles.logoContainer}>
      {renderBackIcon()}
      <Image source={Icons.biuLogo} style={styles.biuLogo} />
    </View>
  );
}

Header.defaultProps = {
  onPress: () => alert('button'),
  renderBackIcon: false,
}

export type StylesT = {
  logoContainer?: ViewStyle,
  biuLogo?: ImageStyle,
  imageContainer?: ViewStyle,
  backIcon?: ImageStyle,
}

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create<StylesT>({
  logoContainer: {
    marginTop: deviceHeight * .042,
    paddingBottom: deviceHeight * .017,
    borderBottomColor: Colors.white,
    borderBottomWidth: 1,
    // flex: 1,
    flexDirection:'row',
  },
  biuLogo: {
    height: deviceHeight * .05,
    width: deviceWidth * .4,
    alignSelf: 'center',
    marginLeft: deviceWidth *.17,
  },
  imageContainer:{
    width: deviceWidth*.065,
    height: deviceHeight * .045,
    alignSelf:'flex-start',
    marginLeft: deviceWidth *.08,
  },
  backIcon:{
    width: '100%',
    height: '100%',
    resizeMode:'contain',
  },
});

export default Header;
