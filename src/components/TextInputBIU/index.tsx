import React, { forwardRef, ReactNode, createRef, LegacyRef } from "react"
import {
  View, TouchableOpacity, Text, Image, StyleSheet,
  Dimensions, ViewStyle, TextStyle, ImageStyle, TextInput, TextInputProps, ImageSourcePropType,
} from 'react-native';
import Icons from '../../constants/Icons';
import Colors from '../../constants/Colors';
import Fonts from '../../constants/Fonts';
import { responsiveFontSize } from 'react-native-responsive-dimensions';
import { ifIphoneX } from 'react-native-iphone-x-helper'
import { RefObject } from 'react';

// interface RefObject<T> {
//   readonly current: T | null;
// }

// type Ref<T> = string | ((instance: T) => any);

interface TextInputBIUProps extends TextInputProps{
  showIcon?: boolean,
  iconName?: ImageSourcePropType,
  containerStyle?: ViewStyle,
  // ref?: RefObject<T>,
}

// export type TextInputBIURef = LegacyRef<Image>;

//const ref: RefObject<TextInput> = createRef<TextInput>();
export const TextInputBIU = forwardRef<React.LegacyRef<TextInput>, TextInputBIUProps>((props, ref) => (

  // return(
  <View style={[styles.inputContainer, {...props.containerStyle}]}>
      { props.showIcon?
      
        <Image source={props.iconName} style={styles.inputIcon} resizeMode="contain" /> : null
      }
      <TextInput style={[styles.input,  props.showIcon === true ? {paddingLeft: deviceWidth*.01, marginRight: deviceWidth *.05} : {}]} 
        {...props}
        ref={ref}
      />
    </View>
  ));

TextInputBIU.defaultProps = {
  showIcon: false,
  iconName: Icons.email,
  // style:{},
}

export type StylesT = {
  inputContainer?: ViewStyle,
  inputIcon?: ImageStyle,
  input?: TextStyle,
}

const deviceHeight = Dimensions.get('window').height;
const deviceWidth = Dimensions.get('window').width;

const styles = StyleSheet.create<StylesT>({

  inputIcon:{
    width: deviceWidth * .076,
    height: deviceWidth * .054,
    marginLeft: deviceWidth * .04,
    // marginRight: deviceWidth * .097,
    // backgroundColor: 'blue',
  },
  input:{
    // backgroundColor: 'red',
    // ...ifIphoneX({
    //   width: deviceWidth * .52,
    //   marginLeft: deviceWidth * -.05,
    // }, {
    //   width: deviceWidth * .47,
    // }),
    flex:1,
    paddingLeft: deviceWidth*.05,
    paddingRight: deviceWidth*.05,
    fontSize: responsiveFontSize(2.2),
    fontFamily: Fonts.LIGHT,
    color: Colors.white,
    alignSelf: 'center',
    textAlign:'center',
  },
  inputContainer:{
    flexDirection:'row',
    width: deviceWidth * .73,
    alignSelf: 'center',
    alignItems: 'center',
    borderWidth: .3,
    height: deviceHeight * .067,
    borderRadius: 4,
    borderColor: Colors.white,
    // backgroundColor:'red',
  },
});

export default TextInputBIU;

