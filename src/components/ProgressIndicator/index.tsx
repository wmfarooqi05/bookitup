
import React from 'react';
import { ActivityIndicator,View } from 'react-native';
import { MediaQueryStyleSheet } from 'react-native-responsive';
import Colors from '../../constants/Colors';

const withLoadingHandling = WrappedComponent => ({ showLoading, children }) => {
    return(
      <WrappedComponent>
        {showLoading && (
          <View style={styles.overlay}>
            <ActivityIndicator style={styles.activityIndicator} 
              size="large" color={Colors.dropdown}/>
          </View>
        )}
        {children}
      </WrappedComponent>
    );
  };
  
const HOCWithoutProgressIndicator = ({ children }) => <View>{children}</View>
const HOCWithProgressIndicator = withLoadingHandling(HOCWithoutProgressIndicator)

export default HOCWithProgressIndicator;

const styles = MediaQueryStyleSheet.create({
  overlay: {
    position: 'absolute',
    top: 0,
    right: 0,
    bottom: 0,
    left: 0,
    zIndex: 100,
    backgroundColor: Colors.black,
    opacity: 0.6,
    justifyContent:'center',
    alignItems:'center'
  },
  activityIndicator:{
    alignSelf:'center'
  },
});